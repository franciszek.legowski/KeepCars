<?php
/**
 * Created by PhpStorm.
 * User: Szymon
 * Date: 11.04.2018
 * Time: 15:00
 */

namespace App\Http\Controllers;

use App\Car;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;


/**
 * Class RaportController
 * @package App\Http\Controllers
 */
class RaportController
{

    /**
     * Funkcja zwraca widok raportu kierowcy.
     *
     * Pobiera z bazy dane o samochodach i użytkownikach i przekazuje je do widoku.
     *
     * @return view('raport.index')
     */
    public function index()
    {
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();

        return view('raport.index')->with('cars', $cars)->with('users', $users);
    }

    /**
     * Funkcja zwraca widok raportu samochodu.
     *
     * Pobiera z bazy dane o samochodach i użytkownikach i przekazuje je do widoku.
     *
     * @return view('raport.export')
     */
    public function export()
    {
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        return view('raport.export')->with('users', $users)->with('cars', $cars);
    }


    /**
     * Funkcja zwraca tabelę z raportem samochodu.
     *
     * Na podstawie danych z otrzymanego formularza pobiera dane samochodu
     * i oblicza sumy częsciowe wydatków.
     * W zależności od parametru $export zwraca plik Excel lub widok.
     *
     * @param bool $export - czy chcemy eksportować dane do excela.
     * @return view('raport/show')
     */
    public function car($export = False)
    {
//        $przyslane = Input::get();
        $source_data_from = Input::get('data_from');
        $source_data_to = Input::get('data_to');
        $data_from = date("Y-m-d", strtotime("{$source_data_from}"));
        $data_to = date("Y-m-d", strtotime("{$source_data_to}"));
        $typ_koszt = Input::get('costType');
        $typ_car = Input::get('cos');
        $car_id = Input::get('car_id');
//return Input::get();
        $parametry = array('data_from' => $source_data_from, 'data_to' => $source_data_to, 'costType' => $typ_koszt,
            'cos' => $typ_car, 'car_id' => $car_id);

        $car_id = [];
        $samochod = [];
        $cars = NULL;
        $tankowania = [];
        $serwis = [];
        $suma_czesciowa_paliwo = [];
        $suma_czesciowa_serwis = [];
        $suma_calkowita_paliwo = [];
        $suma_calkowita_serwis = [];

        if ($typ_car == 'konkretnyPojazd') {
            array_push($car_id, Input::get('car_id'));
        } else {
            $cars = DB::table('cars')->select('id')->get();
            $car_id = [];
            foreach ($cars as $car) {
                array_push($car_id, $car->id);
            }
        }
        foreach ($car_id as $car) {
            $dane = DB::table('refuels')
                ->select(DB::raw('sum(refuel_quota) as koszt'))->where('car_id', $car)
                ->whereBetween('refuel_date', [$data_from, $data_to])->first()->koszt;
            array_push($suma_calkowita_paliwo, $dane > 0 ? $dane : 0);
            $dane = DB::table('costs')
                ->select(DB::raw('sum(cost_quota) as koszt'))->where('car_id', $car)
                ->whereBetween('cost_date', [$data_from, $data_to])->first()->koszt;
            array_push($suma_calkowita_serwis, $dane > 0 ? $dane : 0);
            array_push($samochod, DB::table('cars_view')->where('id', $car)->first());
        }

        //LICZENIE CZĘŚCIOWYCH WYDATKÓW KIEROWCÓW
        $dane = DB::table('refuels')->select(DB::raw('sum(refuel_quota) as koszt'), 'car_id', 'user_id')
            ->whereBetween('refuel_date', [$data_from, $data_to])
            ->groupBy('car_id')->groupBy('user_id')
            ->orderBy('car_id')->orderBy('user_id')->get();
        foreach ($dane as $value) {
            $suma_czesciowa_paliwo = array_merge($suma_czesciowa_paliwo,
                array("{$value->car_id}_{$value->user_id}" => $value->koszt));
        }

        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'), 'car_id', 'user_id')
            ->whereBetween('cost_date', [$data_from, $data_to])
            ->groupBy('car_id')->groupBy('user_id')
            ->orderBy('car_id')->orderBy('user_id')->get();
        foreach ($dane as $value) {
            $suma_czesciowa_serwis = array_merge($suma_czesciowa_serwis,
                array("{$value->car_id}_{$value->user_id}" => $value->koszt));
        }

        $widok = null;
        switch ($typ_koszt) {
            case "fuelCosts":
                $dane = DB::table('refuels')->select('car_id', 'user_id',
                    'refuel_quota', 'refuel_date', 'mileage')
                    ->whereBetween('refuel_date', [$data_from, $data_to])
                    ->orderBy('car_id')->orderBy('user_id')
                    ->orderBy('refuel_date')->get();
                foreach ($dane as $value) {
                    $value->user_id = DB::table('users_view')->where('id', $value->user_id)->first();
                    array_push($tankowania, $value);
                }

                $widok = [
                    'export' => $export,
                    'parametry' => $parametry,
                    'samochod' => $samochod,
                    'tankowania' => $tankowania,
                    'suma_czesciowa_paliwo' => $suma_czesciowa_paliwo,
                    'suma_calkowita_paliwo' => $suma_calkowita_paliwo,
                    'daty' => $data_from . " - " . $data_to];
                break;

            case "carCosts":
                $dane = DB::table('costs_view')
                    ->whereBetween('data', [$data_from, $data_to])
                    ->orderBy('car_id')->orderBy('user_id')
                    ->orderBy('data')->get();
                foreach ($dane as $value) {
                    $value->user_id = DB::table('users_view')->where('id', $value->user_id)->first();
                    array_push($serwis, $value);
                }

                $widok = [
                    'export' => $export,
                    'parametry' => $parametry,
                    'samochod' => $samochod,
                    'serwis' => $serwis,
                    'suma_czesciowa_serwis' => $suma_czesciowa_serwis,
                    'suma_calkowita_serwis' => $suma_calkowita_serwis,
                    'daty' => $data_from . " - " . $data_to];
                break;

            case "bothCosts":
                $dane = DB::table('refuels')->select('car_id', 'user_id',
                    'refuel_quota', 'refuel_date', 'mileage')
                    ->whereBetween('refuel_date', [$data_from, $data_to])
                    ->orderBy('car_id')->orderBy('user_id')
                    ->orderBy('refuel_date')->get();
                foreach ($dane as $value) {
                    $value->user_id = DB::table('users_view')->where('id', $value->user_id)->first();
                    array_push($tankowania, $value);
                }


                $dane = DB::table('costs_view')
                    ->whereBetween('data', [$data_from, $data_to])
                    ->orderBy('car_id')->orderBy('user_id')
                    ->orderBy('data')->get();
                foreach ($dane as $value) {
                    $value->user_id = DB::table('users_view')->where('id', $value->user_id)->first();
                    array_push($serwis, $value);
                }

                $widok = [
                    'export' => $export,
                    'parametry' => $parametry,
                    'samochod' => $samochod,
                    'tankowania' => $tankowania,
                    'serwis' => $serwis,
                    'suma_czesciowa_paliwo' => $suma_czesciowa_paliwo,
                    'suma_czesciowa_serwis' => $suma_czesciowa_serwis,
                    'suma_calkowita_paliwo' => $suma_calkowita_paliwo,
                    'suma_calkowita_serwis' => $suma_calkowita_serwis,
                    'daty' => $data_from . " - " . $data_to];
                break;
        }

        if ($export) {
            Excel::create('Raport', function ($excel) use ($widok) {
                foreach ($widok['samochod'] as $value) {
                    $excel->sheet("{$value->marka} {$value->model} {$value->registration_number}",
                        function ($sheet) use ($widok, $value) {
                            $sheet->loadView("raport/show2", $widok)->with('value', $value);
                            $sheet->setColumnFormat(array('B' => '0.00'));
                        });
                }
            })->export('xlsx');
        } else
            return view("raport/show", $widok);
    }

    /**
     * Funkcja liczy dane potrzebne do wykresów w raporcie pojazdu.
     *
     * Pobiera z formularza zakres dat, indeks samochodu, kierowcę, interwał wykresów.
     * Dla pobranych danych oblicza wszystkie statystyki i dane do wykresów
     * przedstawione w widoku.
     * Zwraca widok z danymi.
     *
     * @return view('raport/export')
     */
    public function car_wykres()
    {
        $data_from = Input::get('data_from');
        $data_to = Input::get('data_to');
        $car_id = Input::get('car_id');
        if ($car_id == null) return "<h1>Nie wybrano samochodu</h1>";
        $samochod = DB::table('cars_view')->where('id', $car_id)->first();
        $user_id = [Input::get('user_id')];
        if ($user_id[0] == 0) {
            $dane = DB::table('users')->select('id')
                ->where('deleted_at', '<', $data_from)->get();
            $user_id = [];
            foreach ($dane as $value)
                array_push($user_id, $value->id);
        }
        $step = 1;
        $interval = Input::get('interwalRaport');

        $wykresSrednieSpalanieWszyscy = [];
        $wykresKosztyPaliwaWszyscy = [];
        $wykresKosztySerwisoweWszyscy = [];
        $wykresPrzebiegKilometrowWszyscy = [];
        $wydatki_paliwo_car = (new ChartsController)
            ->wydatki_paliwo_car($car_id, $data_from, $data_to);
        $wydatki_serwisowe_car = (new ChartsController)
            ->wydatki_serwisowe_car($car_id, $data_from, $data_to);
        $suma_zatankowawe_litry_car = (new ChartsController)
            ->suma_zatankowawe_litry_car($car_id, $data_from, $data_to);
        $ilosc_wydatkow_serwisowych_car = (new ChartsController)
            ->ilosc_wydatkow_serwisowych_car($car_id, $data_from, $data_to);
        $przejechane_kilometry_car = (new ChartsController)
            ->przejechane_kilometry_car($car_id, $data_from, $data_to);
        $mies_diff = (new ChartsController)
            ->ile_miesiecy_roznicy($data_from, $data_to);
        $ilosc_tankowan_car = (new ChartsController)
            ->ilosc_tankowan_car($car_id, $data_from, $data_to);
        $srednie_wydatki_car = (new ChartsController)->avg_wydatki_car($car_id, $data_from, $data_to);
        $srednie_spalanie_car = (new ChartsController)
            ->wykresLiniowySrednieSpalanie_car($car_id, $data_from, $data_to, $step, $interval);
        $srednie_spalanie_car = json_encode($srednie_spalanie_car);

        //DODANIE DANYCH DO WYKRESU : WYKRES LINIOWY KOSZTÓW SERWISOWYCH
        $jsonarray = (new ChartsController)->wykresLiniowyKosztowSerwisowych(
            $car_id, $data_from, $data_to, $step, $interval);
        $tablica_daty = json_encode($jsonarray['daty']);
        $tablica_koszty = json_encode($jsonarray['inne']);

        //Kolowy
        $dane = (new ChartsController)
            ->wykresKolowyPodzialuKosztowNaRodzaje($car_id, $data_from, $data_to);
        $tablica_nazwy_kosztow = json_encode($dane['typ']);
        $tablica_koszty_wykres_kolowy = json_encode($dane['koszt']);

        //DODANIE DANYCH DO WYKRESU : WYKRES SŁUPKOWY KOSZTÓW  NA PALIWO I SERWISOWYCH
        $dane = (new ChartsController)
            ->wykresSlupkowyKosztowPaliwowychOrazSerwisowych($car_id, $data_from, $data_to, $step, $interval);
        $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis = $dane['serwis'];
        $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo = $dane['paliwo'];
        $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis = json_encode(
            $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis);
        $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo = json_encode(
            $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo);

        //WYKRES LINIOWY PRZEBIEGU KILOMETRÓW
        $wykresLiniowyPrzebieguKilometrow_km = (new ChartsController)
            ->wykresLiniowyPrzebieguKilometrow_car($car_id, $data_from, $data_to, $step, $interval);
        $wykresLiniowyPrzebieguKilometrow_km = json_encode($wykresLiniowyPrzebieguKilometrow_km);

        return view('raport/export', [
            'users' => User::orderBy('surname')->orderBy('name')->orderBy('email')->get(),
            'cars' => Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get(),
            'wejscie' => Input::get(),
            'data_from' => $data_from,
            'data_to' => $data_to,
            'car_id' => $car_id,
            'samochod' => $samochod,
            'step' => $step,
            'interval' => $interval,
            'wykresSrednieSpalanieWszyscy' => $wykresSrednieSpalanieWszyscy,
            'wykresKosztyPaliwaWszyscy' => $wykresKosztyPaliwaWszyscy,
            'wykresKosztySerwisoweWszyscy' => $wykresKosztySerwisoweWszyscy,
            'wykresPrzebiegKilometrowWszyscy' => $wykresPrzebiegKilometrowWszyscy,
            'wydatki_paliwo_car' => $wydatki_paliwo_car,
            'wydatki_serwisowe_car' => $wydatki_serwisowe_car,
            'suma_zatankowawe_litry_car' => $suma_zatankowawe_litry_car,
            'ilosc_wydatkow_serwisowych_car' => $ilosc_wydatkow_serwisowych_car,
            'przejechane_kilometry_car' => $przejechane_kilometry_car,
            'mies_diff' => $mies_diff,
            'ilosc_tankowan_car' => $ilosc_tankowan_car,
            'srednie_wydatki_car' => $srednie_wydatki_car,
            'srednie_spalanie_car' => $srednie_spalanie_car,
            'tablica_daty' => $tablica_daty,
            'tablica_koszty' => $tablica_koszty,
            'tablica_nazwy_kosztow' => $tablica_nazwy_kosztow,
            'tablica_koszty_wykres_kolowy' => $tablica_koszty_wykres_kolowy,
            'wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis' =>
                $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis,
            'wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo' =>
                $wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo,
            'wykresLiniowyPrzebieguKilometrow_km' => $wykresLiniowyPrzebieguKilometrow_km
        ]);
    }

    /**
     * Funkcja pobiera dane o samochodach i userach z bazy i zwraca je z widokiem
     * @return view('raport/excel')
     */
    public function excel()
    {
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        return view("raport/excel", ['users' => $users, 'cars' => $cars]);
    }

    /**
     * Funkcja zwraca tabelę z raportem kierowcy.
     *
     * Na podstawie danych z otrzymanego formularza pobiera dane kierowcy
     * i oblicza sumy częsciowe wydatków.
     * W zależności od parametru $export zwraca plik Excel lub widok.
     *
     * @param bool $export - czy chcemy eksportować dane do excela.
     * @return view('raport/show')
     */
    public function kierowca($export = False)
    {
        $source_data_from = Input::get('data_from');
        $source_data_to = Input::get('data_to');
        $data_from = date("Y-m-d", strtotime("{$source_data_from}"));
        $data_to = date("Y-m-d", strtotime("{$source_data_to}"));
        $typ_koszt = Input::get('costType');
        $typ_user = Input::get('cos');
        $user_id = Input::get('user_id');
//        return Input::get();
        $parametry = array('data_from' => $source_data_from, 'data_to' => $source_data_to, 'costType' => $typ_koszt,
            'cos' => $typ_user, 'user_id' => $user_id);
//        return $parametry;


        $tankowania = [];
        $serwis = [];
        $suma_czesciowa_paliwo = [];
        $suma_czesciowa_serwis = [];
        $suma_calkowita_paliwo = [];
        $suma_calkowita_serwis = [];

        if ($typ_user == "user1") {
            if ($user_id[0] == 'W')
                return "<h1>Wybierz konkretnego kierowcę.</h1>";
            else
                $user_id = [$user_id];
        } else {
            $user_id = [];
            $dane = User::select('id')->orderBy('surname')->get();
            foreach ($dane as $value)
                array_push($user_id, $value->id);
        }
        $kierowca = [];//DB::table('users_view')->whereIn('id',$user_id)->get();

        foreach ($user_id as $user) {
            $dane = DB::table('refuels')
                ->select(DB::raw('sum(refuel_quota) as koszt'))->where('user_id', $user)
                ->whereBetween('refuel_date', [$data_from, $data_to])->first()->koszt;
            array_push($suma_calkowita_paliwo, $dane > 0 ? $dane : 0);
            $dane = DB::table('costs')
                ->select(DB::raw('sum(cost_quota) as koszt'))->where('user_id', $user)
                ->whereBetween('cost_date', [$data_from, $data_to])->first()->koszt;
            array_push($suma_calkowita_serwis, $dane > 0 ? $dane : 0);
            array_push($kierowca, DB::table('users_view')->where('id', $user)->first());
        }


        //LICZENIE CZĘŚCIOWYCH WYDATKÓW NA SAMOCHODY
        $dane = DB::table('refuels')->select(DB::raw('sum(refuel_quota) as koszt'), 'car_id', 'user_id')
            ->whereBetween('refuel_date', [$data_from, $data_to])
            ->groupBy('user_id')->groupBy('car_id')
            ->orderBy('car_id')->orderBy('user_id')->get();
        foreach ($dane as $value) {
            $suma_czesciowa_paliwo = array_merge($suma_czesciowa_paliwo,
                array("{$value->car_id}_{$value->user_id}" => $value->koszt));
        }

        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'), 'car_id', 'user_id')
            ->whereBetween('cost_date', [$data_from, $data_to])
            ->groupBy('user_id')->groupBy('car_id')
            ->orderBy('car_id')->orderBy('user_id')->get();
        foreach ($dane as $value) {
            $suma_czesciowa_serwis = array_merge($suma_czesciowa_serwis,
                array("{$value->car_id}_{$value->user_id}" => $value->koszt));
        }


        switch ($typ_koszt) {
            case "refuels":
                $dane = DB::table('refuels')->select('car_id', 'user_id',
                    'refuel_quota', 'refuel_date', 'mileage')
                    ->whereBetween('refuel_date', [$data_from, $data_to])
                    ->orderBy('user_id')->orderBy('car_id')
                    ->orderBy('refuel_date')->get();
                foreach ($dane as $value) {
                    $value->car_id = DB::table('cars_view')->where('id', $value->car_id)->first();
                    array_push($tankowania, $value);
                }

                $widok = [
                    'export' => $export,
                    'parametry' => $parametry,
                    'kierowca' => $kierowca,
                    'tankowania' => $tankowania,
                    'suma_czesciowa_paliwo' => $suma_czesciowa_paliwo,
                    'suma_calkowita_paliwo' => $suma_calkowita_paliwo,
                    'daty' => $data_from . " - " . $data_to];
                break;
            case "serwices":
                $dane = DB::table('costs_view')
                    ->whereBetween('data', [$data_from, $data_to])
                    ->orderBy('user_id')->orderBy('car_id')
                    ->orderBy('data')->get();
                foreach ($dane as $value) {
                    $value->car_id = DB::table('cars_view')->where('id', $value->car_id)->first();
                    array_push($serwis, $value);
                }

                $widok = [
                    'export' => $export,
                    'parametry' => $parametry,
                    'kierowca' => $kierowca,
                    'serwis' => $serwis,
                    'suma_czesciowa_serwis' => $suma_czesciowa_serwis,
                    'suma_calkowita_serwis' => $suma_calkowita_serwis,
                    'daty' => $data_from . " - " . $data_to];
                break;

            case "both":
                $dane = DB::table('refuels')->select('car_id', 'user_id',
                    'refuel_quota', 'refuel_date', 'mileage')
                    ->whereBetween('refuel_date', [$data_from, $data_to])
                    ->orderBy('car_id')->orderBy('user_id')
                    ->orderBy('refuel_date')->get();
                foreach ($dane as $value) {
                    $value->car_id = DB::table('cars_view')->where('id', $value->car_id)->first();
                    array_push($tankowania, $value);
                }


                $dane = DB::table('costs_view')
                    ->whereBetween('data', [$data_from, $data_to])
                    ->orderBy('car_id')->orderBy('user_id')
                    ->orderBy('data')->get();
                foreach ($dane as $value) {
                    $value->car_id = DB::table('cars_view')->where('id', $value->car_id)->first();
                    array_push($serwis, $value);
                }

                $widok = [
                    'export' => $export,
                    'parametry' => $parametry,
                    'kierowca' => $kierowca,
                    'tankowania' => $tankowania,
                    'serwis' => $serwis,
                    'suma_czesciowa_paliwo' => $suma_czesciowa_paliwo,
                    'suma_czesciowa_serwis' => $suma_czesciowa_serwis,
                    'suma_calkowita_paliwo' => $suma_calkowita_paliwo,
                    'suma_calkowita_serwis' => $suma_calkowita_serwis,
                    'daty' => $data_from . " - " . $data_to];
                break;
        }


        if ($export) {
            Excel::create('Raport', function ($excel) use ($widok) {
                foreach ($widok['kierowca'] as $value) {
                    $excel->sheet("{$value->name} {$value->surname}", function ($sheet) use ($widok, $value) {
                        $sheet->loadView("raport/show2", $widok)->with('value', $value);
                        $sheet->setColumnFormat(array('B' => '0.00'));
                    });
                }
            })->export('xlsx');
        } else
            return view("raport/show", $widok);
    }

    /**
     * Funkcja liczy dane potrzebne do wykresów w raporcie kierowcy.
     *
     * Pobiera z formularza zakres dat, indeks samochodu, kierowcę, interwał wykresów.
     * Dla pobranych danych oblicza wszystkie statystyki i dane do wykresów
     * przedstawione w widoku.
     * Zwraca widok z danymi.
     *
     * @return view('raport/index')
     */
    public function kierowca_wykres()
    {
        $user_id = Input::get('user_id');
        $car_id = [Input::get('car_id')];
        $data_from = Input::get('data_from');
        $data_to = Input::get('data_to');
        $interval = Input::get('interwalRaport');

        $user = DB::table('users_view')->where('id', $user_id)->first();

        if ($car_id[0] == 0) {
            $car_id = (new ChartsController)->samochody_ktorymi_jezdzil($user_id, $data_from, $data_to);
        }
        $okres = date("d-m-Y", strtotime($data_from)) . " - " . date("d-m-Y", strtotime($data_to));

        $daty = json_encode((new ChartsController)->daty($data_from, $data_to, 1, $interval));

        $wydatki_paliwo = (new ChartsController)->wydatki_paliwo_flota_kierowca($user_id, $data_from, $data_to);
        $wydatki_serwis = (new ChartsController)->wydatki_serwisowe_flota_kierowca($user_id, $data_from, $data_to);
        $zatankowane_litry = (new ChartsController)
            ->suma_zatankowawe_litry_flota_kierowca($user_id, $data_from, $data_to);
        $ilosc_wydatkow_serwisowych = (new ChartsController)
            ->ilosc_wydatkow_serwisowych_flota_kierowca($user_id, $data_from, $data_to);
        $przejechane_kilometry = (new ChartsController)
            ->przejechane_kilometry_flota_kierowca($user_id, $data_from, $data_to);
//        return $przejechane_kilometry;
        $miesdiff = (new ChartsController)->ile_miesiecy_roznicy($data_from, $data_to);
        $srednie_wydatki_miesieczne = round(($wydatki_paliwo + $wydatki_serwis) / $miesdiff, 2);
        $sredni_przebieg_miesieczny = round($przejechane_kilometry / $miesdiff, 2);
        $ilosc_tankowan = (new ChartsController)->ilosc_tankowan_flota_kierowca($user_id, $data_from, $data_to);

        $efektywnosc = [];

        $wykres_liniowy_przebieg = (new ChartsController)
            ->wykresLiniowyPrzebieguKilometrow_flota_kierowca($user_id, $data_from, $data_to, 1, $interval);
        $wykres_liniowy_przebieg = json_encode($wykres_liniowy_przebieg);

        $wykres_slupkowy_koszty_paliwowe_i_serwisowe = (new ChartsController)
            ->wykresSlupkowyKosztowPaliwowychOrazSerwisowych_flota_kierowca(
                $user_id, $data_from, $data_to, 1, $interval);

        $srednie_wydatki_paliwowe = $ilosc_tankowan ? round($wydatki_paliwo / $ilosc_tankowan, 2) : 0;
        $srednie_wydatki_serwisowe = $ilosc_wydatkow_serwisowych ? round($wydatki_serwis / $ilosc_wydatkow_serwisowych, 2) : 0;
        $max_wydatki_paliwo = (new ChartsController)->maxWydatkiPaliwo_kierowca($user_id, $data_from, $data_to);
        $min_wydatki_paliwo = (new ChartsController)->minWydatkiPaliwo_kierowca($user_id, $data_from, $data_to);

        $wykres_slupkowy_koszty_paliwowe = json_encode($wykres_slupkowy_koszty_paliwowe_i_serwisowe['paliwo']);
        $wykres_slupkowy_koszty_serwisowe = json_encode($wykres_slupkowy_koszty_paliwowe_i_serwisowe['serwis']);

        $wykres_kolowy_kilometry_pojazdy = (new ChartsController)
            ->wykresKolowyPrzejechanychKilometrowPojazdy_kierowca($user_id, $data_from, $data_to);
        $wykres_kolowy_kilometry_pojazdy_cars = json_encode($wykres_kolowy_kilometry_pojazdy['samochody']);
        $wykres_kolowy_kilometry_pojazdy_kilometry = json_encode($wykres_kolowy_kilometry_pojazdy['kilometry']);

        $wydatki_paliwo_wszystkie = [];
        $max_wydatki_paliwo_wszyscy = [];
        $min_wydatki_paliwo_wszyscy = [];
        $avg_wydatki_paliwo_wszyscy = [];
        $przejechane_kilometry_wszyscy = [];
        $max_kilometry_wszyscy = [];
        $min_kilometry_wszyscy = [];
        $avg_kilometry_wszyscy = [];
        $avg_spalanie_wszyscy = [];
        $avg_przebieg_miesiac_wszyscy = [];
        $wydatki_serwis_wszyscy = [];
        $avg_serwis_wszyscy = [];

        $wykres_slupkowy_koszty_paliwowe_wszyscy = [];
        $wykres_slupkowy_koszty_serwisowe_wszyscy = [];
        $wykres_kolowy_kilometry_pojazdu_wszyscy = [];
        $wykres_liniowy_srednie_spalanie_wszyscy = [];
        $wykres_liniowy_srednie_spalanie_car_wszyscy = [];
        $wykres_liniowy_przebieg_wszyscy = [];

        foreach ($car_id as $car) {
            $wydatki_paliwo_wszystkie = array_merge($wydatki_paliwo_wszystkie,
                [$car . "." => (new ChartsController)
                    ->wydatki_paliwo_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $max_wydatki_paliwo_wszyscy = array_merge($max_wydatki_paliwo_wszyscy,
                [$car . "." => (new ChartsController)
                    ->maxWydatkiPaliwo_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $min_wydatki_paliwo_wszyscy = array_merge($min_wydatki_paliwo_wszyscy,
                [$car . "." => (new ChartsController)
                    ->minWydatkiPaliwo_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $avg_wydatki_paliwo_wszyscy = array_merge($avg_wydatki_paliwo_wszyscy,
                [$car . "." => (new ChartsController)
                    ->avg_wydatkiPaliwo_car_kierowca($user_id, $car, $data_from, $data_to)]);

            $przejechane_kilometry_wszyscy = array_merge($przejechane_kilometry_wszyscy,
                [$car . "." => (new ChartsController)
                    ->przejechane_kilometry_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $max_kilometry_wszyscy = array_merge($max_kilometry_wszyscy,
                [$car . "." => (new ChartsController)
                    ->maxTrasa_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $min_kilometry_wszyscy = array_merge($min_kilometry_wszyscy,
                [$car . "." => (new ChartsController)
                    ->minTrasa_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $avg_kilometry_wszyscy = array_merge($avg_kilometry_wszyscy,
                [$car . "." => (new ChartsController)
                    ->avg_trasa_car_kierowca($user_id, $car, $data_from, $data_to)]);

            $avg_spalanie_wszyscy = array_merge($avg_spalanie_wszyscy,
                [$car . "." => (new ChartsController)
                    ->avg_spalanie_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $avg_przebieg_miesiac_wszyscy = array_merge($avg_przebieg_miesiac_wszyscy,
                [$car . "." => round((new ChartsController)
                        ->przejechane_kilometry_car_kierowca($user_id, $car, $data_from, $data_to)
                    / $miesdiff, 2)]);

            $wydatki_serwis_wszyscy = array_merge($wydatki_serwis_wszyscy,
                [$car . "." => (new ChartsController)
                    ->wydatki_serwisowe_car_kierowca($user_id, $car, $data_from, $data_to)]);
            $avg_serwis_wszyscy = array_merge($avg_serwis_wszyscy,
                [$car . "." => (new ChartsController)
                    ->avg_wydatki_serwisowe_car_kierowca($user_id, $car, $data_from, $data_to)]);

            // EFEKTYWNOŚĆ KIEROWCY
            array_push($efektywnosc, (new ChartsController)->avg_spalanie_car($car, $data_from, $data_to)
                - $avg_spalanie_wszyscy[$car . "."]);

            //DANE DO WYKRESÓW

            $wykres_slupkowy_koszty_paliwowe_i_serwisowe_wszyscy = (new ChartsController)
                ->wykresSlupkowyKosztowPaliwowychOrazSerwisowych_kierowca(
                    $user_id, $car, $data_from, $data_to, 1, $interval);
            $wykres_slupkowy_koszty_paliwowe_wszyscy = array_merge($wykres_slupkowy_koszty_paliwowe_wszyscy,
                [$car . "." => json_encode($wykres_slupkowy_koszty_paliwowe_i_serwisowe_wszyscy['paliwo'])]);
            $wykres_slupkowy_koszty_serwisowe_wszyscy = array_merge($wykres_slupkowy_koszty_serwisowe_wszyscy,
                [$car . "." => json_encode($wykres_slupkowy_koszty_paliwowe_i_serwisowe_wszyscy['serwis'])]);

            $wykres_kolowy_kilometry_pojazdu_wszyscy = array_merge($wykres_kolowy_kilometry_pojazdu_wszyscy,
                [$car . "." => ['inni' => json_encode((new ChartsController)
                    ->przejechane_kilometry_car_inni($user_id, $car, $data_from, $data_to)),
                    'ja' => json_encode((new ChartsController)
                        ->przejechane_kilometry_car_kierowca($user_id, $car, $data_from, $data_to))]]);

            $wykres_liniowy_srednie_spalanie_wszyscy = array_merge($wykres_liniowy_srednie_spalanie_wszyscy, [
                $car . "." => json_encode((new ChartsController)->wykresLiniowySrednieSpalanie_car_kierowca(
                    $user_id, $car, $data_from, $data_to, 1, $interval))
            ]);
            $wykres_liniowy_przebieg_wszyscy = array_merge($wykres_liniowy_przebieg_wszyscy, [$car . "." => json_encode(
                (new ChartsController)->wykresLiniowyPrzebieguKilometrow_car_kierowca(
                    $user_id, $car, $data_from, $data_to, 1, $interval)
            )]);
            $wykres_liniowy_srednie_spalanie_car_wszyscy = array_merge($wykres_liniowy_srednie_spalanie_car_wszyscy,
                [$car . "." => json_encode((new ChartsController)->wykresLiniowySrednieSpalanie_car(
                    $car, $data_from, $data_to, 1, $interval))]);

        }
//    return $avg_spalanie_wszyscy;
        $efektywnosc = count($car_id) ? round(array_sum($efektywnosc) / count($efektywnosc), 3) : 0;

//        return $efektywnosc;
        return view("raport/index", [
            'users' => User::orderBy('surname')->orderBy('name')->orderBy('email')->get(),
            'cars' => Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get(),
            'user' => $user,
            'prowadzone' => $car_id,
            'okres' => $okres,
            'daty' => $daty,
            'wydatki_paliwo' => $wydatki_paliwo,
            'wydatki_serwis' => $wydatki_serwis,
            'zatankowane_litry' => $zatankowane_litry,
            'ilosc_wydatkow_serwisowych' => $ilosc_wydatkow_serwisowych,
            'przejechane_kilometry' => $przejechane_kilometry,
            'srednie_wydatki_miesieczne' => $srednie_wydatki_miesieczne,
            'sredni_przebieg_miesieczny' => $sredni_przebieg_miesieczny,
            'ilosc_tankowan' => $ilosc_tankowan,
            'efektywnosc' => $efektywnosc,
            'wykres_liniowy_przebieg' => $wykres_liniowy_przebieg,
            'wykres_slupkowy_koszty_paliwowe' => $wykres_slupkowy_koszty_paliwowe,
            'wykres_slupkowy_koszty_serwisowe' => $wykres_slupkowy_koszty_serwisowe,
            'srednie_wydatki_paliwowe' => $srednie_wydatki_paliwowe,
            'srednie_wydatki_serwisowe' => $srednie_wydatki_serwisowe,
            'max_wydatki_paliwo' => $max_wydatki_paliwo,
            'min_wydatki_paliwo' => $min_wydatki_paliwo,
            'wykres_kolowy_kilometry_pojazdy_cars' => $wykres_kolowy_kilometry_pojazdy_cars,
            'wykres_kolowy_kilometry_pojazdy_kilometry' => $wykres_kolowy_kilometry_pojazdy_kilometry,
            'wydatki_paliwo_wszystkie' => $wydatki_paliwo_wszystkie,
            'max_wydatki_paliwo_wszyscy' => $max_wydatki_paliwo_wszyscy,
            'min_wydatki_paliwo_wszyscy' => $min_wydatki_paliwo_wszyscy,
            'avg_wydatki_paliwo_wszyscy' => $avg_wydatki_paliwo_wszyscy,
            'przejechane_kilometry_wszyscy' => $przejechane_kilometry_wszyscy,
            'max_kilometry_wszyscy' => $max_kilometry_wszyscy,
            'min_kilometry_wszyscy' => $min_kilometry_wszyscy,
            'avg_kilometry_wszyscy' => $avg_kilometry_wszyscy,
            'avg_spalanie_wszyscy' => $avg_spalanie_wszyscy,
            'avg_przebieg_miesiac_wszyscy' => $avg_przebieg_miesiac_wszyscy,
            'wydatki_serwis_wszyscy' => $wydatki_serwis_wszyscy,
            'avg_serwis_wszyscy' => $avg_serwis_wszyscy,
            'wykres_slupkowy_koszty_paliwowe_wszyscy' => $wykres_slupkowy_koszty_paliwowe_wszyscy,
            'wykres_slupkowy_koszty_serwisowe_wszyscy' => $wykres_slupkowy_koszty_serwisowe_wszyscy,
            'wykres_kolowy_kilometry_pojazdu_wszyscy' => $wykres_kolowy_kilometry_pojazdu_wszyscy,
            'wykres_liniowy_srednie_spalanie_wszyscy' => $wykres_liniowy_srednie_spalanie_wszyscy,
            'wykres_liniowy_przebieg_wszyscy' => $wykres_liniowy_przebieg_wszyscy,
            'wykres_liniowy_srednie_spalanie_car_wszyscy' => $wykres_liniowy_srednie_spalanie_car_wszyscy,
        ]);
    }

}