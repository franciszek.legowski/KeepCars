<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRide;
use App\Http\Resources\RideResource;
use App\Ride;
use Symfony\Component\HttpFoundation\Response;

/**
 * klasa RideAPIController odpowiada za pracę z Przejazdami w aplikacji Android.
 * @package App\Http\Controllers
 */
class RideAPIController extends Controller
{
    /**
     * RideAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:ride_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:ride_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:ride_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:ride_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca dane o wyszstkich Przejazdach.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return RideResource::collection(Ride::all());
    }


    /**
     * Funkcja dodaje do bazy nowy Przejazd.
     * @param RequestRide $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestRide $request)
    {
        $ride = new Ride;
        $ride->user_id = $request->user_id;
        $ride->car_id = $request->car_id;
        $ride->ride_date = $request->ride_date;
        $ride->begin_mileage = $request->begin_mileage;
        $ride->end_mileage = $request->end_mileage;
        $ride->save();

        return response([
            'data' => new RideResource($ride)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane o Przejeździe o podanym id.
     * @param $id
     * @return RideResource
     */
    public function show($id)
    {
        return new RideResource(Ride::find($id));
    }

    /**
     * Funkcja edytuje w bazie Przejazdu o danym id.
     * @param RequestRide $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestRide $request, $id)
    {
        $ride = Ride::find($id);
        $ride->user_id = $request->user_id;
        $ride->car_id = $request->car_id;
        $ride->ride_date = $request->ride_date;
        $ride->begin_mileage = $request->begin_mileage;
        $ride->end_mileage = $request->end_mileage;
        $ride->save();

        return response([
            'data' => new RideResource($ride)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy Przejazd o danym id.
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $ride = Ride::find($id);
        $ride->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
