<?php

namespace App\Http\Controllers;

use App\Car;
use App\Cost;
use App\CostType;
use App\Http\Requests\RequestCost;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa CostController posiada funkcje pracujące z Kosztami serwisowymi.
 * @package App\Http\Controllers
 */
class CostController extends Controller
{
    /**
     * CostController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:cost_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:cost_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:cost_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:cost_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Kosztów serwisowych.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $cost_type = Input::has('search_cost_type') ? Input::get('search_cost_type') : null;
        $car = Input::has('search_car') ? Input::get('search_car') : null;
        $user = Input::has('search_user') ? Input::get('search_user') : null;
        $cost_quota_from = Input::has('search_cost_quota_from') ? Input::get('search_cost_quota_from') : null;
        $cost_quota_to = Input::has('search_cost_quota_to') ? Input::get('search_cost_quota_to') : null;
        $cost_date_from = Input::has('search_cost_date_from') ? Input::get('search_cost_date_from') : null;
        $cost_date_to = Input::has('search_cost_date_to') ? Input::get('search_cost_date_to') : null;
        $description = Input::has('search_description') ? Input::get('search_description') : null;

        if (isset($cost_type)) {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->where('cost_types.name', 'LIKE', '%' . $cost_type . '%')
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        } else if (isset($car)) {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->where('brands.name', 'LIKE', '%' . $car . '%')
                ->orWhere('cars.model', 'LIKE', '%' . $car . '%')
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        } else if (isset($user)) {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->where('users.name', 'LIKE', '%' . $user . '%')
                ->orWhere('users.surname', 'LIKE', '%' . $user . '%')
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        } else if (isset($cost_quota_from) and isset($cost_quota_to)) {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->whereBetween('costs.cost_quota', [$cost_quota_from, $cost_quota_to])
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        } else if (isset($cost_date_from) and isset($cost_date_to)) {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->whereBetween('costs.cost_date', [$cost_date_from, $cost_date_to])
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        } else if (isset($description)) {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->where('costs.description', 'LIKE', '%' . $description . '%')
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        } else {
            $costs = DB::table('costs')
                ->leftJoin('cost_types', 'costs.cost_type_id', '=', 'cost_types.id')
                ->leftJoin('cars', 'costs.car_id', '=', 'cars.id')
                ->leftJoin('users', 'costs.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('costs.id as id',
                    'cost_types.name as cost_type',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'costs.cost_quota as cost_quota',
                    'costs.cost_date as cost_date',
                    'costs.description as description'
                )
                ->orderBy('cost_date', 'dsc')
                ->paginate(10);
        }
        return view('cost.index', ['costs' => $costs]);
    }

    /**
     * Funkcja zwraca widok do tworzenia kosztu serwisowego.
     * @return view('cost.create')
     */
    public function create()
    {
//        $costs = Cost::orderBy('name')->get();
        $cost_types = CostType::orderBy('name')->get();
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();

        return view('cost.create')->with('cost_types', $cost_types)->with('cars', $cars)->with('users', $users);
    }

    /**
     * Funkcja dodaje nowy koszt do bazy.
     * @param RequestCost $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestCost $request)
    {
        $costs = new Cost;
        $costs->cost_type_id = Input::get('cost_type_id');
        $costs->car_id = Input::get('car_id');
        $costs->user_id = Input::get('user_id');
        $costs->cost_quota = Input::get('cost_quota');
        $costs->cost_date = Input::get('cost_date');
        $costs->description = ucfirst(Input::get('description'));
        $costs->save();
        $request->session()->flash('success', 'Dodano nowy koszt pomyślnie!');

        return redirect()->route('cost.create');
    }

    /**
     * Funcja zwraca widok do edycji kosztu o danym id.
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $costs = Cost::find($id);
        $cost_types = CostType::all();
        $cars = Car::all();
        $users = User::all();

        return view('cost.edit')->with('costs', $costs)->with('cost_types', $cost_types)->with('cars', $cars)->with('users', $users);
    }

    /**
     * Funkcja edytuje w bazie Koszt o danym id.
     * @param RequestCost $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestCost $request, $id)
    {
        $costs = Cost::find($id);
        $costs->cost_type_id = Input::get('cost_type_id');
        $costs->car_id = Input::get('car_id');
        $costs->user_id = Input::get('user_id');
        $costs->cost_quota = Input::get('cost_quota');
        $costs->cost_date = Input::get('cost_date');
        $costs->description = ucfirst(Input::get('description'));
        $costs->save();
        $request->session()->flash('success', 'Edytowano nowy koszt pomyślnie!');

        return redirect()->route('cost.index');
    }

    /**
     * Funkcja usuwa z bazy koszt o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $costs = Cost::where('id', $id)->first();
        $costs->delete();
        Session::flash('success', 'Usunięto koszt pomyślnie!');

        return redirect()->route('cost.index');
    }
}
