<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRole;
use App\Http\Resources\RoleResource;
use App\Role;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa RoleAPIController odpowiada za pracę z Rolami w aplikacji Android.
 * @package App\Http\Controllers
 */
class RoleAPIController extends Controller
{
    /**
     * RoleAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:role_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:role_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca dane o wyszstkich Rolach.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return RoleResource::collection(Role::all());
    }

    /**
     * Funkcja dodaje do bazy nową Rolę.
     * @param RequestRole $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestRole $request)
    {
        $role = new Role;
        $role->name = ucfirst($request->name);
        $role->display_name = ucfirst($request->display_name);
        $role->description = ucfirst($request->description);
        $role->save();

        return response([
            'data' => new RoleResource($role)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane o Roli o podanym id.
     * @param $id
     * @return RoleResource
     */
    public function show($id)
    {
        return new RoleResource(Role::find($id));
    }

    /**
     * Funkcja edytuje w bazie Rolę o danym id.
     * @param RequestRole $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestRole $request, $id)
    {
        $role = Role::find($id);
        $role->name = ucfirst($request->name);
        $role->display_name = ucfirst($request->display_name);
        $role->description = ucfirst($request->description);
        $role->save();

        return response([
            'data' => new RoleResource($role)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy Rolę o danym id.
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
