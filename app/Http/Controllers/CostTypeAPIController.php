<?php

namespace App\Http\Controllers;

use App\CostType;
use App\Http\Requests\RequestCostType;
use App\Http\Resources\CostTypeResource;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa CostTypeAPIController odpowiada za pracę z Typami kosztów w aplikacji Android.
 * @package App\Http\Controllers
 */
class CostTypeAPIController extends Controller
{
    /**
     * CostTypeAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:cost_type_index', ['only' => ['show']]);
        $this->middleware('permission:cost_type_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:cost_type_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:cost_type_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca dane o wyszstkich typach kosztów.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CostTypeResource::collection(CostType::all());
    }

    /**
     * Funkcja dodaje do bazy nowy typ kosztu.
     * @param RequestCostType $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestCostType $request)
    {
        $cost_type = new CostType;
        $cost_type->name = ucfirst($request->name);
        $cost_type->save();

        return response([
            'data' => new CostTypeResource($cost_type)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane o typie kosztu o podanym id.
     * @param $id
     * @return CostTypeResource
     */
    public function show($id)
    {
        return new CostTypeResource(CostType::find($id));
    }

    /**
     * Funkcja edytuje w bazie Typ kosztu o danym id
     * @param RequestCostType $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestCostType $request, $id)
    {
        $cost_type = CostType::find($id);
        $cost_type->name = ucfirst($request->name);
        $cost_type->save();

        return response([
            'data' => new CostTypeResource($cost_type)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy typ kosztu o danym id.
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $cost_type = CostType::find($id);
        $cost_type->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
