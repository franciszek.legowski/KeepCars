<?php

namespace App\Http\Controllers;

use App\OAuth;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

/**
 * Class TokenController
 * @package App\Http\Controllers
 */
class TokenController extends Controller
{
    /**
     * index
     * @return $this
     */
    public function index()
    {
        $token = OAuth::where('user_id', Auth::id());
        $token = $token->first();
        return view('user.token')->with(['token' => $token]);
    }

    /**
     * Funkcja pobiera mail, zwraca cli_id, us_id i secret.
     * @return string
     */
    public function secret()
    {
        $userEmail = Input::get('email');
        $user_id = User::where('email', $userEmail);
        $user_id = $user_id->first();
        if (!empty($user_id)) {
            $token = OAuth::where('user_id', $user_id->id);
            $token = $token->first();
            return json_encode(['client_id' => $token->id, 'user_id' => $token->user_id, 'secret' => $token->secret]);
        } else {
            return json_encode(['errors' => 'Nie ma takiego emaila']);
        }
    }
}
