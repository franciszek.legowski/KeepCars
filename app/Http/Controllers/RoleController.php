<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRole;
use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa RoleController posiada funkcje pracujące z Rolami pracowników.
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{
    /**
     * RoleController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:role_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:role_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Roli pracowników.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $display_name = Input::has('search_display_name') ? Input::get('search_display_name') : null;
        $description = Input::has('search_description') ? Input::get('search_description') : null;

        if (isset($display_name)) {
            $roles = DB::table('roles')
                ->select('roles.id as id',
                    'roles.name as name',
                    'roles.display_name as display_name',
                    'roles.description as description'
                )
                ->where('roles.display_name', 'LIKE', '%' . $display_name . '%')
                ->whereNull('roles.deleted_at')
                ->orderBy('roles.id', 'asc')
                ->paginate(15);
        } else if (isset($description)) {
            $roles = DB::table('roles')
                ->select('roles.id as id',
                    'roles.name as name',
                    'roles.display_name as display_name',
                    'roles.description as description'
                )
                ->where('roles.description', 'LIKE', '%' . $description . '%')
                ->whereNull('roles.deleted_at')
                ->orderBy('roles.id', 'asc')
                ->paginate(15);
        } else {
            $roles = DB::table('roles')
                ->select('roles.id as id',
                    'roles.name as name',
                    'roles.display_name as display_name',
                    'roles.description as description'
                )
                ->whereNull('roles.deleted_at')
                ->orderBy('roles.id', 'asc')
                ->paginate(15);
        }

        return view('role.index', ['roles' => $roles]);
    }

    /**
     * Funkcja zwraca widok do tworzenia Roli.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('role.create');
    }

    /**
     * Funkcja dodaje nową Rolę do bazy.
     * @param RequestRole $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestRole $request)
    {
        $role = new Role;
        $role->name = $request->name;
        $role->display_name = ucfirst($request->display_name);
        $role->description = ucfirst($request->description);
        $role->save();
        $request->session()->flash('success', 'Dodano nową rolę pomyślnie!');

        return redirect()->route('role.create');
    }

    /**
     * Funcja zwraca widok do edycji Roli o danym id.
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $role = Role::find($id);

        return view('role.edit')->with('role', $role);
    }

    /**
     * Funkcja edytuje w bazie Rolę o danym id.
     * @param RequestRole $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestRole $request, $id)
    {
        $role = Role::find($id);
        $role->name = $request->name;
        $role->display_name = ucfirst($request->display_name);
        $role->description = ucfirst($request->description);
        $role->save();
        $request->session()->flash('success', 'Edytowano rolę pomyślnie!');

        return redirect()->route('role.index');
    }

    /**
     * Funkcja usuwa z bazy Rolę o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $roles = Role::where('id', $id)->first();
        $roles->delete();
        Session::flash('success', 'Usunięto rolę ' . $roles->display_name . '!');

        return redirect()->route('role.index');
    }
}
