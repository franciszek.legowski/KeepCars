<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\RequestCar;
use App\Http\Resources\CarResource;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa CarAPIController odpowiada za pracę z Samochodami w aplikacji Android.
 * @package App\Http\Controllers
 */
class CarAPIController extends Controller
{
    /**
     * CarAPIController constructor.
     * Stpawdza czy użytkownik ma uprawnienia do korzystania z funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:car_index', ['only' => ['show']]);
        $this->middleware('permission:car_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:car_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:car_delete', ['only' => ['destroy']]);
        $this->middleware('permission:car_read_qr', ['only' => ['qr']]);
    }

    /**
     * Funkcja zwraca Samochody z bazy.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CarResource::collection(Car::all());
    }

    /**
     * Funkcja zapisuje dane samochodu do bazy.
     * @param RequestCar $request dane samochodu
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestCar $request)
    {
        $car = new Car;
        $car->brand_id = $request->brand_id;
        $car->model = ucfirst($request->model);
        $car->production_year = $request->production_year;
        $car->fuel_type_id = $request->fuel_type_id;
        $car->vin = strtoupper($request->vin);
        $car->registration_number = strtoupper($request->registration_number);
        $car->mileage = $request->mileage;
        $car->save();

        return response([
            'data' => new CarResource($car)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane konkretnego samochodu.
     * @param int $id indeks samochodu
     * @return CarResource
     */
    public function show($id)
    {
        return new CarResource(Car::find($id));
    }

    /**
     * Funkcja edytuje dane samochodu z bazy.
     * @param RequestCar $request
     * @param int $id indeks samochodu
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestCar $request, $id)
    {
        $car = Car::find($id);
        $car->brand_id = $request->brand_id;
        $car->model = ucfirst($request->model);
        $car->production_year = $request->production_year;
        $car->fuel_type_id = $request->fuel_type_id;
        $car->vin = strtoupper($request->vin);
        $car->registration_number = strtoupper($request->registration_number);
        $car->mileage = $request->mileage;
        $car->save();

        return response([
            'data' => new CarResource($car)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa samochód z bazy.
     * @param int $id indeks samochodu
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $car = Car::find($id);
        $car->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Funkcja zwraca indeks samochodu o podanym vinie.
     * @return string
     */
    public function qr()
    {
        $carVIN = Input::get('VIN');
        $car_id = Car::where('VIN', $carVIN);
        $car_id = $car_id->first();

        if (!empty($car_id)) {
            return json_encode(['car_id' => $car_id->id]);
        } else {
            return json_encode(['errors' => 'Nie ma takiego samochodu']);
        }
    }
}
