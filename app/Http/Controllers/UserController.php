<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestUser;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa UserController posiada funkcje pracujące z Pracownikami.
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * UserController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:user_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:user_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Pracowników.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $role = Input::has('search_role') ? Input::get('search_role') : null;
        $name = Input::has('search_name') ? Input::get('search_name') : null;
        $surname = Input::has('search_surname') ? Input::get('search_surname') : null;
        $email = Input::has('search_email') ? Input::get('search_email') : null;

        if (isset($role)) {
            $users = DB::table('users')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->orderBy('users.id', 'asc')
                ->select('users.id as id',
                    'roles.display_name as role',
                    'users.name as name',
                    'users.surname as surname',
                    'users.email as email'
                )
                ->where('roles.display_name', 'LIKE', '%' . $role . '%')
                ->whereNull('users.deleted_at')
                ->paginate(15);
        } else if (isset($name)) {
            $users = DB::table('users')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->orderBy('users.id', 'asc')
                ->select('users.id as id',
                    'roles.display_name as role',
                    'users.name as name',
                    'users.surname as surname',
                    'users.email as email'
                )
                ->where('users.name', 'LIKE', '%' . $name . '%')
                ->whereNull('users.deleted_at')
                ->paginate(15);
        } else if (isset($surname)) {
            $users = DB::table('users')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->orderBy('users.id', 'asc')
                ->select('users.id as id',
                    'roles.display_name as role',
                    'users.name as name',
                    'users.surname as surname',
                    'users.email as email'
                )
                ->where('users.surname', 'LIKE', '%' . $surname . '%')
                ->whereNull('users.deleted_at')
                ->paginate(15);
        } else if (isset($email)) {
            $users = DB::table('users')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->orderBy('users.id', 'asc')
                ->select('users.id as id',
                    'roles.display_name as role',
                    'users.name as name',
                    'users.surname as surname',
                    'users.email as email'
                )
                ->where('users.email', 'LIKE', '%' . $email . '%')
                ->whereNull('users.deleted_at')
                ->paginate(15);
        } else {
            $users = DB::table('users')
                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                ->orderBy('users.id', 'asc')
                ->select('users.id as id',
                    'roles.display_name as role',
                    'users.name as name',
                    'users.surname as surname',
                    'users.email as email'
                )
                ->whereNull('users.deleted_at')
                ->paginate(15);
        }
        return view('user.index', ['users' => $users]);
    }

    /**
     * create
     * @return $this
     */
    public function create()
    {
        $role = Role::orderBy('display_name')->get();

        return view('user.create')->with('role', $role);
    }

    /**
     * Funkcja dodaje nowego Pracownika do bazy.
     * @param RequestUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestUser $request)
    {
        $user = new User();
        $user->role_id = $request->role_id;
        $user->name = ucfirst($request->name);
        $user->surname = ucfirst($request->surname);
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $request->session()->flash('success', 'Dodano nowego użytkownika pomyślnie!');

        return redirect()->route('user.create');
    }

    /**
     * Funcja zwraca widok do edycji Pracownika o danym id.
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $users = User::find($id);
        $roles = Role::orderBy('display_name')->get();

        return view('user.edit')->with('users', $users)->with('roles', $roles);
    }

    /**
     * Funkcja edytuje w bazie Pracownika o danym id.
     * @param RequestUser $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestUser $request, $id)
    {
        $users = User::find($id);
        $users->role_id = Input::get('role_id');
        $users->name = ucfirst(Input::get('name'));
        $users->surname = ucfirst(Input::get('surname'));
        $users->email = Input::get('email');
        $users->save();
        $request->session()->flash('success', 'Edytowano użytkownika pomyślnie!');

        return redirect()->route('user.index');
    }

    /**
     * Funkcja usuwa z bazy Pracownika o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $users = User::where('id', $id)->first();
        $users->delete();
        Session::flash('success', 'Usunięto użytkownika pomyślnie!' . $users->name);

        return redirect()->route('user.index');
    }

}