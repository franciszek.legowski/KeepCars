<?php

namespace App\Http\Controllers;

use App\{
    Refuel
};
use Illuminate\Support\Facades\DB;

/**
 * Klasa ChartsController zawiera funkcje wykorzystywane do tworzenia wykresów i liczenia danych statystycznych.
 * @package App\Http\Controllers
 */
class ChartsController extends Controller
{
    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function avgWydatkiPaliwo_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('avg(refuel_quota) as koszt'))
            ->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return float|int
     */
    public function avg_spalanie_car($car_id, $data_from, $data_to)
    {
        $km = $this->przejechane_kilometry_car($car_id, $data_from, $data_to);
        if ($km == 0)
            return 0;
        $koszt = $this->suma_zatankowawe_litry_car($car_id, $data_from, $data_to);
        return round(100 * $koszt / $km, 2);
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function przejechane_kilometry_car($car_id, $data_from, $data_to)
    {
//        wersja 2
        $dane = DB::table('rides')->select(DB::raw('sum(end_mileage) - sum(begin_mileage) as km'))
            ->where('car_id', $car_id)->whereBetween('ride_date', [$data_from, $data_to])->first();
//        return [$car_id, $data_from, $data_to, $dane];
        return $dane->km ? $dane->km : 0;

        $km_przed = DB::table('refuels')->select('mileage')->where('car_id', $car_id)
            ->where('refuel_date', '<', $data_from)
            ->orderBy('refuel_date', 'dsc')->first();

        if ($km_przed == NULL)
            $km_przed = DB::table('refuels')->select('mileage')->where('car_id', $car_id)
                ->where('refuel_date', '>=', $data_from)->orderBy('refuel_date')->first();
        $km_przed = $km_przed ? $km_przed->mileage : 0;

        $ostatni_km = DB::table('refuels')->select('mileage')->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])
            ->orderBy('refuel_date', 'dsc')->first();
        $ostatni_km = $ostatni_km ? $ostatni_km->mileage : $km_przed;

        return $ostatni_km - $km_przed;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function suma_zatankowawe_litry_car($car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(amount_of_litre) as litry'))
            ->where('car_id', $car_id)->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->litry ? $dane->litry : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return float|int
     */
    public function avg_spalanie_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $km = $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from, $data_to);
//        return $km;
        if ($km == 0)
            return 0;
        $koszt = $this->suma_zatankowawe_litry_car_kierowca($user_id, $car_id, $data_from, $data_to);
        return round(100 * $koszt / $km, 2);
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        //2 wersja
        $dane = DB::table('rides')->select(DB::raw('sum(begin_mileage) as km1'), DB::raw('sum(end_mileage) as km2'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('ride_date', [$data_from, $data_to])->first();
        return $dane->km1 ? $dane->km2 - $dane->km1 : 0;


//        return -1000;
        $dane = DB::table('refuels')->select('user_id', 'mileage')->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])
            ->orderBy('refuel_date')->get();
        if (count($dane))  // Jeżeli ktoś zrobił tankowanie w okresie
        {
//            return [$dane, $data_from, $data_to];
            $suma = 0;
            if ($dane[0]->user_id == $user_id) // Jeżeli pierwsze tankowanie było kierowcy $user_id pobierz wcześniejsze
            {
//                return -3000;
                $poprzednie = DB::table('refuels')->select('mileage')
                    ->where('car_id', $car_id)
                    ->where('refuel_date', '<', $data_from)
                    ->orderBy('refuel_date', 'dsc')->first();
                if ($poprzednie) //Było tankowanie przed okresem
                {
//                    return -10;
                    $poprzednie = $poprzednie->mileage;
                } else //Nie było tankowania przed okresem
                {
//                    return -20;
                    $poprzednie = $dane[0]->mileage;
                }
//                return -30;
//                return $poprzednie;
                foreach ($dane as $tankowanie) {
                    if ($tankowanie->user_id == $user_id) {
//                        return -60;
                        $suma += $tankowanie->mileage - $poprzednie;
                        $poprzednie = $tankowanie->mileage;
                    } else {
//                        return -70;
                        $poprzednie = $tankowanie->mileage;
                    }
                }
//                return -80;
                return $suma;
            } else // Pierwsze tankowanie w okresie zrobione przez kogoś innego
            {
//                return -4000;
//                return -40;
                $poprzednie = $dane[0]->mileage;
                foreach ($dane as $tankowanie) {
//                    return -90;
                    if ($tankowanie->user_id == $user_id) {
//                        return -100;
                        $suma += $tankowanie->mileage - $poprzednie;
                        $poprzednie = $tankowanie->mileage;
                    } else {
//                        return -110;
                        $poprzednie = $tankowanie->mileage;
                    }
                }
//                return -120;
                return $suma;
            }
        } else
//            return 10000;
            return 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function suma_zatankowawe_litry_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(amount_of_litre) as litry'))
            ->where('car_id', $car_id)->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->litry ? $dane->litry : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return float|int
     */
    public function avg_trasa_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $kms = [];
        while ($data_from <= $data_to) {
            $km = $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from,
                date("Y-m-d", strtotime("{$data_from} + 1 day")));
            if ($km > 0)
                array_push($kms, $km);
            $data_from = date("Y-m-d", strtotime("{$data_from} + 1 day"));
        }
        return count($kms) ? round(array_sum($kms) / count($kms), 2) : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return float|int
     */
    public function avg_wydatkiPaliwo_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('avg(refuel_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? round($dane->koszt, 2) : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return float
     */
    public function avg_wydatki_car($car_id, $data_from, $data_to)
    {
        return round($this->wydatki_paliwo_car($car_id, $data_from, $data_to)
            + $this->wydatki_serwisowe_car($car_id, $data_from, $data_to)
            / $this->ile_miesiecy_roznicy($data_from, $data_to), 2);
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_paliwo_car($car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(refuel_quota) as koszt'))
            ->where('car_id', $car_id)->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_serwisowe_car($car_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
            ->where('car_id', $car_id)->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return mixed
     */
    public function ile_miesiecy_roznicy($data_from, $data_to)
    {
        $m1 = date("m", strtotime($data_from));
        $m2 = date('m', strtotime($data_to));
        $y1 = date("Y", strtotime($data_from));
        $y2 = date('Y', strtotime($data_to));
//        $d1 = date('d', strtotime($data_from));
//        $d2 = date('d', strtotime($data_to));
        return max(12 * ($y2 - $y1) + $m2 - $m1, 1);
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return float|int
     */
    public function avg_wydatki_serwisowe_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first()->koszt;
        $dane2 = DB::table('costs')->select(DB::raw('count(cost_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first()->koszt;
        return $dane2 > 0 ? round($dane / $dane2, 2) : 0;
    }

    /**
     *  * Show a sample chart.
     *  *
     *  * @return Response
     *  */

    public function chart()
    {
        return view('chart_view');
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function daty($data_from, $data_to, $step, $interval)
    {
        $daty = [];
        while ($data_from <= $data_to) {
            array_push($daty, $data_from);
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $daty;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function daty_km_wszystkie_dni($car_id, $data_from, $data_to, $step, $interval)
    {
        $daty = [];
        $mileages = [];
        $km = Refuel::select('mileage')
            ->where('car_id', $car_id)
            ->where('refuel_date', '<', $data_from)
            ->orderBy('refuel_date', 'dsc')
            ->first();
        array_push($mileages, $km['mileage'] ? $km['mileage'] : -1);
        while ($data_from <= $data_to) {
            $km = Refuel::select('mileage')
                ->where('car_id', $car_id)
                ->whereBetween('refuel_date', [$data_from, date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))])
                ->orderBy('refuel_date', 'dsc')
                ->first();
            array_push($mileages, $km['mileage'] ? $km['mileage'] : end($mileages));
            array_push($daty, $data_from);
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }

        $indeks = count($mileages);
        while ($indeks > 2) {
            $indeks--;
            if ($mileages[$indeks - 1] > -1)
                $mileages[$indeks] = $mileages[$indeks] - $mileages[$indeks - 1];
        }
        $mileages[1] = $mileages[0] == -1 ? 0 : $mileages[1] - $mileages[0];
        array_shift($mileages);
        return ['daty' => $daty, 'km' => $mileages];
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return string
     */
    public function daty_litry($car_id, $data_from, $data_to, $step, $interval)
    {
        if ($data_from == NULL)
            $data_from = '2018-03-01';
        if ($data_to == NULL)
            $data_to = date('Y-m-d', strtotime("{$data_from} +{$step} {$interval}"));
        $lts = Refuel::select('refuel_date', DB::raw('amount_of_litre as litry_dzien'))
            ->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])
            ->orderBy('refuel_date')
            ->get();
        $daty = [];
        $litry = [];
        foreach ($lts as $key => $lt) {
            array_push($litry, $lt->litry_dzien ? $lt->litry_dzien : 0);
            array_push($daty, date('Y-m-d', strtotime($lt->refuel_date)));
        }

        return json_encode(['daty' => $daty, 'litry' => $litry]);
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_tankowan_car($car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('count(*) as ilosc'))->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_tankowan_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('count(*) as ilosc'))
            ->where('car_id', $car_id)->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_tankowan_flota($data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('count(*) as ilosc'))
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_tankowan_flota_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('count(*) as ilosc'))->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_wydatkow_serwisowych_car($car_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('count(*) as ilosc'))->where('car_id', $car_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_wydatkow_serwisowych_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('count(*) as ilosc'))
            ->where('car_id', $car_id)->where('user_id', $user_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_wydatkow_serwisowych_flota($data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('count(*) as ilosc'))
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function ilosc_wydatkow_serwisowych_flota_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('count(*) as ilosc'))->where('user_id', $user_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->ilosc ? $dane->ilosc : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @return string
     */
    public function kolowy_tankowania_inne($car_id)
    {
        $tankowania = [];
        $inne = [];
        $dane = DB::table('refuels')->select(DB::raw("sum(refuel_quota) as litry"))
            ->where('car_id', $car_id)
            ->first();
        array_push($tankowania, $dane->litry ? $dane->litry : 0);

        $dane = DB::table('costs')->selectRaw('sum(cost_quota) as koszt')
            ->where('car_id', $car_id)
            ->first();
        array_push($inne, $dane->koszt ? $dane->koszt : 0);
        return json_encode(['tankowania' => $tankowania, 'inne' => $inne]);
    }

    /**
     * @return string
     */
    public function losuj_kolor()
    {
        $z = '0123456789abcdef';
        return "#{$z[rand(0, 15)]}{$z[rand(0, 15)]}{$z[rand(0, 15)]}
        {$z[rand(0, 15)]}{$z[rand(0, 15)]}{$z[rand(0, 15)]}";
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function maxTrasa_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $kms = [];
        while ($data_from <= $data_to) {
            $km = $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from,
                date("Y-m-d", strtotime("{$data_from} + 1 day")));
            if ($km > 0)
                array_push($kms, $km);
            $data_from = date("Y-m-d", strtotime("{$data_from} + 1 day"));
        }
        return count($kms) ? max($kms) : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function maxWydatkiPaliwo_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('max(refuel_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function maxWydatkiPaliwo_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('max(refuel_quota) as koszt'))
            ->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function minTrasa_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $kms = [];
        while ($data_from <= $data_to) {
            $km = $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from,
                date("Y-m-d", strtotime("{$data_from} + 1 day")));
            if ($km > 0)
                array_push($kms, $km);
            $data_from = date("Y-m-d", strtotime("{$data_from} + 1 day"));
        }
        return count($kms) ? min($kms) : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function minWydatkiPaliwo_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('min(refuel_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function minWydatkiPaliwo_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('min(refuel_quota) as koszt'))
            ->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int
     */
    public function przejechane_kilometry_car_inni($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('rides')->select(DB::raw(
            'sum(end_mileage) - sum(begin_mileage) as km'))->where('user_id', '<>', $user_id)
            ->where('car_id', $car_id)->whereBetween('ride_date', [$data_from, $data_to])->first();
        return $dane->km ? intval($dane->km) : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return array
     */
    public function samochody_ktorymi_jezdzil($user_id, $data_from, $data_to)
    {
        $cars = [];
        $dane = DB::table('rides')->select('car_id')->where('user_id', $user_id)
            ->whereBetween('ride_date', [$data_from, $data_to])
            ->groupBy('car_id')
            ->get();
        foreach ($dane as $value) {
            array_push($cars, $value->car_id);
        }
        return $cars;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function suma_zatankowawe_litry_flota($data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(amount_of_litre) as litry'))
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->litry ? $dane->litry : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function suma_zatankowawe_litry_flota_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(amount_of_litre) as litry'))
            ->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->litry ? $dane->litry : 0;
    }

    /**
     * @param $typ
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_serwisowe_car_typ($typ, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
            ->where('car_id', $car_id)->where('cost_type_id', $typ)
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return array
     */
    public function wykresKolowyPodzialuKosztowNaRodzaje($car_id, $data_from, $data_to)
    {
        $serwis = ['typ' => [], 'koszt' => []];
        $dane = DB::table('cost_types')->select('id', 'name')->get();
        foreach ($dane as $id) {
            $dane2 = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
                ->where('cost_type_id', $id->id)->where('car_id', $car_id)
                ->whereBetween('cost_date', [$data_from, $data_to])
                ->first();
            array_push($serwis['typ'], $id->name);
            array_push($serwis['koszt'], $dane2->koszt ? $dane2->koszt : 0);
        }
        return $serwis;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return array
     */
    public function wykresKolowyPodzialuKosztowNaRodzaje_flota($data_from, $data_to)
    {
        $serwis = ['typ' => [], 'koszt' => []];
        $dane = DB::table('cost_types')->select('id', 'name')->get();
        foreach ($dane as $id) {
            $dane2 = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
                ->where('cost_type_id', $id->id)
                ->whereBetween('cost_date', [$data_from, $data_to])
                ->first();
            array_push($serwis['typ'], $id->name);
            array_push($serwis['koszt'], $dane2->koszt ? $dane2->koszt : 0);
        }
        return $serwis;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return array
     */
    public function wykresKolowyPodzialuKosztowNaRodzaje_flota_kierowca($user_id, $data_from, $data_to)
    {
        $serwis = ['typ' => [], 'koszt' => []];
        $dane = DB::table('cost_types')->select('id', 'name')->get();
        foreach ($dane as $id) {
            $dane2 = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
                ->where('cost_type_id', $id->id)->where('user_id', $user_id)
                ->whereBetween('cost_date', [$data_from, $data_to])
                ->first();
            array_push($serwis['typ'], $id->name);
            array_push($serwis['koszt'], $dane2->koszt ? $dane2->koszt : 0);
        }
        return $serwis;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return array
     */
    public function wykresKolowyPodzialuKosztowNaRodzaje_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $serwis = ['typ' => [], 'koszt' => []];
        $dane = DB::table('cost_types')->select('id', 'name')->get();
        foreach ($dane as $id) {
            $dane2 = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
                ->where('cost_type_id', $id->id)->where('car_id', $car_id)
                ->where('user_id', $user_id)->whereBetween('cost_date', [$data_from, $data_to])
                ->first();
            array_push($serwis['typ'], $id->name);
            array_push($serwis['koszt'], $dane2->koszt ? $dane2->koszt : 0);
        }
        return $serwis;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return array
     */
    public function wykresKolowyPrzejechanychKilometrowPojazdy_kierowca($user_id, $data_from, $data_to)
    {
        $samochody = [];
        $kilometry = [];
        $cars_id = [];
        $dane = DB::table('rides')->select('car_id')->where('user_id', $user_id)->whereBetween('ride_date', [$data_from, $data_to])->get();
        foreach ($dane as $value)
            array_push($cars_id, $value->car_id);
        $dane = DB::table('cars_view')->select('id', 'marka', 'model', 'registration_number')
            ->whereIn('id', $cars_id)
            ->orderBy('marka')->orderBy('model')->orderBy('registration_number')->get();
        foreach ($dane as $value) {
            array_push($samochody, $value->marka . " " . $value->model . " " . $value->registration_number);
            array_push($kilometry, $this->przejechane_kilometry_car_kierowca($user_id, $value->id, $data_from, $data_to));
        }
        return ['samochody' => $samochody, 'kilometry' => $kilometry];
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyKosztowSerwisowych($car_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $inne = [];
        $daty = [];
        while ($data_from <= $data_to) {
            $dane = DB::table('costs')->selectRaw('sum(cost_quota) as koszt')
                ->where('car_id', $car_id)
                ->whereBetween('cost_date', [$data_from,
                    date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))])
                ->first();
            array_push($inne, $dane->koszt ? floatval($dane->koszt) : 0);
            array_push($daty, $data_from);
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return ['inne' => $inne, 'daty' => $daty];
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyKosztowSerwisowych_flota($data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $inne = [];
        while ($data_from <= $data_to) {
            array_push($inne,
                $this->wydatki_serwisowe_flota($data_from,
                    date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $inne;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_serwisowe_flota($data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyKosztowSerwisowych_flota_kierowca($user_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $inne = [];
        $cars = DB::table('cars')->select('id')->get();
        while ($data_from <= $data_to) {
            foreach ($cars as $car) {
                array_push($inne,
                    $this->wydatki_serwisowe_flota_kierowca($user_id, $data_from,
                        date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            }
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $inne;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_serwisowe_flota_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
            ->where('user_id', $user_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyKosztowSerwisowych_kierowca($user_id, $car_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $inne = [];
        while ($data_from <= $data_to) {
            array_push($inne,
                $this->wydatki_serwisowe_car_kierowca($user_id, $car_id, $data_from,
                    date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $inne;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_serwisowe_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('costs')->select(DB::raw('sum(cost_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('cost_date', [$data_from, $data_to])->first();
        return $dane->koszt > 0 ? $dane->koszt : 0;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyLitry_car($car_id, $data_from, $data_to, $step, $interval)
    {
        $litry = [];
        while ($data_from <= $data_to) {
            array_push($litry, $this->suma_zatankowawe_litry_car($car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $litry;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyLitry_car_kierowca($user_id, $car_id, $data_from, $data_to, $step, $interval)
    {
        $litry = [];
        while ($data_from <= $data_to) {
            array_push($litry, $this->suma_zatankowawe_litry_car($car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $litry;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyPrzebieguKilometrow_car($car_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $kilometry = [];
        while ($data_from <= $data_to) {
            array_push($kilometry, $this->przejechane_kilometry_car($car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $kilometry;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyPrzebieguKilometrow_car_kierowca($user_id, $car_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $kilometry = [];
        while ($data_from <= $data_to) {
            array_push($kilometry, $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $kilometry;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyPrzebieguKilometrow_flota($data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $kilometry = [];
        $dane = DB::table('cars')->select('id')->get();
        while ($data_from <= $data_to) {
            array_push($kilometry, $this->przejechane_kilometry_flota($data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $kilometry;
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return mixed
     */
    public function przejechane_kilometry_flota($data_from, $data_to)
    {
//        $dane = DB::table('cars')->select('id')->get();
//        $car_ids = [];
//        foreach ($dane as $car) {
//            array_push($car_ids, $car->id);
//        }
//        $suma_km = 0;
//        foreach ($car_ids as $car_id) {
//            $suma_km += $this->przejechane_kilometry_car($car_id, $data_from, $data_to);
//        }
//        return $suma_km;
        return DB::table('rides')->select(DB::raw('sum(end_mileage) - sum(begin_mileage) as km'))
            ->whereBetween('ride_date', [$data_from, $data_to])->first()->km;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyPrzebieguKilometrow_flota_kierowca($user_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $kilometry = [];
        while ($data_from <= $data_to) {
            array_push($kilometry, $this->przejechane_kilometry_flota_kierowca($user_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $kilometry;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int
     */
    public function przejechane_kilometry_flota_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('rides')->select(DB::raw('sum(end_mileage) - sum(begin_mileage) as km'))
            ->where('user_id', $user_id)
            ->whereBetween('ride_date', [$data_from, $data_to])
            ->get()->first();
//        return ['user'=>$user_id, 'dane'=>$dane];
        return $dane->km ? $dane->km : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowyPrzebieguKilometrow_kierowca($user_id, $car_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $kilometry = [];
        while ($data_from <= $data_to) {
            array_push($kilometry, $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $kilometry;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowySrednieSpalanie_car($car_id, $data_from, $data_to, $step, $interval)
    {
        $srednie = [];
        while ($data_from <= $data_to) {
            $km = $this->przejechane_kilometry_car($car_id, $data_from,
                date("Y-m-d", strtotime("{$data_from} + {$step} {$interval}")));
//            return $km;
            if ($km) {
                $l = $this->suma_zatankowawe_litry_car($car_id, $data_from,
                    date("Y-m-d", strtotime("{$data_from} + {$step} {$interval}")));
                array_push($srednie, round(100 * $l / $km, 2));
            } else
                array_push($srednie, "NAN");

            $data_from = date("Y-m-d", strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $srednie;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresLiniowySrednieSpalanie_car_kierowca($user_id, $car_id, $data_from, $data_to, $step, $interval)
    {
        $srednie = [];
        while ($data_from <= $data_to) {
            $km = $this->przejechane_kilometry_car_kierowca($user_id, $car_id, $data_from,
                date("Y-m-d", strtotime("{$data_from} + {$step} {$interval}")));
            if ($km) {
                $l = $this->suma_zatankowawe_litry_car_kierowca($user_id, $car_id, $data_from,
                    date("Y-m-d", strtotime("{$data_from} + {$step} {$interval}")));
                array_push($srednie, round(100 * $l / $km, 2));
            } else
                array_push($srednie, "NAN");

            $data_from = date("Y-m-d", strtotime("{$data_from} + {$step} {$interval}"));
        }
        return $srednie;
    }

    /**
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresSlupkowyKosztowPaliwowychOrazSerwisowych($car_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $paliwo = [];
        $serwis = [];
        while ($data_from <= $data_to) {
            array_push($serwis, $this->wydatki_serwisowe_car($car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            array_push($paliwo, $this->wydatki_paliwo_car($car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return ['serwis' => $serwis, 'paliwo' => $paliwo];
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresSlupkowyKosztowPaliwowychOrazSerwisowych_flota($data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $paliwo = [];
        $serwis = [];
        while ($data_from <= $data_to) {
            array_push($paliwo, $this->wydatki_paliwo_flota($data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            array_push($serwis, $this->wydatki_serwisowe_flota($data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));

            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return ['paliwo' => $paliwo, 'serwis' => $serwis];
    }

    /**
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_paliwo_flota($data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(refuel_quota) as koszt'))
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresSlupkowyKosztowPaliwowychOrazSerwisowych_flota_kierowca($user_id, $data_from, $data_to, $step, $interval)
    {
        $data_from = date('Y-m-d', strtotime($data_from));
        $data_to = date('Y-m-d', strtotime($data_to));
        $paliwo = [];
        $serwis = [];
        while ($data_from <= $data_to) {
            array_push($paliwo, $this->wydatki_paliwo_flota_kierowca($user_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            array_push($serwis, $this->wydatki_serwisowe_flota_kierowca($user_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));

            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return ['paliwo' => $paliwo, 'serwis' => $serwis];
    }

    /**
     * @param int $user_id indeks pracownika
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_paliwo_flota_kierowca($user_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(refuel_quota) as koszt'))
            ->where('user_id', $user_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
        return $dane->koszt ? $dane->koszt : 0;
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @param int $step ilość skoku daty
     * @param string $interval rozmiar skoku daty ['day'|'week'|'month'|'year']
     * @return array
     */
    public function wykresSlupkowyKosztowPaliwowychOrazSerwisowych_kierowca($user_id, $car_id, $data_from, $data_to, $step, $interval)
    {
        $paliwo = [];
        $serwis = [];
        while ($data_from <= $data_to) {
            array_push($serwis, $this->wydatki_serwisowe_car_kierowca($user_id, $car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            array_push($paliwo, $this->wydatki_paliwo_car_kierowca($user_id, $car_id, $data_from,
                date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"))));
            $data_from = date('Y-m-d', strtotime("{$data_from} + {$step} {$interval}"));
        }
        return ['serwis' => $serwis, 'paliwo' => $paliwo];
    }

    /**
     * @param int $user_id indeks pracownika
     * @param int $car_id indeks samochodu
     * @param string $data_from początek okresu
     * @param string $data_to koniec okresu
     * @return int|mixed
     */
    public function wydatki_paliwo_car_kierowca($user_id, $car_id, $data_from, $data_to)
    {
        $dane = DB::table('refuels')->select(DB::raw('sum(refuel_quota) as koszt'))
            ->where('user_id', $user_id)->where('car_id', $car_id)
            ->whereBetween('refuel_date', [$data_from, $data_to])->first();
//        return ['u_id'=>$user_id, 'c_id'=>$car_id, 'daty'=>$data_from." - ".$data_to, 'dane'=>$dane];
        return $dane->koszt ? $dane->koszt : 0;
    }

}