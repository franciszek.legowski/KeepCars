<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\RequestRefuel;
use App\Refuel;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Class RefuelController  posiada funkcje pracujące z Tankowaniami.
 * @package App\Http\Controllers
 */
class RefuelController extends Controller
{
    /**
     * RefuelController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:refuel_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:refuel_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:refuel_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:refuel_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Tankowań.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $car = Input::has('search_car') ? Input::get('search_car') : null;
        $user = Input::has('search_user') ? Input::get('search_user') : null;
        $refuel_quota_from = Input::has('search_refuel_quota_from') ? Input::get('search_refuel_quota_from') : null;
        $refuel_quota_to = Input::has('search_refuel_quota_to') ? Input::get('search_refuel_quota_to') : null;
        $price_of_litre_from = Input::has('search_price_of_litre_from') ? Input::get('search_price_of_litre_from') : null;
        $price_of_litre_to = Input::has('search_price_of_litre_to') ? Input::get('search_price_of_litre_to') : null;
        $amount_of_litre_from = Input::has('search_amount_of_litre_from') ? Input::get('search_amount_of_litre_from') : null;
        $amount_of_litre_to = Input::has('search_amount_of_litre_to') ? Input::get('search_amount_of_litre_to') : null;
        $refuel_date_from = Input::has('search_refuel_date_from') ? Input::get('search_refuel_date_from') : null;
        $refuel_date_to = Input::has('search_refuel_date_to') ? Input::get('search_refuel_date_to') : null;
        $mileage_from = Input::has('search_mileage_from') ? Input::get('search_mileage_from') : null;
        $mileage_to = Input::has('search_mileage_to') ? Input::get('search_mileage_to') : null;

        if (isset($car)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->where('brands.name', 'LIKE', '%' . $car . '%')
                ->orWhere('cars.model', 'LIKE', '%' . $car . '%')
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else if (isset($user)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->where('users.name', 'LIKE', '%' . $user . '%')
                ->orWhere('users.surname', 'LIKE', '%' . $user . '%')
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else if (isset($refuel_quota_from) and isset($refuel_quota_to)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->whereBetween('refuels.refuel_quota', [$refuel_quota_from, $refuel_quota_to])
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else if (isset($price_of_litre_from) and isset($price_of_litre_to)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->whereBetween('refuels.price_of_litre', [$price_of_litre_from, $price_of_litre_to])
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else if (isset($amount_of_litre_from) and isset($amount_of_litre_to)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->whereBetween('refuels.amount_of_litre', [$amount_of_litre_from, $amount_of_litre_to])
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else if (isset($refuel_date_from) and isset($refuel_date_to)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->whereBetween('refuels.refuel_date', [$refuel_date_from, $refuel_date_to])
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else if (isset($mileage_from) and isset($mileage_to)) {
            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->whereBetween('refuels.mileage', [$mileage_from, $mileage_to])
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        } else {

            $refuels = DB::table('refuels')
                ->leftJoin('cars', 'refuels.car_id', '=', 'cars.id')
                ->leftJoin('users', 'refuels.user_id', '=', 'users.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('refuels.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.registration_number as registration_number',
                    'users.name as name',
                    'users.surname as surname',
                    'refuels.refuel_quota as refuel_quota',
                    'refuels.price_of_litre as price_of_litre',
                    'refuels.amount_of_litre as amount_of_litre',
                    'refuels.refuel_date as refuel_date',
                    'refuels.mileage as mileage'
                )
                ->orderBy('refuel_date', 'dsc')
                ->paginate(15);
        }
        return view('refuel.index', ['refuels' => $refuels]);
    }

    /**
     * Funkcja zwraca widok do tworzenia Tankowania.
     * @return $this
     */
    public function create()
    {
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();

        return view('refuel.create')->with('allCars', $cars)->with('allUsers', $users);
    }

    /**
     * Funkcja dodaje nowe Tankowanie do bazy.
     * @param RequestRefuel $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestRefuel $request)
    {
        $refuels = new Refuel;
        $refuels->car_id = Input::get('car_id');
        $refuels->user_id = Input::get('user_id');
        $refuels->refuel_quota = Input::get('refuel_quota');
        $refuels->price_of_litre = Input::get('price_of_litre');
        $refuels->amount_of_litre = Input::get('amount_of_litre');
        $refuels->refuel_date = Input::get('refuel_date');
        $refuels->mileage = Input::get('mileage');
        $refuels->save();
        $request->session()->flash('success', 'Dodano nowe tankowanie pomyślnie!');

        return redirect()->route('refuel.create');
    }


    /**
     * Funcja zwraca widok do edycji Tankowania o danym id.
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $refuels = Refuel::find($id);
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();

        return view('refuel.edit')->with('refuels', $refuels)->with('cars', $cars)->with('users', $users);
    }

    /**
     * Funkcja edytuje w bazie Tankowanie o danym id.
     * @param RequestRefuel $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestRefuel $request, $id)
    {
        $refuels = Refuel::find($id);
        $refuels->car_id = Input::get('car_id');
        $refuels->user_id = Input::get('user_id');
        $refuels->refuel_quota = Input::get('refuel_quota');
        $refuels->price_of_litre = Input::get('price_of_litre');
        $refuels->amount_of_litre = Input::get('amount_of_litre');
        $refuels->refuel_date = Input::get('refuel_date');
        $refuels->mileage = Input::get('mileage');
        $refuels->save();
        $request->session()->flash('success', 'Edytowano tankowanie pomyślnie!');

        return redirect()->route('refuel.index');
    }

    /**
     * Funkcja usuwa z bazy Tankowanie o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $refuels = Refuel::where('id', $id)->first();
        $refuels->delete();
        Session::flash('success', 'Usunięto tankowanie pomyślnie!');

        return redirect()->route('refuel.index');
    }
}
