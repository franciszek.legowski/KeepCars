<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Klasa RegisterController odpowiada za Rejestrację użytkowników.
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    /**
     * RegisterController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Funkcja waliduje wprowadzone dane do utworzenia nowego użytkownika.
     * @param array $data Tablica z wartościami do wstawienia do bazy.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'role_id' => 'required|integer',
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Funkcja dopisuje Użytkownika do bazy.
     * @param array $data
     * @return mixed
     */
    protected function create(array $data)
    {
        return User::create([
            'role_id' => $data['role_id'],
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     *showRegistrationForm
     */
    public function showRegistrationForm()
    {
        //$role = Role::all();
        //return view('auth.register')->with('role', $role);
    }
}
