<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Support\Facades\DB;

/**
 * Klasa Car_CostController odpowiada za funkcje związane z kosztami auta.
 * @package App\Http\Controllers
 */
class Car_CostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int $id
     * @return view('car_cost.index')
     */
    public function index($id = 1)
    {
        $cars = Car::all();

        $results = DB::select('(SELECT cost_types.name as KOSZT,  cost_quota as PLN, cost_date as DAT, 
                                users.name as IMIE ,users.surname as NAZWISKO,
                                users.email as KIEROWCA  FROM costs, users, cost_types  where costs.car_id = :id1 
                                and costs.user_id = users.id and costs.cost_type_id =cost_types.id) union 
                                (SELECT \'Tankowanie\' as KOSZT,  refuel_quota as PLN, refuel_date as DAT, 
                                 users.name as IMIE ,users.surname as NAZWISKO,
                                users.email as KIEROWCA  FROM refuels, users  where refuels.car_id = :id2 
                                and refuels.user_id = users.id) ORDER BY DAT DESC limit 0,50;', ['id1' => $id, 'id2' => $id]);

        return view('car_cost.index', ['dane' => $results, 'cars' => $cars]);
    }

    /**
     * Wyświetla koszty dla danego auta.
     *
     * @param  int $id indeks auta
     * @return view('car_cost.show')
     */
    public function show($id)
    {
        $cars = Car::find($id);

        $results = DB::select('(SELECT cost_types.name as KOSZT,  cost_quota as PLN, cost_date as DAT, 
                                users.email as KIEROWCA  FROM costs, users, cost_types  where costs.car_id = :id1 
                                and costs.user_id = users.id and costs.cost_type_id =cost_types.id) union 
                                (SELECT \'Tankowanie\' as KOSZT,  refuel_quota as PLN, refuel_date as DAT, 
                                users.email as KIEROWCA  FROM refuels, users  where refuels.car_id = :id2 
                                and refuels.user_id = users.id) ORDER BY DAT DESC;', ['id1' => $id, 'id2' => $id]);

        return view('car_cost.index', ['dane' => $results, 'cars' => $cars]);
        $cars = Car::find($id);

        $results = DB::select('(SELECT cost_types.name as KOSZT,  cost_quota as PLN, cost_date as DAT, 
                                users.email as KIEROWCA  FROM costs, users, cost_types  where costs.car_id = :id1 
                                and costs.user_id = users.id and costs.cost_type_id =cost_types.id) union 
                                (SELECT \'Tankowanie\' as KOSZT,  refuel_quota as PLN, refuel_date as DAT, 
                                users.email as KIEROWCA  FROM refuels, users  where refuels.car_id = :id2 
                                and refuels.user_id = users.id) ORDER BY DAT DESC;', ['id1' => $id, 'id2' => $id]);

        return view('car_cost.show', ['dane' => $results, 'cars' => $cars]);
    }
}
