<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\RequestRide;
use App\Ride;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa RideController posiada funkcje pracujące z Przejazdami.
 * @package App\Http\Controllers
 */
class RideController extends Controller
{
    /**
     * RideController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:ride_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:ride_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:ride_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:ride_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Przejazdów.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Input::has('search_user') ? Input::get('search_user') : null;
        $car = Input::has('search_car') ? Input::get('search_car') : null;
        $ride_date_from = Input::has('search_ride_date_from') ? Input::get('search_ride_date_from') : null;
        $ride_date_to = Input::has('search_ride_date_to') ? Input::get('search_ride_date_to') : null;
        $begin_mileage = Input::has('search_begin_mileage') ? Input::get('search_begin_mileage') : null;
        $end_mileage = Input::has('search_end_mileage') ? Input::get('search_end_mileage') : null;

        if (isset($user)) {
            $rides = DB::table('rides')
                ->leftJoin('users', 'rides.user_id', '=', 'users.id')
                ->leftJoin('cars', 'rides.car_id', '=', 'cars.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('rides.id as id',
                    'users.name as name',
                    'users.surname as surname',
                    'brands.name as brand',
                    'cars.model as model',
                    'rides.ride_date as ride_date',
                    'rides.begin_mileage as begin_mileage',
                    'rides.end_mileage as end_mileage'
                )
                ->where('users.name', 'LIKE', '%' . $user . '%')
                ->orWhere('users.surname', 'LIKE', '%' . $user . '%')
                ->orderBy('rides.ride_date', 'dsc')
                ->paginate(15);
        } else if (isset($car)) {
            $rides = DB::table('rides')
                ->leftJoin('users', 'rides.user_id', '=', 'users.id')
                ->leftJoin('cars', 'rides.car_id', '=', 'cars.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('rides.id as id',
                    'users.name as name',
                    'users.surname as surname',
                    'brands.name as brand',
                    'cars.model as model',
                    'rides.ride_date as ride_date',
                    'rides.begin_mileage as begin_mileage',
                    'rides.end_mileage as end_mileage'
                )
                ->where('brands.name', 'LIKE', '%' . $car . '%')
                ->orWhere('cars.model', 'LIKE', '%' . $car . '%')
                ->orderBy('rides.ride_date', 'dsc')
                ->paginate(15);
        } else if (isset($ride_date_from) and isset($ride_date_to)) {
            $rides = DB::table('rides')
                ->leftJoin('users', 'rides.user_id', '=', 'users.id')
                ->leftJoin('cars', 'rides.car_id', '=', 'cars.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('rides.id as id',
                    'users.name as name',
                    'users.surname as surname',
                    'brands.name as brand',
                    'cars.model as model',
                    'rides.ride_date as ride_date',
                    'rides.begin_mileage as begin_mileage',
                    'rides.end_mileage as end_mileage'
                )
                ->whereBetween('rides.ride_date', [$ride_date_from, $ride_date_to])
                ->orderBy('rides.ride_date', 'dsc')
                ->paginate(15);
        } else if (isset($begin_mileage) and isset($end_mileage)) {
            $rides = DB::table('rides')
                ->leftJoin('users', 'rides.user_id', '=', 'users.id')
                ->leftJoin('cars', 'rides.car_id', '=', 'cars.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('rides.id as id',
                    'users.name as name',
                    'users.surname as surname',
                    'brands.name as brand',
                    'cars.model as model',
                    'rides.ride_date as ride_date',
                    'rides.begin_mileage as begin_mileage',
                    'rides.end_mileage as end_mileage'
                )
                ->where('rides.begin_mileage', '>=', $begin_mileage)
                ->where('rides.end_mileage', '<=', $end_mileage)
                ->orderBy('rides.ride_date', 'dsc')
                ->paginate(15);
        } else {
            $rides = DB::table('rides')
                ->leftJoin('users', 'rides.user_id', '=', 'users.id')
                ->leftJoin('cars', 'rides.car_id', '=', 'cars.id')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->select('rides.id as id',
                    'users.name as name',
                    'users.surname as surname',
                    'brands.name as brand',
                    'cars.model as model',
                    'rides.ride_date as ride_date',
                    'rides.begin_mileage as begin_mileage',
                    'rides.end_mileage as end_mileage'
                )
                ->orderBy('rides.ride_date', 'dsc')
                ->paginate(15);
        }
        return view('ride.index', ['rides' => $rides]);
    }

    /**
     * Funkcja zwraca widok do tworzenia Przejazdu.
     * @return $this
     */
    public function create()
    {
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();

        return view('ride.create')->with('users', $users)->with('cars', $cars);
    }

    /**
     * Funkcja dodaje nowy Przejazd do bazy.
     * @param RequestRide $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestRide $request)
    {
        $rides = new Ride;
        $rides->user_id = Input::get('user_id');
        $rides->car_id = Input::get('car_id');
        $rides->ride_date = Input::get('ride_date');
        $rides->begin_mileage = Input::get('begin_mileage');
        $rides->end_mileage = Input::get('end_mileage');
        $rides->save();
        $request->session()->flash('success', 'Dodano nowy przejazd pomyślnie!');

        return redirect()->route('ride.create');
    }


    /**
     * Funcja zwraca widok do edycji Przejazdu o danym id.
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $rides = Ride::find($id);
        $cars = Car::orderBy('brand_id')->orderBy('model')->orderBy('registration_number')->get();
        $users = User::orderBy('surname')->orderBy('name')->orderBy('email')->get();

        return view('ride.edit')->with('rides', $rides)->with('users', $users)->with('cars', $cars);
    }

    /**
     * Funkcja edytuje w bazie Przejazd o danym id.
     * @param RequestRide $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestRide $request, $id)
    {
        $rides = Ride::find($id);
        $rides->user_id = Input::get('user_id');
        $rides->car_id = Input::get('car_id');
        $rides->ride_date = Input::get('ride_date');
        $rides->begin_mileage = Input::get('begin_mileage');
        $rides->end_mileage = Input::get('end_mileage');
        $rides->save();
        $request->session()->flash('success', 'Edytowano przejazd pomyślnie!');

        return redirect()->route('ride.index');
    }

    /**
     * Funkcja usuwa z bazy Przejazd o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $rides = Ride::find($id);
        $rides->delete();
        Session::flash('success', 'Usunięto przejazd pomyślnie!');

        return redirect()->route('ride.index');
    }
}
