<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\RequestBrand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa BrandController obejmuje funkcje związane z Markami.
 * @package App\Http\Controllers
 */
class BrandController extends Controller
{
    /**
     * BrandController constructor.
     * Sprawdzenie czy użytkownik jest uprawniony do korzystania z funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:brand_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:brand_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:brand_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:brand_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok z danymi o Markach.
     *
     *Zaimplementowane szukanie i sortowanie.
     * @return view('brand.index')
     */
    public function index()
    {
        $name = Input::has('search_name') ? Input::get('search_name') : null;

        if (isset($name)) {
            $brands = DB::table('brands')
                ->select('brands.id as id',
                    'brands.name as name'
                )
                ->where('brands.name', 'LIKE', '%' . $name . '%')
                ->whereNull('brands.deleted_at')
                ->orderBy('brands.id', 'asc')
                ->paginate(15);
        } else {
            $brands = DB::table('brands')
                ->select('brands.id as id',
                    'brands.name as name'
                )->orderBy('brands.id', 'asc')
                ->whereNull('brands.deleted_at')
                ->paginate(15);

        }
        return view('brand.index', ['brands' => $brands]);
    }

    /**
     * Funkcja zwraca widok do tworzenia nowej Marki.
     * @return view('brand.create')
     */
    public function create()
    {
        return view('brand.create');
    }

    /**
     * Funkcja zapisuje nową Markę do bazy.
     * @param RequestBrand $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestBrand $request)
    {
        $brands = new Brand();
        $brands->name = ucfirst($request->name);
        $brands->save();
        $request->session()->flash('success', 'Dodano nową markę samochodu pomyślnie!');

        return redirect()->route('brand.create');
    }

    /**
     * Funkcja zwraca widok do edycji Marki o podanym indeksie.
     * @param int $id indeks
     * @return view('brand.edit')
     */
    public function edit($id)
    {
        $brands = Brand::find($id);

        return view('brand.edit')->with('brands', $brands);
    }

    /**
     * Funkcja aktualizuję dane Marki w bazie.
     * @param RequestBrand $request
     * @param int $id indeks marki
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestBrand $request, $id)
    {
        $brands = Brand::find($id);
        $brands->name = ucfirst($request->name);
        $brands->save();
        $request->session()->flash('success', 'Edytowano markę samochodu pomyślnie!');

        return redirect()->route('brand.index');
    }

    /**
     * Funkcja usuwa Markę z bazy.
     * @param int $id indeks marki
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $brands = Brand::where('id', $id)->first();
        $brands->delete();
        Session::flash('success', 'Pomyślnie usunięto ' . $brands->name . '!');

        return redirect()->route('brand.index');
    }
}
