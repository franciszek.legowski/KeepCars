<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\RequestBrand;
use App\Http\Resources\BrandResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa BrandAPIController odpowiada za pracę z Markami w aplikacji androidowej.
 * @package App\Http\Controllers
 */
class BrandAPIController extends Controller
{
    /**
     * BrandAPIController constructor.
     * Sprawdza czy użytkownik jest uprawniony do wykonywania funkcji z kontrolera.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:brand_index', ['only' => ['show']]);
        $this->middleware('permission:brand_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:brand_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:brand_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca Marki z bazy.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return BrandResource::collection(Brand::all());
    }

    /**
     * Funkcja zapisuje dane do bazy.
     * @param RequestBrand $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestBrand $request)
    {
        $brand = new Brand;
        $brand->name = ucfirst($request->name);
        $brand->save();

        return response([
            'data' => new BrandResource($brand)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca Markę o podanym indeksie.
     * @param int $id indeks marki
     * @return BrandResource
     */
    public function show($id)
    {
        return new BrandResource(Brand::find($id));
    }

    /**
     * Funkcja edytuje Markę o podanym indeksie.
     * @param Request $request dane
     * @param int $id indeks marki
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        $brand->name = ucfirst($request->name);
        $brand->save();

        return response([
            'data' => new BrandResource($brand)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa Markę z bazy
     * @param int $id indeks marki
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
