<?php

namespace App\Http\Controllers;

use App\CostType;
use App\Http\Requests\RequestCostType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa CostTypeController posiada funkcje pracujące z Kosztami serwisowymi.
 * @package App\Http\Controllers
 */
class CostTypeController extends Controller
{
    /**
     * CostTypeController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:cost_type_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:cost_type_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:cost_type_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:cost_type_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Typów kosztów.
     * @return $this
     */
    public function index()
    {
        $name = Input::has('search_name') ? Input::get('search_name') : null;

        if (isset($name)) {
            $cost_types = DB::table('cost_types')
                ->select('cost_types.id as id',
                    'cost_types.name as name'
                )
                ->where('cost_types.name', 'LIKE', '%' . $name . '%')
                ->whereNull('cost_types.deleted_at')
                ->orderBy('cost_types.id', 'asc')
                ->paginate(15);
        } else {
            $cost_types = DB::table('cost_types')
                ->select('cost_types.id as id',
                    'cost_types.name as name'
                )
                ->whereNull('cost_types.deleted_at')
                ->orderBy('cost_types.id', 'asc')
                ->paginate(15);

        }

        return view('cost_type.index')->with('cost_types', $cost_types);
    }

    /**
     * funkcja zwraca widok do towrzenia nowego kosztu.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        // W widoku jest tylko pole do wpisania nowego typu kosztu i przycisk.
        return view('cost_type.create');
    }

    /**
     * Funkcja dodaje nowy typ kosztu do bazy.
     * @param RequestCostType $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestCostType $request)
    {
        $cost_types = new CostType();
        $cost_types->name = ucfirst($request->name);
        $cost_types->save();
        $request->session()->flash('success', 'Dodano nowy typ kosztu pomyślnie!');

        return redirect()->route('cost_type.create');
    }

    /**
     * Funcja zwraca widok do edycji typu kosztu o danym id.
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $cost_types = CostType::find($id);

        return view('cost_type.edit')->with('cost_types', $cost_types);
    }

    /**
     * Funkcja edytuje w bazie Typ kosztu o danym id.
     * @param RequestCostType $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestCostType $request, $id)
    {
        $cost_types = CostType::find($id);
        $cost_types->name = ucfirst($request->name);
        $cost_types->save();
        $request->session()->flash('success', 'Edytowano typ kosztu pomyślnie!');

        return redirect()->route('cost_type.index');
    }

    /**
     * Funkcja usuwa z bazy typ kosztu o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $cost_types = CostType::where('id', $id)->first();
        $cost_types->delete();
        Session::flash('success', 'Pomyślnie usunięto ' . $cost_types->name . '!');

        return redirect()->route('cost_type.index');
    }
}
