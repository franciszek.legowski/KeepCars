<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRefuel;
use App\Http\Resources\RefuelResource;
use App\Refuel;
use Symfony\Component\HttpFoundation\Response;

/**
 * klasa RefuelAPIController odpowiada za pracę z Tankowaniami w aplikacji Android.
 * @package App\Http\Controllers
 */
class RefuelAPIController extends Controller
{
    /**
     * RefuelAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:refuel_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:refuel_latest', ['only' => ['latest']]);
        $this->middleware('permission:refuel_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:refuel_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:refuel_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca dane o wyszstkich Tankowaniach.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return RefuelResource::collection(Refuel::all());
    }

    /**
     * Funkcja dodaje do bazy nowe Tankowanie.
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function latest($id)
    {
        return RefuelResource::collection(
            Refuel::whereNotNull('created_at')
                ->where('car_id', '=', $id)
                ->orderBy('refuel_date', 'desc')
                ->take(10)
                ->get()
        );
    }


    /**
     * Funkcja dodaje do bazy Tankowanie o danym id
     * @param RequestRefuel $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestRefuel $request)
    {
        $refuel = new Refuel;
        $refuel->car_id = $request->car_id;
        $refuel->user_id = $request->user_id;
        $refuel->refuel_quota = $request->refuel_quota;
        $refuel->price_of_litre = $request->price_of_litre;
        $refuel->amount_of_litre = $request->amount_of_litre;
        $refuel->refuel_date = $request->refuel_date;
        $refuel->mileage = $request->mileage;
        $refuel->save();

        return response([
            'data' => new RefuelResource($refuel)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane o Tankowaniu o podanym id.
     * @param $id
     * @return RefuelResource
     */
    public function show($id)
    {
        return new RefuelResource(Refuel::find($id));
    }

    /**
     * Funkcja edytuje w bazie Tankowanie o danym id
     * @param RequestRefuel $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestRefuel $request, $id)
    {
        $refuel = Refuel::find($id);
        $refuel->car_id = $request->car_id;
        $refuel->user_id = $request->user_id;
        $refuel->refuel_quota = $request->refuel_quota;
        $refuel->price_of_litre = $request->price_of_litre;
        $refuel->amount_of_litre = $request->amount_of_litre;
        $refuel->refuel_date = $request->refuel_date;
        $refuel->mileage = $request->mileage;
        $refuel->save();

        return response([
            'data' => new RefuelResource($refuel)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy Tankowanie o danym id.
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $refuel = Refuel::find($id);
        $refuel->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
