<?php

namespace App\Http\Controllers;

use App\Fuel_type;
use App\Http\Requests\RequestFuelType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Class FuelTypeController posiada funkcje pracujące z Typami paliw.
 * @package App\Http\Controllers
 */
class FuelTypeController extends Controller
{
    /**
     * FuelTypeController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:fuel_type_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:fuel_type_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:fuel_type_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:fuel_type_delete', ['only' => ['show', 'destroy']]);
    }

    /**
     * Funkcja zwraca widok Typów paliw.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $name = Input::has('search_name') ? Input::get('search_name') : null;

        if (isset($name)) {
            $fuel_types = DB::table('fuel_types')
                ->select('fuel_types.id as id',
                    'fuel_types.name as name'
                )
                ->where('fuel_types.name', 'LIKE', '%' . $name . '%')
                ->whereNull('fuel_types.deleted_at')
                ->orderBy('fuel_types.id', 'asc')
                ->paginate(15);
        } else {
            $fuel_types = DB::table('fuel_types')
                ->select('fuel_types.id as id',
                    'fuel_types.name as name'
                )
                ->whereNull('fuel_types.deleted_at')
                ->orderBy('fuel_types.id', 'asc')
                ->paginate(15);

        }
        return view('fuel_type.index', ['fuel_types' => $fuel_types]);
    }

    /**
     * Funkcja zwraca widok do tworzenia Typu paliwa.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('fuel_type.create');
    }

    /**
     * Funkcja dodaje nowy Typ paliwa do bazy.
     * @param RequestFuelType $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestFuelType $request)
    {
        $fuel_types = new Fuel_type();

        $fuel_types->name = ucfirst($request->name);

        $fuel_types->save();
        $request->session()->flash('success', 'Dodano nowy typ paliwa pomyślnie!');
        return redirect()->route('fuel_type.create');
    }

    /**
     * Funcja zwraca widok do edycji typu paliwa o danym id.
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        $fuel_types = Fuel_type::find($id);
        return view('fuel_type.edit')->with('fuel_types', $fuel_types);
    }

    /**
     * Funkcja edytuje w bazie Typ paliwa o danym id.
     * @param $id
     * @param RequestFuelType $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, RequestFuelType $request)
    {
        $fuel_types = Fuel_type::find($id);
        $fuel_types->name = ucfirst(Input::get('name'));

        $fuel_types->save();
        $request->session()->flash('success', 'Edytowano typ paliwa pomyślnie!');
        return redirect()->route('fuel_type.index');
    }

    /**
     * Funkcja usuwa z bazy Typ paliwa o danym id.
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $fuel_types = Fuel_type::where('id', $id)->first();
        $fuel_types->delete();
        Session::flash('success', 'Pomyślnie usunięto ' . $fuel_types->name . '!');

        return redirect()->route('fuel_type.index');
    }
}
