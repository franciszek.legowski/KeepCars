<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CarResource
 * @package App\Http\Resources
 */
class CarResource extends JsonResource
{
    /**
     * toArray.
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'model' => $this->model,
            'production_year' => $this->production_year,
            'fuel_type_id' => $this->fuel_type_id,
            'vin' => $this->vin,
            'registration_number' => $this->registration_number,
            'mileage' => $this->mileage,
        ];
    }
}
