<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RideResource
 * @package App\Http\Resources
 */
class RideResource extends JsonResource
{
    /**
     * toArray.
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'car_id' => $this->car_id,
            'ride_date' => $this->ride_date,
            'begin_mileage' => $this->begin_mileage,
            'end_mileage' => $this->end_mileage
        ];
    }
}
