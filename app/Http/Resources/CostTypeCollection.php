<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class CostTypeCollection
 * @package App\Http\Resources
 */
class CostTypeCollection extends ResourceCollection
{
    /**
     * toArray.
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }
}
