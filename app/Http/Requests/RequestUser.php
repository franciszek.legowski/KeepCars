<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestUser odpowiada za walidację Pracownika.
 * @package App\Http\Requests
 */
class RequestUser extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Pracownika.
     * @return array
     */
    public function rules()
    {
        return [
            'role_id' => 'required|integer',
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $this->id,
            'password' => 'sometimes|required|string|min:6|confirmed',
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Pracownika.
     * @return array
     */
    public function messages()
    {
        return [
            'role_id.required' => 'Nie wybrano roli',
            'role_id.integer' => 'Rola musi być liczbą',
            'name.required' => 'Nie podano imienia',
            'name.string' => 'Imię musi być ciągiem znaków',
            'name.max' => 'Imię przekracza :max znaków',
            'surname.required' => 'Nie podano nazwiska',
            'surname.string' => 'Nazwisko musi być ciągiem znaków',
            'surname.max' => 'Nazwisko przekracza :max znaków',
            'email.required' => 'Nie podano emaila',
            'email.string' => 'Email musi być ciągiem znaków',
            'email.email' => 'To nie jest postać emaila',
            'email.max' => 'Email przekracza :max znaków',
            'email.unique' => 'Email musi być unikalny',
            'password.required' => 'Nie podano hasła',
            'password.string' => 'Hasło musi być ciągiem znaków',
            'password.min' => 'Hasło nie przekracza :min znaków',
        ];
    }
}
