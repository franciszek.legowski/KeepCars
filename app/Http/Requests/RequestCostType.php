<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestCostType odpowiada za walidację Typ kosztu.
 * @package App\Http\Requests
 */
class RequestCostType extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Typ kosztu.
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:cost_types',
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Typu kosztu.
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nie podano typu kosztu',
            'name.string' => 'Typ kosztu musi być ciągiem znaków',
            'name.max' => 'Typ kosztu przekracza :max znaków',
            'name.unique' => 'Typ kosztu musi być unikalny',
        ];
    }
}
