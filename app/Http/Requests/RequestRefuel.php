<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Klasa RequestRefuel odpowiada za walidację Tankowania.
 * @package App\Http\Requests
 */
class RequestRefuel extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Tankowanie.
     * @return array
     */
    public function rules()
    {
        $mileage = DB::table('cars')->select('mileage')->where('id', '=', (int)$this->car_id)->get();
        $mileage = str_replace("[{\"mileage\":", "", $mileage);
        $mileage = str_replace("}]", "", $mileage);
        $mileage = intval($mileage) - 1000;

        return [
            'car_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'refuel_quota' => 'required|numeric|min:0',
            'price_of_litre' => 'required|numeric|min:0',
            'amount_of_litre' => 'required|numeric|min:0',
            'refuel_date' => 'required|date_format:Y-m-d H:i',
            'mileage' => 'required|numeric|min:' . $mileage,
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Tankowania.
     * @return array
     */
    public function messages()
    {
        return [
            'car_id.required' => 'Nie wybrano samochodu',
            'car_id.numeric' => 'Nie wybrano samochodu',
            'user_id.required' => 'Nie wybrano użytkownika',
            'user_id.numeric' => 'Nie wybrano użytkownika',
            'refuel_quota.required' => 'Nie podano kwoty',
            'refuel_quota.numeric' => 'Kwota musi być liczbą',
            'refuel_quota.min' => 'Kwota nie może być ujemna',
            'price_of_litre.required' => 'Nie podano ceny paliwa',
            'price_of_litre.numeric' => 'Cena musi być liczbą',
            'price_of_litre.min' => 'Cena nie może być ujemna',
            'amount_of_litre.required' => 'Nie podano ilości litrów',
            'amount_of_litre.numeric' => 'Ilość litrów musi być liczbą',
            'amount_of_litre.min' => 'Ilość litrów nie może być ujemna',
            'refuel_date.required' => 'Nie podano daty tankowania',
            'refuel_date.date_format' => 'Podano zły format daty i czasu',
            'mileage.required' => 'Nie podano przebiegu',
            'mileage.numeric' => "Przebieg musi być liczbą",
            'mileage.min' => "Przebieg tankowania nie może być mniejszy niz przebieg aktualny",
        ];
    }
}
