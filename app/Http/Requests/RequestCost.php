<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestCost odpowiada za walidację Koszt.
 * @package App\Http\Requests
 */
class RequestCost extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Koszt.
     * @return array
     */
    public function rules()
    {
        return [
            'cost_type_id' => 'required|numeric',
            'car_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'cost_quota' => 'required|numeric|min:0',
            'cost_date' => 'required|date_format:Y-m-d H:i',
            'description' => 'max:1000',
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Kosztu.
     * @return array
     */
    public function messages()
    {
        return [
            'cost_type_id.required' => 'Nie wybrano typu kosztu',
            'cost_type_id.numeric' => 'Nie wybrano typu kosztu',
            'car_id.required' => 'Nie wybrano samochodu',
            'car_id.numeric' => 'Nie wybrano samochodu',
            'user_id.required' => 'Nie wybrano użytkownika',
            'user_id.numeric' => 'Nie wybrano użytkownika',
            'cost_quota.required' => 'Nie podano kwoty',
            'cost_quota.numeric' => 'Kwota musi być liczbą',
            'cost_quota.min' => 'Kwota nie może być ujemna',
            'cost_date.required' => 'Nie podano daty tankowania',
            'cost_date.date_format' => 'Podano zły format daty i czasu',
            'description.max' => 'Opis przekracza :max znaków',
        ];
    }
}
