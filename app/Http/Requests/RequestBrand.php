<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestBrand odpowiada za walidację Marki.
 * @package App\Http\Requests
 */
class RequestBrand extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Markę.
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:brands',
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Marki.
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nie podano marki',
            'name.unique' => 'Marka musi być unikalna',
            'name.string' => 'Marka musi być ciągiem znaków',
            'name.max' => 'Nazwa przekracza :max znaków',
        ];
    }
}
