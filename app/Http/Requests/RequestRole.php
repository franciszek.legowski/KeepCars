<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestRole odpowiada za walidację Roli.
 * @package App\Http\Requests
 */
class RequestRole extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Rolę.
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:roles,name,' . $this->id,
            'display_name' => 'required|string|max:255|unique:roles,display_name,' . $this->id,
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Roli.
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nie podano roli',
            'name.unique' => 'Rola musi być unikalna',
            'name.string' => 'Rola musi być ciągiem znaków',
            'name.max' => 'Rola przekracza :max znaków',
            'display_name.required' => 'Nie podano wyświetlanej nazwy',
            'display_name.unique' => 'Wyświetlana nazwa musi być unikalna',
            'display_name.string' => 'Wyświetlana nazwa musi być ciągiem znaków',
            'display_name.max' => 'Wyświetlana nazwa przekracza :max znaków',
        ];
    }
}
