<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestFuelType odpowiada za walidację Typu paliwa.
 * @package App\Http\Requests
 */
class RequestFuelType extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Typ paliwa.
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:fuel_types',
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Typu paliwa.
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nie podano typu paliwa',
            'name.unique' => 'Typ paliwa musi być unikalny',
            'name.string' => 'Typ paliwa musi być ciągiem znaków',
            'name.max' => 'Typ paliwa przekracza :max znaków',
        ];
    }
}
