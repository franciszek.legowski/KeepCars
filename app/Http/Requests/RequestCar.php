<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Klasa RequestCar odpowiada za walidację Samochodu.
 * @package App\Http\Requests
 */
class RequestCar extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Samochód.
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => 'required|numeric',
            'model' => 'required|string|max:255',
            'production_year' => 'required|numeric|between:1900,2018',
            'fuel_type_id' => 'required|numeric',
            'vin' => 'required|string|size:17|unique:cars,vin,' . $this->id,
            'registration_number' => 'required|string|size:7',
            'mileage' => 'required|numeric|min:0',
            'cost_sum' => 'numeric|min:0',
            'mileage_sum' => 'numeric|min:0',
            'litre_sum' => 'numeric|min:0',
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Samochodu.
     * @return array
     */
    public function messages()
    {
        return [
            'brand_id.required' => 'Nie wybrano marki',
            'brand_id.numeric' => 'Nie wybrano marki',
            'model.required' => 'Nie podano modelu',
            'model.max' => 'Nazwa przekracza :max znaków',
            'production_year.required' => 'Nie podano roku produkcji',
            'production_year.numeric' => 'Rok produkcji musi być liczbą',
            'production_year.between' => 'Rok produkcji nie zawiera się pomiędzy 1900 a 2018',
            'fuel_typ_id.required' => 'Nie wybrano typu paliwa',
            'fuel_typ_id.numeric' => 'Nie wybrano typu paliwa',
            'vin.required' => 'Nie podano VIN',
            'vin.size' => 'VIN składa się z 17 znaków',
            'registration_number.required' => 'Nie podano numeru rejestracyjnego',
            'registration_number.size' => 'Numer rejestracyjny składa się z 7 znaków',
            'mileage.required' => 'Nie podano przebiegu',
            'mileage.numeric' => 'Przebieg musi być liczbą',
        ];
    }
}
