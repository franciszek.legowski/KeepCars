<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;

/**
 * Klasa Role odpowiada tabeli roles.
 * @package App
 */
class Role extends EntrustRole
{
    use SoftDeletes;

    protected $table = 'roles';

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    /**
     * Funkcja zwraca pracownika dla Roli.
     * @return User::class
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Funkcja zwraca PermissionRole dla Roli.
     * @return PermissionRole::class
     */
    public function permissionRoles()
    {
        return $this->hasMany(PermissionRole::class);
    }

    /**
     * Funkcja zwraca RoleUser dla Roli.
     * @return RoleUser::class
     */
    public function roleUsers()
    {
        return $this->hasMany(RoleUser::class);
    }

}