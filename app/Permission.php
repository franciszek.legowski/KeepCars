<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustPermission;

/**
 * Klasa Permission odpowiada tabeli permissions.
 * @package App
 */
class Permission extends EntrustPermission
{
    use SoftDeletes;

    protected $table = 'permissions';

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    /**
     * Funkcja zwraca Rolę dla Pozwolenia
     * @return PermissionRole::class
     */
    public function permissionRoles()
    {
        return $this->hasMany(PermissionRole::class);
    }
}