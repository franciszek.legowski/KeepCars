<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OAuth
 * @package App
 */
class OAuth extends Model
{
    protected $table = 'oauth_clients';
}
