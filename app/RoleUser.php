<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Klasa RoleUser odpowiada tabeli role_user
 * @package App
 */
class RoleUser extends Model
{
    //use SoftDeletes;

    protected $fillable = [
        'user_id', 'role_id',
    ];

    protected $table = 'role_user';

    /**
     * Funkcja zwraca Pracownika dla RoleUser
     * @return User::class
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Funkcja zwraca Rolę dla RoleUser
     * @return Role::class
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }


}
