<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Klasa Cost odpowiadająca tabeli costs
 * @package App
 */
class Cost extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'cost_type_id', 'car_id', 'user_id', 'cost_quota', 'cost_date', 'description',
    ];

    /**
     * Funkcja zwraca Typ kosztu dla danego kosztu.
     * @return CostType::class
     */
    public function cost_type()
    {
        return $this->belongsTo(CostType::class);
    }

    /**
     * Funkcja zwraca samochody dla Kosztu.
     * @return Car::class
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    /**
     * Funkcja zwraca kierowców dla Kosztu.
     * @return User::class
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
