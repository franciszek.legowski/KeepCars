<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Klasa Fuel_type odpowiada tabeli fuel_types
 * @package App
 */
class Fuel_type extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
    ];

    /**
     * Funkcja zwraca samochody dla danego Rodzaju paliwa.
     * @return Car::class
     */
    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
