<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Klasa Refuel odpowiada tabeli refuels.
 * @package App
 */
class Refuel extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'car_id', 'user_id', 'refuel_quota', 'price_of_litre', 'amount_of_litre', 'refuel_date', 'mileage',
    ];

    /**
     * Funkcja zwraca samochód dla podanego Tankowania.
     * @return Car::class
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    /**
     * Funkcja zwraca kierowcę dla podanego Tankowania.
     * @return User::class
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
