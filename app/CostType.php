<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Klasa CostType odpowiada tabeli cost_types.
 * @package App
 */
class CostType extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
    ];

    /**
     * Funkcja zwraca koszty serwisowe dla Typu kosztu.
     * @return Cost:class
     */
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }
}
