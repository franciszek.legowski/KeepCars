<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Klasa PermissionRole odpowiada tabeli permision_role.
 * @package App
 */
class PermissionRole extends Model
{
    use SoftDeletes;

    protected $table = 'permission_role';

    protected $fillable = [
        'permission_id', 'role_id',
    ];

    /**
     * Funkcja zwraca Pozwolenia dla Roli.
     * @return Permission::class
     */
    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    /**
     * Funkcja zwraca Rolę dla Roli.
     * @return Role::class
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
