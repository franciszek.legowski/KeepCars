<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Klasa Ride odpowiada tabeli rides.
 * @package App
 */
class Ride extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'user_id', 'car_id', 'ride_date', 'begin_mileage', 'end_mileage',
    ];

    /**
     * Funkcja zwraca kierowcę dla Przejazdu.
     * @return User::class
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Funkcja zwraca samochód dla Przejazdu.
     * @return Car::class
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

}
