<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Klasa User odpowiada tabeli users.
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    use SoftDeletes, EntrustUserTrait {
        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'role_id', 'name', 'surname', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Funkcja zwrca roleUser dla Pracownika.
     * @return RoleUser::class
     */
    public function roleUsers()
    {
        return $this->hasMany(RoleUser::class);
    }

    /**
     * Funkcja zwrca Tankowania dla Pracownika.
     * @return Refuel::class
     */
    public function refuels()
    {
        return $this->hasMany(Refuel::class);
    }

    /**
     * Funkcja zwrca Koszty serwisowe dla Pracownika.
     * @return Cost::class
     */
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    /**
     * Funkcja zwrca Przejazdy dla Pracownika.
     * @return Ride::class
     */
    public function rides()
    {
        return $this->hasMany(Ride::class);
    }
}
