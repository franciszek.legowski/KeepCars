@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dodaj nową rolę</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('role.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nazwa</label>
                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="display_name" class="col-md-4 col-form-label text-md-right">Nazwa
                                    wyświetlana</label>
                                <div class="col-md-6">
                                    <input id="display_name" type="text"
                                           class="form-control{{ $errors->has('display_name') ? ' is-invalid' : '' }}"
                                           name="display_name" value="{{ old('display_name') }}" required>
                                    @if ($errors->has('display_name'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Opis</label>
                                <div class="col-md-6">
                                    <input id="description" type="text"
                                           class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                           name="description" value="{{ old('description') }}" required>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            {{--<div class="form-group row">--}}
                            {{--<label for="description" class="col-md-4 col-form-label text-md-right">Uprawnienia</label>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<ul class="list-unstyled">--}}
                            {{--<li><input  type="checkbox" value="Usuwanie" onclick="toggle('usuwanie')"><span class=" h5 font-weight-bold">Usuwanie</span></li>--}}
                            {{--<li><input  type="checkbox" value="Usuwanie" class="ml-4" name="usuwanie">Usuwanie Pojazdów</li>--}}
                            {{--<li><input  type="checkbox" value="Usuwanie" class="ml-4" name="usuwanie">Usuwanie Użytkowników</li>--}}
                            {{--<li><input  type="checkbox" value="Usuwanie" class="ml-4" name="usuwanie">Usuwanie Kosztów i Tankowań</li>--}}
                            {{--<li><input  type="checkbox" value="Dodawanie" onclick="toggle('dodawanie')" class="font-weight-bold">Dodawanie</li>--}}
                            {{--<li><input  type="checkbox" value="Dodawanie" class="ml-4" name="dodawanie">Dodawanie Pojazdów</li>--}}
                            {{--<li><input  type="checkbox" value="Dodawanie" class="ml-4" name="dodawanie">Dodawanie Użytkowników</li>--}}
                            {{--<li><input  type="checkbox" value="Dodawanie" class="ml-4" name="dodawanie">Dodawanie Kosztów i Tankowań</li>--}}
                            {{--</ul>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<script language="JavaScript">--}}
                            {{--function toggle(source) {--}}
                            {{--checkboxes = document.getElementsByName(source);--}}
                            {{--if (checkboxes[1].checked == false)--}}
                            {{--for (var i = 0, n = checkboxes.length; i < n; i++) {--}}
                            {{--checkboxes[i].checked = true;--}}
                            {{--}--}}
                            {{--else--}}
                            {{--for (var i = 0, n = checkboxes.length; i < n; i++) {--}}
                            {{--checkboxes[i].checked = false;--}}
                            {{--}--}}
                            {{--}--}}
                            {{--</script>--}}
                            <div class="form-group row offset-5 mb-0">
                                <button type="submit" class="btn btn-success">
                                    Dodaj rolę
                                </button>
                        </form>

                        <form action="{{ route('role.index') }}">
                            <button type="submit" class="btn btn-danger mx-sm-2">
                                Powrót
                            </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
