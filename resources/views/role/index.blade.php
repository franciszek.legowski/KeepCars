@extends('layouts.app')

@section('content')
    <div class="container">
        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('role.create') }}">
            <button type="submit" class="btn btn-primary mb-3">
                Nowa rola <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <form action="{{ route('role.index') }}" method="GET" accept-charset="utf-8">
            <?php
            use Illuminate\Support\Facades\Input;
            $display_name = Input::has('search_display_name') ? Input::get('search_display_name') : [];
            $description = Input::has('search_description') ? Input::get('search_description') : [];?>
            <div id="filtrowanieDiv" style="display: none" class="mb-3">
                <input id="search_display_name" type="search"
                       name="search_display_name" value="" placeholder="Wyszukaj rolę"
                       class="search-input-for-tabelki">
                <i class="fas fa-search"></i>

                <input id="search_description" type="search"
                       name="search_description" value="" placeholder="Wyszukaj opis"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>
                <br>
                <button type="submit" class="btn btn-primary offset-2 mt-3">Filtruj<i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" class="btn btn-primary mt-3 ml-2"
                        onclick="window.location='{{ url("role/index") }}'">Reset<i class="fas fa-undo ml-1"></i>
                </button>
            </div>
        </form>
        <script>
            function filtrowanieShow() {

                if (document.getElementById('filtrowanieDiv').style.display == "none")
                    document.getElementById('filtrowanieDiv').style.display = "block";
                else
                    document.getElementById('filtrowanieDiv').style.display = "none";
            }
        </script>

        <table class="index-table" id="roleTable">
            <tr class="font-weight-bold description-tr">
                <td>Nazwa</td>
                <td>Opis</td>
            </tr>

            @foreach($roles as $key => $value)
                <tr class="description-tr">
                    <td>{{$value->display_name}}</td>
                    <td>{{$value->description}}</td>
                    <td id="second-td-table">
                        <a href=""
                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                            <i class="material-icons">mode_edit</i>
                        </a>
                        <form id="edit-form-{{$value->id}}" action="{{route('role.edit',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="GET"></input>
                            @csrf
                        </form>
                        <a href=""
                           onclick="event.preventDefault(); document.getElementById('delete-form-{{$value->id}}').submit();">
                            <i class="material-icons">delete_sweep</i>
                        </a>
                        <form id="delete-form-{{$value->id}}" action="{{route('role.destroy',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="DELETE"> </input>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $roles->appends(Request::input())->links() }}
    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>

@endsection
