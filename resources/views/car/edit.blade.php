@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edytuj samochód</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('car.update',['id'=>$allCars->id]) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="brand_id" class="col-md-4 col-form-label text-md-right">Marka</label>

                                <div class="col-md-6">
                                    <select id="brand_id" type="number"
                                            class="form-control{{ $errors->has('brand_id') ? ' is-invalid' : '' }}"
                                            name="brand_id">
                                        <option>Wybierz markę...</option>
                                        @foreach($allBrands as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if($allCars->brand->id==$value->id)selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('brand_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('brand_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="model" class="col-md-4 col-form-label text-md-right">Model</label>

                                <div class="col-md-6">
                                    <input id="model" type="text"
                                           class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}"
                                           name="model" value="{{ $allCars->model }}" required>

                                    @if ($errors->has('model'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="production_year" class="col-md-4 col-form-label text-md-right">Rok
                                    produkcji</label>

                                <div class="col-md-6">
                                    <input id="production_year" type="number"
                                           class="form-control{{ $errors->has('production_year') ? ' is-invalid' : '' }}"
                                           name="production_year" value="{{ $allCars->production_year }}" required>

                                    @if ($errors->has('production_year'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('production_year') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fuel_type_id" class="col-md-4 col-form-label text-md-right">Typ
                                    paliwa</label>

                                <div class="col-md-6">
                                    <select id="fuel_type_id" type="text"
                                            class="form-control{{ $errors->has('fuel_type_id') ? ' is-invalid' : '' }}"
                                            name="fuel_type_id">
                                        <option>Wybierz typ paliwa...</option>
                                        @foreach($fuel_types as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if($allCars->fuel_type->id==$value->id)selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('fuel_type_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fuel_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="vin" class="col-md-4 col-form-label text-md-right">VIN</label>

                                <div class="col-md-6">
                                    <input id="vin" type="text"
                                           class="form-control{{ $errors->has('vin') ? ' is-invalid' : '' }}"
                                           name="vin" value="{{ $allCars->vin }}" required>

                                    @if ($errors->has('vin'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('vin') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="registration_number" class="col-md-4 col-form-label text-md-right">Numer
                                    rejestracyjny</label>

                                <div class="col-md-6">
                                    <input id="registration_number" type="text"
                                           class="form-control{{ $errors->has('registration_number') ? ' is-invalid' : '' }}"
                                           name="registration_number" value="{{ $allCars->registration_number }}"
                                           required>

                                    @if ($errors->has('registration_number'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('registration_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mileage" class="col-md-4 col-form-label text-md-right">Przebieg</label>

                                <div class="col-md-6">
                                    <input id="mileage" type="number"
                                           class="form-control{{ $errors->has('mileage') ? ' is-invalid' : '' }}"
                                           name="mileage" value="{{ $allCars->mileage }}" required>

                                    @if ($errors->has('mileage'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mileage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Zdjęcie</label>

                                <div class="col-md-6">
                                    <label>Wybierz zdjęcie do przesłania:</label>
                                    <input type="file" name="image" id="image">
                                    <input type="hidden" value="{{ csrf_token() }}">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Zapisz
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
