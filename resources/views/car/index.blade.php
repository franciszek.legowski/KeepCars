@extends('layouts.app')

@section('content')
    <div id="allQRs" style="max-width: 900px; display: none;">
        @foreach($cars as $key => $value)
            @if(file_exists('images/QR/' . $value->vin))
                <img src="{{URL::asset('images/QR/' . $value->vin)}}"
                     height="250" width="250" class="d-inline">
                <p class="text-black d-inline" style="font-size: 13px; width: 180px;">
                    {{$value->brand}}
                    {{$value->model}}
                    {{$value->registration_number}}
                </p>
            @endif
        @endforeach
    </div>
    <div class="container " id="notAllQRs">
        @foreach($cars as $key => $value)
            @if(file_exists('images/cars/' . $value->vin))
                <div class="display-none modalPOPUP" id="<?php echo $value->vin; ?>">
                    <span class="closePOPUP" onclick="showImage('<?php echo $value->vin; ?>')">&times;</span>

                    <img class="modal-contentPOPUP" src="{{URL::asset('images/cars/' . $value->vin)}}"
                         alt="profile Pic"
                         height="auto" width="auto"
                         onclick="showImage('<?php echo $value->vin; ?>')">
                    <div id="captionPOPUP">
                        {{$value->brand}}
                        {{$value->model}}
                        {{$value->production_year}}
                        {{$value->registration_number}}
                    </div>
                </div>
            @endif
        @endforeach
        @foreach($cars as $key => $value)
            @if(file_exists('images/QR/' . $value->vin))
                <div class="display-none modalPOPUP" id="QR<?php echo $value->vin; ?>">
                        <span class="closePOPUP mr-5 printSpan" id="QRP<?php echo $value->vin; ?>">
                            <i class="fa fa-print" style="font-size: 25px;" aria-hidden="true"
                               onclick="printQR('QRP<?php echo $value->vin; ?>','QRC<?php echo $value->vin; ?>')"></i>
                        </span>
                    <span class="closePOPUP closeSpan" onclick="showQR('QR<?php echo $value->vin; ?>')"
                          id="QRC<?php echo $value->vin; ?>">&times;</span>


                    <img class="modal-contentPOPUPQR offset-5" src="{{URL::asset('images/QR/' . $value->vin)}}"
                         alt="profile Pic"
                         height="250" width="250"
                         onclick="showQR('QR<?php echo $value->vin; ?>')">
                    <div id="captionPOPUP">
                        {{$value->brand}}
                        {{$value->model}}
                        {{$value->production_year}}
                        {{$value->registration_number}}
                    </div>
                </div>
            @endif
        @endforeach

        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('car.create') }}">
            <button type="submit" class="btn btn-primary mb-3">
                Nowy samochód <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <button class="btn-primary btn mb-3" onmousedown="hideForQR()" type="button">
            Drukuj wszystkie kody QR <i class="fa fa-print ml-2" aria-hidden="true"></i>
        </button>
        <form action="{{ route('car.index') }}" method="GET" accept-charset="utf-8">
            <?php
            use Illuminate\Support\Facades\Input;
            $brand_model = Input::has('search_brand_model') ? Input::get('search_brand_model') : [];
            $production_year_from = Input::has('search_production_year_from') ? Input::get('search_production_year_from') : [];
            $production_year_to = Input::has('search_production_year_to') ? Input::get('search_production_year_to') : [];
            $search_fuel_type = Input::has('search_fuel_type') ? Input::get('search_fuel_type') : [];
            $search_vin = Input::has('search_vin') ? Input::get('search_vin') : [];
            $search_registration_number = Input::has('search_registration_number') ? Input::get('search_registration_number') : [];
            $search_mileage_from = Input::has('search_mileage_from') ? Input::get('search_mileage_from') : [];
            $search_mileage_to = Input::has('search_mileage_to') ? Input::get('search_mileage_to') : [];?>
            <div id="filtrowanieDiv" style="display: none" class="mb-3">
                <input id="search_brand_model" type="search"
                       name="search_brand_model" value="" placeholder="Marka i model"
                       class="search-input-for-tabelki mb-2">
                <i class="fas fa-search"></i>
                <input id="search_registration_number" type="search"
                       name="search_registration_number" value="" placeholder="Nr rejestracyjny"
                       class="search-input-for-tabelki  ml-5 mb-2">
                <i class="fas fa-search"></i>
                <input id="search_vin" type="search"
                       name="search_vin" value="" placeholder="VIN"
                       class="search-input-for-tabelki  ml-5 mb-2">
                <i class="fas fa-search"></i>
                <input id="search_fuel_type" type="search"
                       name="search_fuel_type" value="" placeholder="Typ paliwa"
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search"></i>
                <input id="rokOd" type="search"
                       name="search_production_year_from" value="" placeholder="Rok produkcji od..."
                       class="search-input-for-tabelki ">
                <i class="fas fa-search"></i>
                <input id="search_production_year_to" type="search"
                       name="search_production_year_to" value="" placeholder="Rok produkcji do..."
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>
                <input id="search_mileage_from" type="search"
                       name="search_mileage_from" value="" placeholder="Przebieg od..."
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>
                <input id="search_mileage_to" type="search"
                       name="search_mileage_to" value="" placeholder="Przebieg do..."
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>


                <br>
                <button type="submit" class="offset-5 btn btn-primary mt-3">Filtruj <i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" onclick="window.location='{{ url("car/index") }}'"
                        class="ml-2 btn btn-primary mt-3">Reset<i class="fas fa-undo ml-1"></i></button>
            </div>
            <script>
                function filtrowanieShow() {

                    if (document.getElementById('filtrowanieDiv').style.display == "none")
                        document.getElementById('filtrowanieDiv').style.display = "block";
                    else
                        document.getElementById('filtrowanieDiv').style.display = "none";
                }

            </script>
        </form>

        <table id="car_table" class="table table-striped table-bordered align-items-xl-center">
            <thead>
            <tr class="font-weight-bold">
                <td>Pojazd</td>
                <td>Rocznik</td>
                <td>Paliwo</td>
                <td>VIN</td>
                <td>Rejestracja</td>
                <td>Przebieg</td>
                <td>Edycja</td>
            </tr>
            </thead>

            @foreach($cars as $key => $value)

                <tr>
                    <td>{{$value->brand}} {{$value->model}} </td>
                    <td class="small-table-tx-max-width-100"> {{$value->production_year}}</td>
                    <td class="small-table-tx-max-width-150">{{$value->fuel_type}}</td>
                    <td>{{$value->vin}}</td>
                    <td class="small-table-tx-max-width-150"> {{$value->registration_number}}</td>
                    <td class="small-table-tx-max-width-150"> {{$value->mileage}}</td>
                    <td><a href=""
                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                            <i class="material-icons">mode_edit</i>
                        </a>
                        <form id="edit-form-{{$value->id}}" action="{{route('car.edit',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="GET"> </input>
                            @csrf
                        </form>


                        <a href="#"
                           onclick="event.preventDefault(); document.getElementById('destroy-form-{{$value->id}}').submit();">
                            <i class="material-icons">delete</i> </a>
                        <form id="destroy-form-{{$value->id}}" action="{{route('car.destroy',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="DELETE"></input>
                            @csrf
                        </form>
                        @if(file_exists('images/cars/' . $value->vin))
                            <a href="#"><i class="material-icons"
                                           onclick="showImage('<?php echo $value->vin; ?>')">image</i>
                            </a>
                        @endif
                        @if(file_exists('images/QR/' . $value->vin))
                            <a href="#"><i class="material-icons"
                                           onclick="showQR('QR<?php echo $value->vin; ?>')"><i class="fas fa-qrcode"
                                                                                               style="margin-bottom: 3px; font-size: 22px;"></i>
                                </i>
                            </a>
                        @endif
                        <a href="#"
                           onclick="event.preventDefault(); document.getElementById('qr-form-{{$value->id}}').submit();">
                            <i class="material-icons">border_clear</i>
                        </a>
                        <form id="qr-form-{{$value->id}}" action="{{route('car.qr',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="POST"> </input>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $cars->appends(Request::input())->links() }}
    </div>

    <script src="{{asset('js/filteredTable.js')}}"></script>
    <script>

        function showImage(imageID) {
            if (document.getElementById(imageID).style.display == "block") {
                document.getElementById(imageID).style.display = "none";
            }
            else document.getElementById(imageID).style.display = "block";
        }

        function showQR(imageID) {
            if (document.getElementById(imageID).style.display == "block") {
                document.getElementById(imageID).style.display = "none";
            }
            else document.getElementById(imageID).style.display = "block";
        }

        function printQR(cl, pr) {
            document.getElementById(cl).style.display = "none";
            document.getElementById(pr).style.display = "none";
            document.getElementById('footerDiv').style.display = "none";
            window.print();
            document.getElementById(cl).style.display = "block";
            document.getElementById(pr).style.display = "block";
            document.getElementById('footerDiv').style.display = "block";

        }

        function hideForQR() {
            document.getElementById('notAllQRs').style.display = "none";
            document.getElementById('allQRs').style.display = "block";
            document.getElementById('footerDiv').style.display = "none";
            setTimeout(window.print(), 1000);
            setTimeout(printAllQR(), 3000);
        }

        function printAllQR() {


            document.getElementById('notAllQRs').style.display = "block";
            document.getElementById('allQRs').style.display = "none";
            document.getElementById('footerDiv').style.display = "block";

        }


    </script>
    <script>
        $(document).ready(function () {
            $('#car_table').DataTable();
        });
    </script>
    <script src="{{asset('https://code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="{{asset('js/sorttable.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    {{--<link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css')}}" rel="stylesheet">--}}
    {{--<link href="{{asset('https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">--}}


@endsection

