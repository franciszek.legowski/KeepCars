@extends('layouts.app')

@section('content')
    <div class="container" id="user_table">
        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('user.create') }}">
            <button type="submit" class="btn btn-primary mb-3">
                Nowy użytkownik <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <form action="{{ route('user.index') }}" method="GET" accept-charset="utf-8">
            <?php
            use Illuminate\Support\Facades\Input;
            $role = Input::has('search_role') ? Input::get('search_role') : [];
            $name = Input::has('search_name') ? Input::get('search_name') : [];
            $surname = Input::has('search_surname') ? Input::get('search_surname') : [];
            $email = Input::has('search_email') ? Input::get('search_email') : [];?>
            <div id="filtrowanieDiv" style="display: none" class="mb-3">

                <input id="search_role" type="search"
                       name="search_role" value="" placeholder="Rola"
                       class="search-input-for-tabelki">
                <i class="fas fa-search "></i>


                <input id="search_name" type="search"
                       name="search_name" value="" placeholder="Imię"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search "></i>


                <input id="search_surname" type="search"
                       name="search_surname" value="" placeholder="Nazwisko"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search "></i>

                <input id="search_email" type="search"
                       name="search_email" value="" placeholder="Email"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search "></i>

                <br>
                <button type="submit" class="btn btn-primary offset-5 mt-3">Filtruj<i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" class="btn btn-primary mt-3 ml-2"
                        onclick="window.location='{{ url("user/index") }}'">Reset<i class="fas fa-undo ml-1"></i>
                </button>
            </div>
            <script>
                function filtrowanieShow() {

                    if (document.getElementById('filtrowanieDiv').style.display == "none")
                        document.getElementById('filtrowanieDiv').style.display = "block";
                    else
                        document.getElementById('filtrowanieDiv').style.display = "none";
                }
            </script>
        </form>


        <table id="car_table" class="table table-striped table-bordered m-md-auto">
            <thead>
            <tr class="font-weight-bold">
                <td>Rola</td>
                <td>Imię</td>
                <td>Nazwisko</td>
                <td>E-mail</td>
                <td>Edycja</td>
            </tr>
            </thead>
            @foreach($users as $key => $value)
                <tr>
                    <td>{{$value->role}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->surname}}</td>
                    <td>{{$value->email}}</td>

                    <td id="second-td-table">
                        <a href=""
                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                            <i class="material-icons">mode_edit</i>
                        </a>
                        <form id="edit-form-{{$value->id}}" action="{{route('user.edit',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="GET"></input>
                            @csrf
                        </form>
                        <a href=""
                           onclick="event.preventDefault(); document.getElementById('delete-form-{{$value->id}}').submit();">
                            <i class="material-icons">delete_sweep</i>
                        </a>
                        <form id="delete-form-{{$value->id}}" action="{{route('user.destroy',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="DELETE"></input>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $users->appends(Request::input())->links() }}

    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>
    <script src="{{asset('https://code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
@endsection
