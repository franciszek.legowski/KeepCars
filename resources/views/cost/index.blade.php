@extends('layouts.app')
{{--<link href="{{ asset('css/styleTables.css') }}" rel="stylesheet">--}}

@section('content')
    <div class="container">

        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('cost.create') }}">
            <button class="btn-primary btn mb-3" type="button">
                Nowy koszt <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <div id="filtrowanieDiv" style="display: none" class="mb-3">
            <form action="{{ route('cost.index') }}" method="GET" accept-charset="utf-8">
                <?php
                use Illuminate\Support\Facades\Input;
                $cost_type = Input::has('search_cost_type') ? Input::get('search_cost_type') : [];
                $car = Input::has('search_car') ? Input::get('search_car') : [];
                $user = Input::has('search_user') ? Input::get('search_user') : [];
                $cost_quota_from = Input::has('search_cost_quota_from') ? Input::get('search_cost_quota_from') : [];
                $cost_quota_to = Input::has('search_cost_quota_to') ? Input::get('search_cost_quota_to') : [];
                $cost_date_from = Input::has('search_cost_date_from') ? Input::get('search_cost_date_from') : [];
                $cost_date_to = Input::has('search_cost_date_to') ? Input::get('search_cost_date_to') : [];
                $description = Input::has('search_description') ? Input::get('search_description') : [];?>

                <input id="search_cost_type" type="search"
                       name="search_cost_type" value="" placeholder="Typ kosztu"
                       class="search-input-for-tabelki mb-2">
                <i class="fas fa-search"></i>

                <input id="search_car" type="search"
                       name="search_car" value="" placeholder="Samochód"
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search"></i>

                <input id="search_user" type="search"
                       name="search_user" value="" placeholder="Użytkownik"
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search"></i>

                <input id="search_description" type="search"
                       name="search_description" value="" placeholder="Opis"
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search"></i>
                <br>

                <input id="search_cost_quota_from" type="search"
                       name="search_cost_quota_from" value="" placeholder="Kwota od"
                       class="search-input-for-tabelki">
                <i class="fas fa-search"></i>

                <input id="search_cost_quota_to" type="search"
                       name="search_cost_quota_to" value="" placeholder="Kwota do"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>

                <input id="dataOd" type="search"
                       name="search_cost_date_from" value="" placeholder="Data od"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>

                <input id="dataDo" type="text"
                       name="search_cost_date_to" value="" placeholder="Data do"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>


                <br>
                <button type="submit" class="btn btn-primary offset-5 mt-3">Filtruj<i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" class="btn btn-primary mt-3"
                        onclick="window.location='{{ url("cost/index") }}'">Reset<i class="fas fa-undo ml-1"></i>
                </button>
            </form>
        </div>
        <script>
            function filtrowanieShow() {
                if (document.getElementById('filtrowanieDiv').style.display == "none")
                    document.getElementById('filtrowanieDiv').style.display = "block";
                else
                    document.getElementById('filtrowanieDiv').style.display = "none";
            }
        </script>
        <div class="table100 ver5">
            <div class="table100-body js-pscroll">
                <table id="cost_table" class="table table-striped table-bordered m-md-auto" style="width:100%">
                    <thead>
                    <tr class="font-weight-bold">
                        <td>Koszt</td>
                        <td>Pojazd</td>
                        <td>Kierowca</td>
                        <td>Kwota</td>
                        <td>Data</td>
                        <td>Opis</td>
                        <td>Edycja</td>
                    </tr>
                    </thead>
                    @foreach($costs as $key => $value)
                        <tr class="row100 body">
                            <td class="cell100 column5">{{$value->cost_type}}</td>
                            <td class="cell100 column5">{{$value->brand}} {{$value->model}} {{$value->registration_number}}</td>
                            <td class="cell100 column5">{{$value->name}} {{$value->surname}}</td>
                            <td class="cell100 column5">{{$value->cost_quota}}</td>
                            <td class="cell100 ">{{date("d-m-Y H:i", strtotime($value->cost_date))}}</td>
                            <td class="cell100 column5">{{$value->description}}</td>


                            <td class="cell100 column5"><a href=""
                                                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                                    <i class="material-icons">mode_edit</i>
                                </a>
                                <form id="edit-form-{{$value->id}}"
                                      action="{{route('cost.edit',['id'=>$value->id])}}"
                                      method="POST" style="display: none;">
                                    <input type="hidden" name="_method" value="GET"></input>
                                    @csrf
                                </form>

                                <a href=""
                                   onclick="event.preventDefault(); document.getElementById('destroy-form-{{$value->id}}').submit();">
                                    <i class="material-icons">delete</i>

                                </a>
                                <form id="destroy-form-{{$value->id}}"
                                      action="{{route('cost.destroy',['id'=>$value->id])}}"
                                      method="POST" style="display: none;">
                                    <input type="hidden" name="_method" value="DELETE"></input>
                                    @csrf
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{ $costs->appends(Request::input())->links() }}
            </div>
        </div>
    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <script src="{{asset('https://code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    {{--<link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css')}}"--}}
    {{--rel="stylesheet">--}}
    {{--<link href="{{asset('https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">--}}


@endsection
