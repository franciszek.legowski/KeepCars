@extends('layouts.app')
@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
    <div class="container">
        <div class="card">
            <div class="card-header">
                Generuj raport pojazdu
            </div>
            <div class="card-body">
                <form action="{{route("raport.car.wykres")}}" method="POST" onsubmit="move()">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-3 text-center">
                            <h4>Wybierz pojazd :</h4>
                            <select name="car_id" id="car_id" class="auto-width" required>
                                <option selected="true" disabled="disabled" value="">Wybierz samochód...</option>
                                @foreach($cars as $key => $value)
                                    <option value="{{$value->id}}"> {{$value->brand->name}} {{$value->model}}
                                        - {{$value->registration_number}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 text-center">
                            <h4>Wybierz zakres dat:</h4>
                            <label class="h5 mt-2">Od: &nbsp; </label><input type="text" name="data_from"
                                                                             id="dataOdDoRaportu"
                                                                             required><br>
                            <label class="h5 mt-2">Do: &nbsp;</label><input type="text" name="data_to"
                                                                            id="dataDoDoRaportu" required>

                        </div>
                        <div class="col-lg-3">
                            <h4 class="text-center">Wybierz interwał :</h4>
                            <input type="radio" name="interwalRaport" value="day" required> Dzień<br>
                            <input type="radio" name="interwalRaport" value="week"> Tydzień<br>
                            <input type="radio" name="interwalRaport" value="month"> Miesiąc<br>
                            <input type="radio" name="interwalRaport" value="year">Rok
                        </div>
                        <div class="mt-3 col-lg-3">
                            <button type="submit" class="btn btn-primary">Generuj Raport</button>
                            <a class="btn btn-primary mt-3 text-white" id="btn-Convert-Html2Image">Eksportuj raport do
                                pliku JPG</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="statusBar" class="m-auto" style="display: none;">
            <h1 class=" text-center font-weight-bold">Trwa generowanie raportu</h1>
            <div class="loader offset-5"></div>
        </div>
        <script>
            function move() {
                document.getElementById('statusBar').style.display = "block";
            }
        </script>

        <div id="html-content-holder">
            @if(isset($wejscie))
                <div class="card">

                    {{--{{print_r($wejscie)}}--}}
                    <div class="card-header bg-dark text-white font-weight-bold font-italic">
                        Raport dla samochodu {{$samochod->marka}} {{$samochod->model}} w
                        okresie: {{date("d-m-Y", strtotime($data_from))}} - {{date("d-m-Y", strtotime($data_to))}}

                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h2 class="text-muted">{{$samochod->marka}} {{$samochod->model}}
                                    - {{$samochod->registration_number}}</h2>
                                <h4 class="text-muted">Rok produkcji: {{$samochod->production_year}}</h4>
                                <h4 class="text-muted"> VIN: {{$samochod->vin}}</h4>
                                <h4 class="text-muted">Aktualny przebieg: {{$samochod->mileage}} km</h4>
                            </div>
                            <div class="col-lg-6">
                                @if(file_exists('images/cars/' . $samochod->vin))
                                    <img src="{{asset('images/cars/' . $samochod->vin)}}" height="150px"/>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-dark text-white font-weight-bold font-italic">
                        Statystyki całej załogi
                    </div>
                    <div class="card" id="editor">
                        <div class="row">
                            <div class="col-sm-4 my-auto">
                                <table class="index-table">
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold text-primary ">Wydatki ogółem</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-danger">{{$wydatki_paliwo_car + $wydatki_serwisowe_car}}
                                                PLN
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Wydatki paliwowe</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$wydatki_paliwo_car}}PLN
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Pozostałe wydatki</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$wydatki_serwisowe_car}}PLN
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Suma zatankowanych litrów</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$suma_zatankowawe_litry_car}}
                                                litrów
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Ilość innych wydatków</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$ilosc_wydatkow_serwisowych_car}}</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-4 my-auto">
                                <table class="index-table">
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold  ">Przejechane kilometry</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$przejechane_kilometry_car}}</div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Średnie wydatki miesięczne</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">
                                                {{$srednie_wydatki_car}} PLN
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Średnie spalanie</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$przejechane_kilometry_car > 0 ? round(100 * $suma_zatankowawe_litry_car / $przejechane_kilometry_car,2) : 0}}
                                                l/100km
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Średni koszt 100 km</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$przejechane_kilometry_car ? round(100 * ($wydatki_paliwo_car + $wydatki_serwisowe_car) / $przejechane_kilometry_car, 2) : 0}}
                                                PLN
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Ilość tankowań</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$ilosc_tankowan_car}}</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-4 my-auto text-center">
                                <span class="chart-label-dk">Podsumowanie wydatków</span>
                                <canvas id="zaloga_DoughnutChart" height="50%"
                                        width="100%"></canvas>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Wykresy wygenerowanych kosztów w danym przedziale czasowym
                            </div>
                            <div class="card-body">
                                <canvas id="zaloga_BarChart" width="100%" height="30"></canvas>


                                <div class="row">
                                    <div class="col-sm-7 my-auto">

                                        <canvas id="zaloga_KosztyChart" width="100%" height="50"></canvas>
                                    </div>
                                    <div class="col-sm-5 my-auto">
                                        <canvas id="zalogaTypKosztow" width="100%" height="70"></canvas>
                                    </div>
                                </div>
                                <canvas id="zaloga_PrzebiegChart" width="100%" height="30"></canvas>
                            </div>
                        </div>
                    </div>

                    @foreach($users as $key => $value)
                        <div id="{{$value->id}}">
                            <div class="card-header bg-dark text-white font-weight-bold font-italic">
                                Kierowca {{$value->name}} {{$value->surname}} {{$value->email}}</div>
                            <?php

                            // STATYSTYKI KIEROWCY

                            //WYDATKI PALIWO
                            $wydatki_paliwo_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->wydatki_paliwo_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $wydatki_serwisowe_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->wydatki_serwisowe_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            ?>



                            @if($wydatki_paliwo_car_kierowca + $wydatki_serwisowe_car_kierowca == 0)
                                <script>
                                    document.getElementById('{{$value->id}}').style.display = "none";
                                </script>
                            @endif
                            <?php
                            $max_wydatki_paliwo_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->maxWydatkiPaliwo_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $min_wydatki_paliwo_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->minWydatkiPaliwo_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $avg_wydatki_paliwo_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->avg_wydatkiPaliwo_car_kierowca($value->id, $car_id, $data_from, $data_to);

                            //PRZEJECHANE KILOMETRY
                            $suma_przejechane_km_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->przejechane_kilometry_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $max_trasa_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->maxTrasa_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $min_trasa = (new App\Http\Controllers\ChartsController)
                                ->minTrasa_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $srednia_km = (new App\Http\Controllers\ChartsController)
                                ->avg_trasa_car_kierowca($value->id, $car_id, $data_from, $data_to);

                            //PODSUMOWANIE OKRESU
                            $srednie_spalanie = (new App\Http\Controllers\ChartsController)
                                ->avg_spalanie_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $srednia_km_mies = (new App\Http\Controllers\ChartsController)
                                ->avg_trasa_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $wydatki_serwisowe = (new App\Http\Controllers\ChartsController)
                                ->wydatki_serwisowe_car_kierowca($value->id, $car_id, $data_from, $data_to);
                            $srednie_wydatki_serwisowe = (new App\Http\Controllers\ChartsController)
                                ->avg_wydatki_serwisowe_car_kierowca($value->id, $car_id, $data_from, $data_to);


                            //DANE DO WYKRESÓW
                            //KOSZTY PALIOW I SERWIS
                            $wykres_wydatki_paliwo_i_serwis_car_kierowca = (new App\Http\Controllers\ChartsController)
                                ->wykresSlupkowyKosztowPaliwowychOrazSerwisowych_kierowca(
                                    $value->id, $car_id, $data_from, $data_to, $step, $interval);
                            $wykres_wydatki_paliwo_kierowca = $wykres_wydatki_paliwo_i_serwis_car_kierowca['paliwo'];
                            $wykres_wydatki_serwis_kierowca = $wykres_wydatki_paliwo_i_serwis_car_kierowca['serwis'];
                            array_push($wykresKosztyPaliwaWszyscy, json_encode($wykres_wydatki_paliwo_kierowca));
                            array_push($wykresKosztySerwisoweWszyscy, json_encode($wykres_wydatki_serwis_kierowca));

                            //PRZEBIEG KILOMETRÓW
                            $wykresPrzebiegKilometrow = (new App\Http\Controllers\ChartsController)
                                ->wykresLiniowyPrzebieguKilometrow_car_kierowca($value->id, $car_id, $data_from, $data_to, $step, $interval);
                            array_push($wykresPrzebiegKilometrowWszyscy, json_encode($wykresPrzebiegKilometrow));

                            //ŚREDNIE SPALANIE
                            $wykresSrednieSpalanie = (new App\Http\Controllers\ChartsController)
                                ->wykresLiniowySrednieSpalanie_car_kierowca($value->id, $car_id, $data_from, $data_to, $step, $interval);
                            array_push($wykresSrednieSpalanieWszyscy, json_encode($wykresSrednieSpalanie));
                            ?>

                            <div class="card-header">
                                Statystyki kierowcy
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 my-auto text-center">
                                            <div class="font-weight-bold text-info mb-4">Wydatki na paliwo</div>
                                            <div class="font-weight-bold">Suma wydatków</div>
                                            <div class="h4 mb-0 text-primary">{{$wydatki_paliwo_car_kierowca}} zł</div>
                                            <hr>
                                            <div class="font-weight-bold">Największe wydatki w okresie</div>
                                            <div class="h4 mb-0 text-danger">{{$max_wydatki_paliwo_car_kierowca}}zł
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Najmniejsze wydatki w okresie</div>
                                            <div class="h4 mb-0 text-success"> {{$min_wydatki_paliwo_car_kierowca}}zł
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Średnie wydatki w okresie</div>
                                            <div class="h4 mb-0 text-info">{{$avg_wydatki_paliwo_car_kierowca}} zł</div>
                                        </div>
                                        <div class="col-sm-4 my-auto text-center">
                                            <div class="font-weight-bold text-info mb-4">Przejechane kilometry</div>
                                            <div class="font-weight-bold">Suma kilometrów</div>
                                            <div class="h4 mb-0 text-primary">{{$suma_przejechane_km_car_kierowca}}km
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Największa trasa</div>
                                            <div class="h4 mb-0 text-danger">{{$max_trasa_car_kierowca}} km</div>
                                            <hr>
                                            <div class="font-weight-bold">Najmniejsza trasa</div>
                                            <div class="h4 mb-0 text-success">{{$min_trasa}} km</div>
                                            <hr>
                                            <div class="font-weight-bold">Średnia długość trasy w okresie</div>
                                            <div class="h4 mb-0 text-info">{{$srednia_km}} km</div>
                                        </div>
                                        <div class="col-sm-4 my-auto text-center">
                                            <div class="font-weight-bold text-info mb-4">Podsumowanie okresu</div>
                                            <div class="font-weight-bold">Suma wydatków serwisowych w okresie</div>
                                            <div class="h4 mb-0 text-primary">{{$wydatki_serwisowe}} zł</div>
                                            <hr>
                                            <div class="font-weight-bold">Średni przebieg miesięczny</div>
                                            <div class="h4 mb-0 text-info">{{$srednia_km_mies}} km</div>
                                            <hr>
                                            <div class="font-weight-bold">Średnie wydatki serwisowe w okresie</div>
                                            <div class="h4 mb-0 text-info">{{$srednie_wydatki_serwisowe}} zł</div>
                                            <hr>
                                            <div class="font-weight-bold">Średnie spalanie</div>
                                            <div class="h4 mb-0 text-info">{{$srednie_spalanie}} l/100km</div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <canvas id="<?php echo $value->email; ?>_BarChart" width="100%" height="30"></canvas>
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- Bar Chart-->

                                    <canvas id="<?php echo $value->email; ?>_AreaChart" width="100"
                                            height="70"></canvas>

                                    <!-- /Card Columns-->
                                </div>
                                <div class="col-lg-6">
                                    <!-- Pie Chart-->
                                    <canvas id="<?php echo $value->email; ?>_WykresLiniowySrednieSpalanie"
                                            width="100%"
                                            height="70"></canvas>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>


        {{--scripts--}}
        <script src="{{asset('js/charts.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

        <script>
            onload = function () {
                var daty = JSON.parse(<?php echo json_encode($tablica_daty);?>);

                var diffDays = Math.round(Math.abs((new Date(daty[1]).getTime() - new Date(daty[0]).getTime()) / (86400000)));
                if (diffDays > 20 && diffDays < 40)
                    for (var i = 0; i < daty.length; i++) {
                        daty[i] = daty[i].substring(0, daty[i].length - 3);
                    }
                if (diffDays > 300)
                    for (var i = 0; i < daty.length; i++) {
                        daty[i] = daty[i].substring(0, daty[i].length - 6);
                    }
                if (diffDays > 2 && diffDays < 20) {
                    for (var i = 0; i < daty.length - 1; i++) {
                        daty[i] = daty[i] + "/" + daty[i + 1].substring(8);
                    }
                    // daty[daty.length] = daty[daty.length]+ "/" +String((parseInt(daty[daty.length].substring(8))%31));
                }
                var wykresLiniowyPrzebieguKilometrow_km = JSON.parse(<?php echo json_encode($wykresLiniowyPrzebieguKilometrow_km); ?>);
                var wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo = JSON.parse(<?php echo json_encode($wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo);?>);
                var wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis = JSON.parse(<?php echo json_encode($wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis);?>);
                var wykresKolowyKosztowSerwisowych_nazwy = JSON.parse(<?php echo json_encode($tablica_nazwy_kosztow);?>);
                var wykresKolowyKosztowSerwisowych_dane = JSON.parse(<?php echo json_encode($tablica_koszty_wykres_kolowy);?>);
                var kosztyWykresLiniowyKosztowSerwisowych = JSON.parse(<?php echo json_encode($tablica_koszty);?>);

                wykresDoughnutProporcjiKosztowPaliwowychDoSerwisowych('zaloga_DoughnutChart', '{{$wydatki_paliwo_car}}', {{$wydatki_serwisowe_car}});
                wykresKolowyPodzialuKosztowNaRodzaje('zalogaTypKosztow', wykresKolowyKosztowSerwisowych_nazwy, wykresKolowyKosztowSerwisowych_dane);
                wykresLiniowyKosztowSerwisowych('zaloga_KosztyChart', daty, kosztyWykresLiniowyKosztowSerwisowych);
                wykresSlupkowyKosztowPaliwowychOrazSerwisowych('zaloga_BarChart', daty, wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo, wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis);
                wykresLiniowyPrzebieguKilometrow('zaloga_PrzebiegChart', daty, wykresLiniowyPrzebieguKilometrow_km);
                var srednie_spalanie_car = JSON.parse(<?php echo json_encode($srednie_spalanie_car); ?>);
                        @foreach($users as $key => $value)

                var complex = JSON.parse( <?php echo json_encode($wykresSrednieSpalanieWszyscy[$key]); ?>);
                var wykres_wydatki_paliwo = JSON.parse(<?php echo json_encode($wykresKosztyPaliwaWszyscy[$key]); ?>);
                var wykres_wydatki_serwis = JSON.parse(<?php echo json_encode($wykresKosztySerwisoweWszyscy[$key]); ?>);
                var wykres_przebiegu_kilometrow = JSON.parse(<?php echo json_encode($wykresPrzebiegKilometrowWszyscy[$key]); ?>);

                wykresLiniowyPrzebieguKilometrow('<?php echo $value->email; ?>_AreaChart', daty, wykres_przebiegu_kilometrow);
                lineChartSrednieSpalanie('<?php echo $value->email; ?>_WykresLiniowySrednieSpalanie', daty, complex, srednie_spalanie_car);
                // wykresKolowyPodzialuKosztowNaRodzaje('<?php echo $value->email; ?>_PieChart');
                wykresSlupkowyKosztowPaliwowychOrazSerwisowych('<?php echo $value->email; ?>_BarChart', daty, wykres_wydatki_paliwo, wykres_wydatki_serwis);
                @endforeach
            }

        </script>
        @endif

        <div id="previewImage" style="display: none;"></div>

        <script>

            window.addEventListener("load", function () {
                var element = $("#html-content-holder"); // global variable
                var getCanvas; // global variable
                setTimeout(animation, 3000);

                function animation() {
                    html2canvas(element, {
                        onrendered: function (canvas) {
                            $("#previewImage").append(canvas);
                            getCanvas = canvas;
                        }
                    });
                }

                $("#btn-Convert-Html2Image").on('click', function () {
                    var imgageData = getCanvas.toDataURL("image/png");
                    // Now browser starts downloading it instead of just showing it
                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                    $("#btn-Convert-Html2Image").attr("download", "raport_kierowca.png").attr("href", newData);
                });
            }, false);


        </script>
    </div>

@endsection