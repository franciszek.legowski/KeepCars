@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Generowanie raportu do tabeli</div>

            <div class="card-body">
                Raport dla pojazdu czy kierowcy ?
                <button type="submit" class="btn btn-primary mx-sm-2" onclick="raportDlaPojazdu()">Pojazdu</button>
                <button type="submit" class="btn btn-primary mx-sm-2" onclick="raportDlaKierowcy()">Kierowcy</button>
            </div>
        </div>

        <div class="card" id="raportPojazdu" style="display: none;">
            <div class="card-header">Raport Pojazdu</div>
            <form action="{{route("raport.car")}}" method="POST">
                {{ csrf_field() }}
                <div class="card-body">

                    <div>
                        <label for="chart_date_from">Wygeneruj raport
                            od :</label>
                        {{--Firefox, IE <9 nie odsługuje typu month--}}
                        <input type="text" class="mx-sm-2" id="dataOdDoRaportu_two" name="data_from" required>
                        <br>
                        <label for="chart_date_to">Wygeneruj raport do
                            :</label>
                        <input type="text" class="mx-sm-2" id="dataDoDoRaportu_two" name="data_to" required>
                    </div>
                    <div class="margin-top-20px">
                        Rodzaj kosztów :<br>
                        <input type="radio" name="costType" value="fuelCosts"> Tylko koszty paliwa<br>
                        <input type="radio" name="costType" value="carCosts"> Tylko koszty amortyzacyjne<br>
                        <input type="radio" name="costType" value="bothCosts"> Oba rodzaje kosztów
                    </div>
                    <div class="margin-top-20px">
                        Rodzaj raportu :<br>
                        <input type="radio" name="cos" value="konkretnyPojazd" onchange="pojazdSelected()"> Dla
                        konkretnego
                        pojazdu<br>
                        <input type="radio" name="cos" value="calaFlota" onchange="flotaSelected()"> Dla całej floty
                        pojazdów
                    </div>
                    <div id="konkretnyPojazdSelectDiv" class="margin-top-20px">
                        Wybierz pojazd:
                        <select id="car_id" name="car_id">
                            <option>Wybierz samochód...</option>
                            @foreach($cars as $key => $value)
                                <option value="{{$value->id}}"
                                        @if(old('car_id')==$value->id)selected @endif>{{$value->brand->name}} {{$value->model}}
                                    - {{$value->registration_number}}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--<a href="{{route('raport.single.car',[1, '2018-01-02','2018-04-30'])}}">--}}
                    <button type="submit" class="btn btn-primary margin-top-20px">Generuj Raport</button>
                    {{--</a>--}}
                </div>
            </form>
        </div>


        <div class="card" id="raportKierowcy" style="display: none;">
            <div class="card-header">Raport Kierowcy</div>

            <div class="card-body">
                <form action="{{route("raport.kierowca")}}" method="post">
                    {{ csrf_field() }}

                    <div>
                        <label for="chart_date_from">Wygeneruj raport
                            od :</label>
                        <input type="text" class="mx-sm-2" id="dataOdDoRaportu" name="data_from" required>
                        <br>
                        <label for="chart_date_to">Wygeneruj raport do
                            :</label>
                        <input type="text" class="mx-sm-2" id="dataDoDoRaportu" name="data_to" required>
                    </div>

                    <div class="margin-top-20px">
                        <div class="margin-top-20px">
                            Rodzaj raportu :<br>
                            <input type="radio" name="costType" value="refuels"> Koszty tankowań<br>
                            <input type="radio" name="costType" value="serwices"> Koszty serwisowe<br>
                            <input type="radio" name="costType" value="both"> Koszty całkowite
                        </div>

                        <div class="margin-top-20px">
                            <div class="margin-top-20px">
                                Rodzaj raportu :<br>
                                <input type="radio" name="cos" value="user1" onchange="konkretnyUserDivSelected()"> Dla
                                konkretnego kierowcy<br>
                                <input type="radio" name="cos" value="allUser" onchange="kazdyUserDivSelected()"> Dla
                                wszystkich
                                kierowców
                            </div>
                            <div class="margin-top-20px" id="konkretnyUserDiv">
                                Wybierz użytkownika:
                                <select id="user_id" name="user_id">
                                    <option>Wybierz użytkownika...</option>
                                    @foreach($users as $key => $value)
                                        <option value="{{$value->id}}"
                                                @if(old('user_id')==$value->id)selected @endif>{{$value->name}} {{$value->surname}}
                                            --- {{$value->email}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary margin-top-20px">Generuj Raport</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>

        document.getElementById('raportKierowcy').style.display = "none";
        document.getElementById('raportPojazdu').style.display = "none";
        document.getElementById('konkretnyPojazdSelectDiv').style.display = "none";
        document.getElementById('konkretnyUserDiv').style.display = 'none';


        function raportDlaPojazdu() {
            document.getElementById('raportKierowcy').style.display = "none";
            document.getElementById('raportPojazdu').style.display = "block";
        }

        function raportDlaKierowcy() {
            document.getElementById('raportPojazdu').style.display = "none";
            document.getElementById('raportKierowcy').style.display = "block";
        }


        function pojazdSelected() {
            document.getElementById('konkretnyPojazdSelectDiv').style.display = "block";

        }

        function modelSelected() {
            document.getElementById('konkretnyPojazdSelectDiv').style.display = "none";
        }

        function flotaSelected() {
            document.getElementById('konkretnyPojazdSelectDiv').style.display = "none";
        }

        function konkretnyUserDivSelected() {
            document.getElementById('konkretnyUserDiv').style.display = 'block';
        }

        function kazdyUserDivSelected() {
            document.getElementById('konkretnyUserDiv').style.display = 'none';
        }

    </script>


@endsection

