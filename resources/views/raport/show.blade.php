@if(!$export)
    @extends('layouts.app')
@section('content')
    {{--{{print_r(array_keys(get_defined_vars()))}}<br>--}}
    {{--{{print_r($export)}}<br>--}}
    {{--{{print_r($parametry)}}<br>--}}
    {{--{{print_r($kierowca)}}<br>--}}
    {{--{{print_r($tankowania)}}<br>--}}
    {{--{{print_r($serwis)}}<br>--}}
    <div style="width: 90%;" class="m-auto">
        {{--<div class="container">--}}
        <?php $route = isset($kierowca) ? "raport.kierowca" : "raport.car";?>
        <form action="{{route($route, ['export'=>True])}}" method="post">

            {{ csrf_field() }}

            <input type="hidden" name="data_from" value="{{$parametry['data_from']}}">
            <input type="hidden" name="data_to" value="{{$parametry['data_to']}}">
            <input type="hidden" name="costType" value="{{$parametry['costType']}}">
            @if(isset($samochod))
                <input type="hidden" name="car_id" value="{{$parametry['car_id']}}">
            @elseif (isset($kierowca))
                <input type="hidden" name="user_id" value="{{$parametry['user_id']}}">
            @endif
            <input type="hidden" name="cos" value="{{$parametry['cos']}}">
            <button type="submit" class="btn-primary btn offset-5">Eksportuj dane do programu excel</button>
        </form>
        @endif
        @if (isset($samochod))
            @foreach($samochod as $c => $car)
                <div class="card">

                    {{--{{print_r($wejscie)}}--}}
                    <div class="card-header bg-dark text-white font-weight-bold font-italic">
                        Eksport danych samochodu {{$car->marka}} {{$car->model}} w
                        okresie: {{$daty}}

                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h2 class="text-muted">{{$car->marka}} {{$car->model}}
                                    - {{$car->registration_number}}</h2>
                                <h4 class="text-muted">Rok produkcji: {{$car->production_year}}</h4>
                                <h4 class="text-muted"> VIN: {{$car->vin}}</h4>
                                <h4 class="text-muted">Aktualny przebieg: {{$car->mileage}} km</h4>
                            </div>
                            <div class="col-lg-6">
                                @if(file_exists('images/cars/' . $car->vin))
                                    <img src="{{asset('images/cars/' . $car->vin)}}" height="150px"/>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    @if(isset($tankowania))
                        <div class="col-lg-6">
                            <h2 class="text-center">Tankowania</h2>
                            <table class="m-auto index-table">
                                <tr class="description-tr">
                                    <th>Kierowca</th>
                                    <th>Koszt</th>
                                    <th>Przebieg</th>
                                    <th>Data</th>
                                </tr>
                                @foreach($tankowania as $key => $value)
                                    @if($value->car_id == $car->id)
                                        <tr class="description-tr">
                                            <td>{{$value->user_id->name}} {{$value->user_id->surname}}</td>
                                            <td>{{$value->refuel_quota}}</td>
                                            <td>{{$value->mileage}}</td>
                                            <td> {{date("d-m-Y H:m", strtotime($value->refuel_date))}}</td>
                                        </tr>

                                        @if($key == count($tankowania) - 1 ||
                                        $tankowania[$key]->car_id != $tankowania[$key + 1]->car_id ||
                                        $tankowania[$key]->user_id != $tankowania[$key + 1]->user_id)
                                            <tr class="description-tr">
                                                <td><b>Suma kosztów :</b></td>
                                                <td>
                                                    <b>{{$suma_czesciowa_paliwo[$car->id . "_" . $value->user_id->id]}}</b>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </table>
                            <div class="row ml-2">
                                <div class="col-lg-5"><h3 class="font-weight-bold text-info">Łączny koszt</h3></div>
                                <div class="col-lg-4"><h3
                                            class="font-weight-bold text-info"> {{$suma_calkowita_paliwo[$c]}} zł</h3>
                                </div>
                            </div>
                        </div>
                        {{--@endif--}}
                    @endif
                    <br><br>
                    @if(isset($serwis))
                        <div class="col-lg-6">

                            <table class="m-auto index-table">
                                <tr class="description-tr">
                                    <h2 class="text-center">Koszty serwisowe</h2>
                                </tr>
                                <tr class="description-tr">
                                    <th>Kierowca</th>
                                    <th>Koszt</th>
                                    <th>Typ</th>
                                    <th>Data</th>
                                </tr>
                                @foreach($serwis as $key => $value)
                                    @if($value->car_id == $car->id)
                                        <tr class="description-tr">
                                            <td>{{$value->user_id->name}} {{$value->user_id->surname}}</td>
                                            {{--                                    <td>{{$value->user_id}}</td>--}}
                                            <td>{{$value->koszt}}</td>
                                            <td>{{$value->typ_kosztu}}</td>
                                            <td> {{date("d-m-Y H:m", strtotime($value->data))}}</td>
                                        </tr>
                                        @if($key == count($serwis) - 1 ||
                                        $serwis[$key]->car_id != $serwis[$key + 1]->car_id ||
                                        $serwis[$key]->user_id != $serwis[$key + 1]->user_id)
                                            <tr class="description-tr">
                                                <td><b>Suma kosztów:</b></td>
                                                <td>
                                                    <b>{{$suma_czesciowa_serwis[$car->id . "_" . $value->user_id->id]}}</b>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </table>

                            <div class="row ml-2">
                                <div class="col-lg-5"><h3 class="font-weight-bold text-info">Łączny koszt</h3></div>
                                <div class="col-lg-4"><h3
                                            class="font-weight-bold text-info"> {{$suma_calkowita_serwis[$c]}} zł</h3>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            @endforeach

        @elseif(isset($kierowca))

            @foreach($kierowca as $c => $user)
                <div class="card">

                    {{--{{print_r($wejscie)}}--}}
                    <div class="card-header bg-dark text-white font-weight-bold font-italic">
                        Eksport danych kierowcy {{$user->name}} {{$user->surname}} w
                        okresie: {{$daty}}
                    </div>
                    <div class="card-body">
                        <h2 class="text-muted">{{$user->name}} {{$user->surname}} - {{$user->email}}</h2>
                        <h4 class="text-muted">Rola: {{$user->rola}}</h4>
                    </div>
                </div>

                <div class="row mt-3">
                    @if(isset($tankowania))
                        <div class="col-lg-6">
                            <h2 class="text-center">Tankowania</h2>
                            <table class="index-table">

                                <tr class="description-tr">
                                    <th>Samochód</th>
                                    <th>Koszt</th>
                                    <th>Przebieg</th>
                                    <th>Data</th>
                                </tr>
                                @foreach($tankowania as $key => $value)
                                    @if($value->user_id == $user->id)
                                        <tr class="description-tr">
                                            <td>{{$value->car_id->marka}} {{$value->car_id->model}} {{$value->car_id->registration_number}}</td>
                                            <td>{{$value->refuel_quota}}</td>
                                            <td>{{$value->mileage}}</td>
                                            <td> {{date("d-m-Y H:m", strtotime($value->refuel_date))}}</td>
                                        </tr>

                                        @if($key == count($tankowania) - 1 ||
                                        $tankowania[$key]->car_id != $tankowania[$key + 1]->car_id ||
                                        $tankowania[$key]->user_id != $tankowania[$key + 1]->user_id)
                                            <tr class="description-tr">
                                                <td><b>Suma</b></td>
                                                <td>
                                                    <b>{{$suma_czesciowa_paliwo[$value->car_id->id . "_" . $user->id]}}</b>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </table>
                            <div class="row ml-2">
                                <div class="col-lg-5"><h3 class="font-weight-bold text-info">Łączny koszt</h3></div>
                                <div class="col-lg-4"><h3
                                            class="font-weight-bold text-info"> {{$suma_calkowita_paliwo[$c]}} zł</h3>
                                </div>
                            </div>
                        </div>

                    @endif

                    @if(isset($serwis))
                        <div class="col-lg-6">

                            <div>


                                <h2 class="text-center">Koszty serwisowe</h2>

                                <table class="index-table">

                                    <tr class="description-tr">
                                        <th>Samochód</th>
                                        <th>Koszt</th>
                                        <th>Typ</th>
                                        <th>Data</th>
                                    </tr>
                                    @foreach($serwis as $key => $value)
                                        @if($value->user_id == $user->id)
                                            <tr class="description-tr">
                                                <td>{{$value->car_id->marka}} {{$value->car_id->model}} {{$value->car_id->registration_number}}</td>
                                                <td>{{$value->koszt}}</td>
                                                <td>{{$value->typ_kosztu}}</td>
                                                <td> {{date("d-m-Y H:m", strtotime($value->data))}}</td>
                                            </tr>

                                            @if($key == count($serwis) - 1 ||
                                            $serwis[$key]->car_id != $serwis[$key + 1]->car_id ||
                                            $serwis[$key]->user_id != $serwis[$key + 1]->user_id)
                                                <tr class="description-tr">
                                                    <td><b>Suma</b></td>
                                                    <td>
                                                        <b>{{$suma_czesciowa_serwis[$value->car_id->id . "_" . $user->id]}}</b>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                </table>
                                <div class="row ml-2">
                                    <div class="col-lg-5"><h3 class="font-weight-bold text-info">Łączny koszt</h3></div>
                                    <div class="col-lg-4"><h3
                                                class="font-weight-bold text-info"> {{$suma_calkowita_serwis[$c]}}
                                            zł</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        @else
            <h1>Nie przesłano samochodu</h1>
        @endif
        @if(!$export)
            @endsection
        @endif
    </div>