@if (isset($samochod))
    @foreach($samochod as $c => $car)
        @if($value == $car)
            <table>
                <tr>
                    <td colspan="4">Raport dla okresu {{$daty}}</td>
                </tr>
                <tr>
                    <td colspan="4"><b>{{$car->marka}} {{$car->model}} {{$car->registration_number}}</b></td>
                </tr>
                <tr>
                    <td colspan="4">Przebieg: {{$car->mileage}} VIN: {{$car->vin}} Rok
                        produkcji: {{$car->production_year}}</td>
                </tr>
            </table>
            @if(isset($tankowania))

                <table>
                    <tr>
                        <th colspan="4">Tankowania</th>
                    </tr>
                    <tr>
                        <th>Kierowca</th>
                        <th>Koszt</th>
                        <th>Przebieg</th>
                        <th>Data</th>
                    </tr>
                    @foreach($tankowania as $key => $value)
                        @if($value->car_id == $car->id)
                            <tr>
                                <td>{{$value->user_id->name}} {{$value->user_id->surname}} {{$value->user_id->rola}}</td>
                                <td>{{$value->refuel_quota}}</td>
                                <td>{{$value->mileage}}</td>
                                <td>{{$value->refuel_date}}</td>
                            </tr>

                            @if($key == count($tankowania) - 1 ||
                            $tankowania[$key]->car_id != $tankowania[$key + 1]->car_id ||
                            $tankowania[$key]->user_id != $tankowania[$key + 1]->user_id)
                                <tr>
                                    <td><b>Suma</b></td>
                                    <td><b>{{$suma_czesciowa_paliwo[$car->id . "_" . $value->user_id->id]}}</b></td>
                                </tr>
                            @endif
                        @endif
                    @endforeach
                    <tr>
                        <td>Łączny koszt</td>
                        <td>{{$suma_calkowita_paliwo[$c]}}</td>
                    </tr>
                </table>

            @endif

            @if(isset($serwis))

                <table>
                    <tr>
                        <th colspan="4">Koszty serwisowe</th>
                    </tr>
                    <tr>
                        <th>Kierowca</th>
                        <th>Koszt</th>
                        <th>Typ</th>
                        <th>Opis</th>
                        <th>Data</th>
                    </tr>
                    @foreach($serwis as $key => $value)
                        @if($value->car_id == $car->id)
                            <tr>
                                <td>{{$value->user_id->name}} {{$value->user_id->surname}} {{$value->user_id->rola}}</td>
                                {{--                                    <td>{{$value->user_id}}</td>--}}
                                <td>{{$value->koszt}}</td>
                                <td>{{$value->typ_kosztu}}</td>
                                <td>{{$value->opis}}</td>
                                <td>{{$value->data}}</td>
                            </tr>

                            @if($key == count($serwis) - 1 ||
                            $serwis[$key]->car_id != $serwis[$key + 1]->car_id ||
                            $serwis[$key]->user_id != $serwis[$key + 1]->user_id)
                                <tr>
                                    <td><b>Suma</b></td>
                                    <td><b>{{$suma_czesciowa_serwis[$car->id . "_" . $value->user_id->id]}}</b></td>
                                </tr>
                            @endif
                        @endif
                    @endforeach
                    <tr>
                        <td>Łączny koszt</td>
                        <td>{{$suma_calkowita_serwis[$c]}}</td>
                    </tr>

                </table>
            @endif
        @endif
    @endforeach
@else
    @foreach($kierowca as $c => $user)
        @if($value == $user)
            <table>
                <tr>
                    <td colspan="4">Raport dla okresu {{$daty}}</td>
                </tr>
                <tr>
                    <td colspan="4"><b>{{$user->name}} {{$user->surname}}</b></td>
                </tr>
                <tr>
                    <td colspan="4">Mail: {{$user->email}} Rola: {{$user->rola}}</td>
                </tr>
            </table>
            <br><br>
            @if(isset($tankowania))
                <table>
                    <tr>
                        <th colspan="4">Tankowania</th>
                    </tr>
                    <tr>
                        <th>Samochód</th>
                        <th>Koszt</th>
                        <th>Przebieg</th>
                        <th>Data</th>
                    </tr>
                    @foreach($tankowania as $key => $value)
                        @if($value->user_id == $user->id)
                            <tr>
                                <td>{{$value->car_id->marka}} {{$value->car_id->model}} {{$value->car_id->registration_number}}</td>
                                <td>{{$value->refuel_quota}}</td>
                                <td>{{$value->mileage}}</td>
                                <td>{{$value->refuel_date}}</td>
                            </tr>

                            @if($key == count($tankowania) - 1 ||
                            $tankowania[$key]->car_id != $tankowania[$key + 1]->car_id ||
                            $tankowania[$key]->user_id != $tankowania[$key + 1]->user_id)
                                <tr>
                                    <td><b>Suma</b></td>
                                    <td><b>{{$suma_czesciowa_paliwo[$value->car_id->id . "_" . $user->id]}}</b>
                                    </td>
                                </tr>
                            @endif
                        @endif
                    @endforeach
                    <tr>
                        <td>Łączny koszt</td>
                        <td>{{$suma_calkowita_paliwo[$c]}}</td>
                    </tr>
                </table>

            @endif

            @if(isset($serwis))
                <div>

                    <table>
                        <tr>
                            <th colspan="5">Koszty serwisowe</th>
                        </tr>
                        <tr>
                            <th>Samochód</th>
                            <th>Koszt</th>
                            <th>Typ</th>
                            <th>Opis</th>
                            <th>Data</th>
                        </tr>
                        @foreach($serwis as $key => $value)
                            @if($value->user_id == $user->id)
                                <tr>
                                    <td>{{$value->car_id->marka}} {{$value->car_id->model}} {{$value->car_id->registration_number}}</td>
                                    <td>{{$value->koszt}}</td>
                                    <td>{{$value->typ_kosztu}}</td>
                                    <td>{{$value->opis}}</td>
                                    <td>{{$value->data}}</td>
                                </tr>

                                @if($key == count($serwis) - 1 ||
                                $serwis[$key]->car_id != $serwis[$key + 1]->car_id ||
                                $serwis[$key]->user_id != $serwis[$key + 1]->user_id)
                                    <tr>
                                        <td><b>Suma</b></td>
                                        <td>
                                            <b>{{$suma_czesciowa_serwis[$value->car_id->id . "_" . $user->id]}}</b>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                        <tr>
                            <td>Łączny koszt</td>
                            <td>{{$suma_calkowita_serwis[$c]}}</td>
                        </tr>

                    </table>
                </div>
            @endif
        @endif
    @endforeach
@endif