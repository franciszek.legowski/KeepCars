<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KeepCars</title>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <!--&lt;!&ndash; Bootstrap core CSS &ndash;&gt;-->
    <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!---->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="{{asset('magnific-popup/magnific-popup.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/creative.min.css')}}" rel="stylesheet">

    <style>
        body{
        }
    </style>

</head>

<body id="page-top">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">KeepCars</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">O nas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Zalety</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Kontakt</a>
                </li>
                <li class="nav-item">
                    @if (Route::has('login'))
                        @auth
                            <a href="{{ url('/home') }}">Home</a>
                        @else
                            <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Zaloguj się</a>
                        @endauth
                    @endif
                </li>
            </ul>
        </div>
    </div>
</nav>

<header class="masthead text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-10 mx-auto mt-5">
                <h1 class="text-uppercase">
                    <strong class="" style="font-family: 'Lato', sans-serif; letter-spacing: 3px; font-size: 105%;">Oszczędzaj zmniejszając wydatki</strong>
                </h1>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>
            <div class="col-lg-8 mx-auto">
                <p class="mt-2 text-white font-weight-bold" style="font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif;">KeepCars to prosty i wygodny sposób na kontrolowanie kosztów
                    utrzymania floty pojazdów służbowych</p>
                <a class="btn btn-primary btn-xl mt-3 js-scroll-trigger " href="#about">Szczegóły</a>
            </div>
        </div>
    </div>
</header>

<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading text-white">Mamy to czego potrzebujesz !</h2>
                <hr class="light my-4">
                <p class="text-faded mb-4">
                    Śledź wydatki związane z paliwem i kosztami serwisowymi. Kontroluj zużycie paliwa każdego i
                    wszystkich pracowników dzięki
                    prezentacji użytecznych danych w raportach wraz z intuicyjnymi wykresami.
                    Aplikacja KeepCars pozwala w jednym miejscu przechowywać informacje o wszystkich pojazdach firmy,
                    ich tankowaniach i wszystkich innych wydatkach. Dzięki aplikacji mobilnej dla kierowców i mechaników
                    dane wprowadzane są w
                    sposób prosty i nieinwazyjny.

                </p>
                <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Usługi</a>
            </div>
        </div>
    </div>
</section>

<section id="services" class="back-image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Nasze Zalety</h2>
                <hr class="my-4">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-area-chart text-white mb-3 sr-icons"></i>
                    <h3 class="mb-3">Intuicyjne Wykresy</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-mobile-phone text-white mb-3 sr-icons"></i>
                    <h3 class="mb-3">Aplikacja mobilna</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-money text-white mb-3 sr-icons"></i>
                    <h3 class="mb-3">Łatwy sposób na ograniczenie wydatków</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-newspaper-o text-white mb-3 sr-icons"></i>
                    <h3 class="mb-3">Przeglądanie wszystkich kosztów</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading">Skontaktuj się już dziś !</h2>
                <hr class="my-4">
                <p class="mb-5">Skontaktuj się z nami w celu prezentacji naszej propozycji aplikacji KeepCars
                    dostosowanej specialnie
                    do potrzeb Twojej firmy</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center">
                <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                <p>609-156-258</p>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                <p>
                    <a href="mailto:zesp08@mat.umk.pl">zesp08@mat.umk.pl</a>
                </p>
            </div>
        </div>
    </div>
</section>

<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright © Zespół VIII 2018</small>
        </div>
    </div>
</footer>


<!-- Bootstrap core JavaScript -->
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('scrollreveal/scrollreveal.min.js')}}"></script>
<script src="{{asset('magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('js/creative.min.js')}}"></script>

</body>

</html>

