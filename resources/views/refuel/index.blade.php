@extends('layouts.app')

@section('content')
    <div class="container">
        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('refuel.create') }}">
            <button type="submit" class="btn btn-primary mb-3">
                Nowe Tankowanie <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <form action="{{ route('refuel.index') }}" method="GET" accept-charset="utf-8">
            <?php
            use Illuminate\Support\Facades\Input;
            $car = Input::has('search_car') ? Input::get('search_car') : null;
            $user = Input::has('search_user') ? Input::get('search_user') : null;
            $refuel_quota_from = Input::has('search_refuel_quota_from') ? Input::get('search_refuel_quota_from') : null;
            $refuel_quota_to = Input::has('search_refuel_quota_to') ? Input::get('search_refuel_quota_to') : null;
            $price_of_litre_from = Input::has('search_price_of_litre_from') ? Input::get('search_price_of_litre_from') : null;
            $price_of_litre_to = Input::has('search_price_of_litre_to') ? Input::get('search_price_of_litre_to') : null;
            $amount_of_litre_from = Input::has('search_amount_of_litre_from') ? Input::get('search_amount_of_litre_from') : null;
            $amount_of_litre_to = Input::has('search_amount_of_litre_to') ? Input::get('search_amount_of_litre_to') : null;
            $refuel_date_from = Input::has('search_refuel_date_from') ? Input::get('search_refuel_date_from') : null;
            $refuel_date_to = Input::has('search_refuel_date_to') ? Input::get('search_refuel_date_to') : null;
            $mileage_from = Input::has('search_mileage_from') ? Input::get('search_mileage_from') : null;
            $mileage_to = Input::has('search_mileage_to') ? Input::get('search_mileage_to') : null;?>
            <div id="filtrowanieDiv" style="display: none" class="mb-3">
                <input id="search_car" type="search"
                       name="search_car" value="" placeholder="Samochód"
                       class="search-input-for-tabelki mb-2">
                <i class="fas fa-search "></i>

                <input id="search_user" type="search"
                       name="search_user" value="" placeholder="Użytkownik"
                       class="search-input-for-tabelki  mb-2 ml-5">
                <i class="fas fa-search "></i>

                <input id="search_refuel_quota_from" type="search"
                       name="search_refuel_quota_from" value="" placeholder="Kwota od..."
                       class="search-input-for-tabelki mb-2 ml-5">
                <i class="fas fa-search "></i>

                <input id="search_refuel_quota_to" type="search"
                       name="search_refuel_quota_to" value="" placeholder="Kwota do..."
                       class="search-input-for-tabelki mb-2 ml-5">
                <i class="fas fa-search "></i>

                <input id="search_price_of_litre_from" type="search"
                       name="search_price_of_litre_from" value=""
                       placeholder="Cena za litr od..."
                       class="search-input-for-tabelki mb-2">
                <i class="fas fa-search "></i>

                <input id="search_price_of_litre_to" type="search"
                       name="search_price_of_litre_to" value="" placeholder="Cena za litr do..."
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search "></i>

                <input id="search_amount_of_litre_from" type="search"
                       name="search_amount_of_litre_from" value=""
                       placeholder="Ilość litrów od..."
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search "></i>

                <input id="search_amount_of_litre_to" type="search"
                       name="search_amount_of_litre_to" value=""
                       placeholder="Ilość litrów do..."
                       class="search-input-for-tabelki ml-5 mb-2 ">
                <i class="fas fa-search "></i>


                <input id="dataOd" type="search"
                       name="search_refuel_date_from" value="" placeholder="Data od..."
                       class="search-input-for-tabelki mb-2">
                <i class="fas fa-search"></i>


                <input id="dataDo" type="search"
                       name="search_refuel_date_to" value="" placeholder="Data do..."
                       class="search-input-for-tabelki ml-5 mb-2">
                <i class="fas fa-search "></i>


                <input id="search_mileage_from" type="search"
                       name="search_mileage_from" value="" placeholder="Przebieg od..."
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>

                <input id="search_mileage_to" type="search"
                       name="search_mileage_to" value="" placeholder="Przebieg do..."
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>

                <br>
                <button type="submit" class="btn btn-primary offset-5 mt-3">Filtruj<i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" class="btn btn-primary mt-3 ml-2"
                        onclick="window.location='{{ url("refuel/index") }}'">Reset<i class="fas fa-undo ml-1"></i>
                </button>
            </div>
            <script>
                function filtrowanieShow() {

                    if (document.getElementById('filtrowanieDiv').style.display == "none")
                        document.getElementById('filtrowanieDiv').style.display = "block";
                    else
                        document.getElementById('filtrowanieDiv').style.display = "none";
                }
            </script>
        </form>

        <table id="refuel_table" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr class="font-weight-bold">
                <td>Pojazd</td>
                <td>Kierowca</td>
                <td>Kwota</td>
                <td>Cena</td>
                <td>Ilość</td>
                <td>Data</td>
                <td>Przebieg</td>
                <td>Edycja</td>
            </tr>
            </thead>

            @foreach($refuels as $key => $value)
                <tr>
                    <td>{{$value->brand}}
                        {{$value->model}} - {{$value->registration_number}}</td>
                    <td>{{$value->name}} {{$value->surname}}</td>
                    <td>{{$value->refuel_quota}} zł</td>
                    <td>{{$value->price_of_litre}} zł/l</td>
                    <td>{{$value->amount_of_litre}} l</td>
                    <td>{{date("d-m-Y H:i", strtotime($value->refuel_date))}} </td>
                    <td>{{$value->mileage}}</td>

                    <td><a href=""
                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                            <i class="material-icons">mode_edit</i>
                        </a>
                        <form id="edit-form-{{$value->id}}" action="{{route('refuel.edit',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="GET"></input>
                            @csrf
                        </form>
                        <a href="#"
                           onclick="event.preventDefault(); document.getElementById('destroy-form-{{$value->id}}').submit();">
                            <i class="material-icons">delete</i>
                        </a>
                        <form id="destroy-form-{{$value->id}}"
                              action="{{route('refuel.destroy',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="DELETE"></input>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

        {{ $refuels->appends(Request::input())->links() }}

    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
    <script src="{{asset('https://code.jquery.com/jquery-1.12.4.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>

@endsection
