<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Keep Cars') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css"
          integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css"
          integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{--logo fonts--}}
    <link href="https://fonts.googleapis.com/css?family=Gugi|Jua|Just+Another+Hand|Kaushan+Script|Pacifico|Shadows+Into+Light"
          rel="stylesheet">

    {{--datepicker stuff--}}

    <link rel="stylesheet" type="text/css" href="{{asset('pickerMaster/daterangepicker.css')}}"/>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">


</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md bg-dark" id="mainNav">
        <div class="container">

            <a class="navbar-brand margin-td-auto" href="{{ url('/') }}">
                <div class="d-inline-flex margin-td-auto">
                    <i class="material-icons text-white " style="font-size: 45px;">local_gas_station</i>
                    <h2 class="text-white margin-td-auto font-weight-bold" style="font-family: 'Lato', sans-serif;"> Keep</h2>
                    <h2 class="font-weight-bold margin-td-auto" style="font-family: 'Lato', sans-serif; color: #ade72a;">Cars</h2>
                    {{--{{ config('app.name', 'Laravel') }}--}}
                    {{--<img src="{{ asset('logo.png') }}">--}}
                </div>

            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li><a class="nav-link" href="{{ route('login') }}">Zaloguj się</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Witaj {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu bor-rad " aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="{{ route('refuel.create') }}">
                                    Dodaj Tankowanie
                                </a>
                                <form id="fuel_type-form" action="{{ route('refuel.create') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('cost.create') }}">
                                    Dodaj Koszt
                                </a>
                                <form id="fuel_type-form" action="{{ route('cost.create') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item d-none" href="{{ route('user.token') }}">
                                    Token
                                </a>

                                <a class="dropdown-item" href="/changePassword">
                                    Zmień hasło
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Wyloguj się
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle " href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Przeglądaj dane <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu bor-rad" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('car.index') }}">
                                    Przeglądaj listę aut
                                </a>
                                <form id="fuel_type-form" action="{{ route('car.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                                <a class="dropdown-item" href="{{ route('cost.index') }}">
                                    Przeglądaj listę kosztow
                                </a>
                                <form id="fuel_type-form" action="{{ route('cost.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                                <a class="dropdown-item" href="{{ route('refuel.index') }}">
                                    Przeglądaj listę tankowań
                                </a>
                                <form id="fuel_type-form" action="{{ route('refuel.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('user.index') }}">
                                    Przeglądaj listę użytkowników
                                </a>
                                <form id="fuel_type-form" action="{{ route('user.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('ride.index') }}">
                                    Przeglądaj przejazdy
                                </a>
                                <form id="fuel_type-form" action="{{ route('ride.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                                <a class="dropdown-item" href="{{ route('car_cost.index') }}">
                                    Przegladaj koszty pojazdu

                                </a>
                                <form id="fuel_type-form" action="{{ route('cost.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                Raportowanie <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu bor-rad" aria-labelledby="navbarDropdown">


                                <a class="dropdown-item" href="{{ route('raport.index') }}">

                                    Generuj raport kierowcy
                                </a>
                                <a class="dropdown-item" href="{{ route('raport.export') }}">

                                    Generuj raport pojazdu
                                </a>

                                <a class="dropdown-item" href="{{route('raport.excel')}}">
                                    Eksportuj dane do .xls
                                </a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                Zarządzaj Systemem <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu bor-rad" aria-labelledby="navbarDropdown">


                                <a class="dropdown-item" href="{{ route('fuel_type.index') }}">
                                    Typy paliwa
                                </a>
                                <form id="fuel_type-form" action="{{ route('fuel_type.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('brand.index') }}">
                                    Marki samochodów
                                </a>
                                <form id="fuel_type-form" action="{{ route('brand.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('role.index') }}">
                                    Role
                                </a>
                                <form id="role-form" action="{{ route('role.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('cost_type.index') }}">
                                    Typy kosztów

                                </a>
                                <form id="fuel_type-form" action="{{ route('cost_type.index') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('car.create') }}">
                                    Dodaj nowy pojazd
                                </a>
                                <form id="fuel_type-form" action="{{ route('car.create') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ route('user.create') }}">
                                    Dodaj nowego użytkownika
                                </a>
                            </div>
                        </li>

                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @include('flash-message')

        @if (Session::has('status'))
            <li>{!! session('status') !!}</li>
        @endif
        @yield('content')
    </main>
    <br/>
    <br/>
</div>
<footer id="footerDiv" class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright © Zespół VIII 2018</small>
        </div>
    </div>
</footer>
<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
        integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+"
        crossorigin="anonymous"></script>

<script type="text/javascript" src="{{asset('pickerMaster/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('pickerMaster/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('pickerMaster/daterangepicker.min.js')}}"></script>
<script>
    $(function() {
        $('#customPicker').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD HH:mm",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });
        $('#customPicker2').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            autoUpdateInput: false,
            timePicker24Hour: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD HH:mm",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });
        $('#dataDo').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            autoUpdateInput: true,
            timePicker24Hour: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD HH:mm",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });
        $('#dataOd').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            autoUpdateInput: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD HH:mm",
                "separator": ".",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });
        $('#dataDoDoRaportu').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });
        $('#dataOdDoRaportu').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });

        $('#dataDoDoRaportu_two').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,


            }

        });

        $('#dataOdDoRaportu_two').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1996,
            maxYear: 2025,
            "locale": {
                "format": "YYYY-MM-DD",
                "separator": " - ",
                "applyLabel": "Zapisz",
                "cancelLabel": "Anuluj",
                "fromLabel": "Od",
                "toLabel": "Do",
                "customRangeLabel": "Własne",
                "weekLabel": "T",
                "daysOfWeek": [
                    "Nd",
                    "Pn",
                    "Wt",
                    "Śr",
                    "Czw",
                    "Pt",
                    "Sob"
                ],
                "monthNames": ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                "firstDay": 1,
            }
        });
    });
</script>


</body>
</html>
