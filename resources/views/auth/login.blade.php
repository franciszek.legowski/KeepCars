<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>KeepCars</title>

    <!--&lt;!&ndash; Bootstrap core CSS &ndash;&gt;-->
    <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!---->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="{{asset('magnific-popup/magnific-popup.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/creative.min.css')}}" rel="stylesheet">


</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}">KeepCars</a>
    </div>
</nav>
<header class="masthead text-center text-white d-flex">
    <div class="own-login-card  mx-auto auto-width">
        <div class="card-header font-weight-bold" style="font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif; font-size: 120%;">Zaloguj się</div>

        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email" style="font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif;">E-mail</label>


                    <input id="email" type="email"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif

                </div>

                <div class="form-group">
                    <label for="password" style="font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif;">Hasło</label>

                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif


                    <div class="form-group">

                        <div class="checkbox mt-3" style="font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif;">
                            <label>
                                <input type="checkbox"  name="remember" {{ old('remember') ? 'checked' : '' }}>
                                Zapamiętaj mnie
                            </label>
                        </div>

                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn-block btn btn-primary">
                            Zaloguj się
                        </button>
                        <a class="btn btn-link text-muted mt-3" href="{{ route('password.request') }}">
                            Zapomniałeś hasła?
                        </a>
                    </div>
                </div>
            </form>
        </div>


    </div>
</header>

{{--<div class="card card-login mx-auto">--}}
{{--<div class="card-header">Zaloguj się</div>--}}

{{--<div class="card-body">--}}
{{--<form method="POST" action="{{ route('login') }}">--}}
{{--@csrf--}}

{{--<div class="form-group">--}}
{{--<label for="email">E-Mail</label>--}}


{{--<input id="email" type="email"--}}
{{--class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"--}}
{{--value="{{ old('email') }}" required autofocus>--}}

{{--@if ($errors->has('email'))--}}
{{--<span class="invalid-feedback">--}}
{{--<strong>{{ $errors->first('email') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}

{{--</div>--}}
{{--<div class="form-group">--}}
{{--<label for="password">Hasło</label>--}}

{{--<input id="password" type="password"--}}
{{--class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"--}}
{{--name="password" required>--}}

{{--@if ($errors->has('password'))--}}
{{--<span class="invalid-feedback">--}}
{{--<strong>{{ $errors->first('password') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}


{{--<div class="form-group">--}}

{{--<div class="checkbox">--}}
{{--<label>--}}
{{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>--}}
{{--Zapamiętaj mnie--}}
{{--</label>--}}
{{--</div>--}}

{{--</div>--}}

{{--<div class="form-group text-center">--}}

{{--<button type="submit" class="btn-block btn btn-primary">--}}
{{--Zaloguj się--}}
{{--</button>--}}

{{--<a class="btn btn-link text-muted mt-3" href="{{ route('password.request') }}">--}}
{{--Zapomniałeś hasła?--}}
{{--</a>--}}

{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}

<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright © Zespół VIII 2018</small>
        </div>
    </div>
</footer>


<!-- Bootstrap core JavaScript -->
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('scrollreveal/scrollreveal.min.js')}}"></script>
<script src="{{asset('magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('js/creative.min.js')}}"></script>

</body>

</html>


{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--<div class="row justify-content-center">--}}
{{--<div class="col-md-8">--}}
{{--<div class="card">--}}
{{--<div class="card-header">Zaloguj się</div>--}}

{{--<div class="card-body">--}}
{{--<form method="POST" action="{{ route('login') }}">--}}
{{--@csrf--}}

{{--<div class="form-group row">--}}
{{--<label for="email" class="col-sm-4 col-form-label text-md-right">E-Mail Address</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

{{--@if ($errors->has('email'))--}}
{{--<span class="invalid-feedback">--}}
{{--<strong>{{ $errors->first('email') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--<label for="password" class="col-md-4 col-form-label text-md-right">Password</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

{{--@if ($errors->has('password'))--}}
{{--<span class="invalid-feedback">--}}
{{--<strong>{{ $errors->first('password') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row">--}}
{{--<div class="col-md-6 offset-md-4">--}}
{{--<div class="checkbox">--}}
{{--<label>--}}
{{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
{{--</label>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row mb-0">--}}
{{--<div class="col-md-8 offset-md-4">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--Login--}}
{{--</button>--}}

{{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--Forgot Your Password?--}}
{{--</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endsection--}}