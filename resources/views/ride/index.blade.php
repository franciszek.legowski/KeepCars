@extends('layouts.app')

@section('content')
    <div class="container">
        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('ride.create') }}">
            <button type="submit" class="btn btn-primary mb-3">
                Nowy przejazd <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <div id="filtrowanieDiv" style="display: none" class="mb-3">
            <form action="{{ route('ride.index') }}" method="GET" accept-charset="utf-8">
                <?php
                use Illuminate\Support\Facades\Input;
                $user = Input::has('search_user') ? Input::get('search_user') : [];
                $car = Input::has('search_car') ? Input::get('search_car') : [];
                $ride_date_from = Input::has('search_ride_date_from') ? Input::get('search_ride_date_from') : [];
                $ride_date_to = Input::has('search_ride_date_to') ? Input::get('search_ride_date_to') : [];
                $begin_mileage = Input::has('search_begin_mileage') ? Input::get('search_begin_mileage') : [];
                $end_mileage = Input::has('search_end_mileage') ? Input::get('search_end_mileage') : [];?>

                <input id="search_user" type="search"
                       name="search_user" value="" placeholder="Użytkownik"
                       class="search-input-for-tabelki mb-2">
                <i class="fas fa-search"></i>

                <input id="search_car" type="search"
                       name="search_car" value="" placeholder="Samochód"
                       class="search-input-for-tabelki mb-2 ml-5">
                <i class="fas fa-search"></i>

                <input id="dataOd" type="search"
                       name="search_ride_date_from" value="" placeholder="Data od"
                       class="search-input-for-tabelki mb-2 ml-5">
                <i class="fas fa-search"></i>

                <input id="dataDo" type="search"
                       name="search_ride_date_to" value="" placeholder="Data do"
                       class="search-input-for-tabelki mb-2 ml-5">
                <i class="fas fa-search"></i>

                <input id="search_begin_mileage" type="search"
                       name="search_begin_mileage" value="" placeholder="Przebieg od"
                       class="search-input-for-tabelki">
                <i class="fas fa-search"></i>

                <input id="search_end_mileage" type="search"
                       name="search_end_mileage" value="" placeholder="Przebieg do"
                       class="search-input-for-tabelki ml-5">
                <i class="fas fa-search"></i>

                <br>
                <button type="submit" class="offset-5 btn btn-primary mt-3">Filtruj <i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" onclick="window.location='{{ url("ride/index") }}'"
                        class="ml-2 btn btn-primary mt-3">Reset<i class="fas fa-undo ml-1"></i></button>
            </form>
        </div>
        <script>
            function filtrowanieShow() {
                if (document.getElementById('filtrowanieDiv').style.display == "none")
                    document.getElementById('filtrowanieDiv').style.display = "block";
                else
                    document.getElementById('filtrowanieDiv').style.display = "none";
            }
        </script>
        <table id="car_table" class="table table-striped table-bordered m-md-auto">
            <thead>
            <tr class="font-weight-bold">
                <td>Kierowca</td>
                <td>Pojazd</td>
                <td>Data</td>
                <td>Przebieg początkowy</td>
                <td>Przebieg końcowy</td>
                <td>Edycja</td>

            </tr>
            </thead>
            @foreach($rides as $key => $value)
                <tr>
                    <td>{{$value->name}} {{$value->surname}}</td>
                    <td>{{$value->brand}} {{$value->model}}</td>
                    <td>{{date("d-m-Y H:i", strtotime($value->ride_date))}} </td>
                    <td>{{$value->begin_mileage}}</td>
                    <td>{{$value->end_mileage}}</td>

                    <td><a href=""
                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                            <i class="material-icons">mode_edit</i>
                        </a>
                        <form id="edit-form-{{$value->id}}" action="{{route('ride.edit',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="GET"></input>
                            @csrf
                        </form>
                        <a href=""
                           onclick="event.preventDefault(); document.getElementById('destroy-form-{{$value->id}}').submit();">
                            <i class="material-icons">delete</i>
                        </a>
                        <form id="destroy-form-{{$value->id}}" action="{{route('ride.destroy',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="DELETE"></input>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $rides->appends(Request::input())->links() }}
    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>

@endsection
