@extends('layouts.app')

@section('content')
    <div class="container">
        <button class="btn-primary btn mb-3" onclick="filtrowanieShow()" type="button">
            Filtruj wyniki <i class="fas fa-search ml-2"></i>
        </button>
        <a href="{{ route('brand.create') }}">
            <button type="submit" class="btn btn-primary mb-3">
                Nowa marka <i class="fas fa-plus ml-2"></i>
            </button>
        </a>
        <form action="{{ route('brand.index') }}" method="GET" accept-charset="utf-8">
            <?php
            use Illuminate\Support\Facades\Input;
            $name = Input::has('search_name') ? Input::get('search_name') : []; ?>
            <div id="filtrowanieDiv" style="display: none" class="mb-3">
                <input id="search_name" type="search"
                       name="search_name" value="" placeholder="Wyszukaj markę auta"
                       class="search-input-for-slowniki">
                <i class="fas fa-search"></i> <br>
                <button type="submit" class="btn btn-primary ml-3 mt-3">Filtruj<i class="fas fa-search ml-1"></i>
                </button>
                <button type="button" class="btn btn-primary mt-3 ml-2"
                        onclick="window.location='{{ url("brand/index") }}'">Reset<i class="fas fa-undo ml-1"></i>
                </button>
            </div>
        </form>
        <script>
            function filtrowanieShow() {

                if (document.getElementById('filtrowanieDiv').style.display == "none")
                    document.getElementById('filtrowanieDiv').style.display = "block";
                else
                    document.getElementById('filtrowanieDiv').style.display = "none";
            }
        </script>

        <table class="index-table" id="brandTable">
            @foreach($brands as $key => $value)
                <tr class="description-tr">
                    <td>{{$value->name}}</td>
                    &nbsp;
                    <td><a href=""
                           onclick="event.preventDefault(); document.getElementById('edit-form-{{$value->id}}').submit();">
                            <i class="material-icons">mode_edit</i>
                        </a>
                        <form id="edit-form-{{$value->id}}" action="{{route('brand.edit',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="GET"></input>
                            @csrf
                        </form>

                        <a href=""
                           onclick="event.preventDefault(); document.getElementById('destroy-form-{{$value->id}}').submit();">
                            <i class="material-icons">delete_sweep</i>
                        </a>
                        <form id="destroy-form-{{$value->id}}" action="{{route('brand.destroy',['id'=>$value->id])}}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="DELETE"></input>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $brands->appends(Request::input())->links() }}
    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>
@endsection
