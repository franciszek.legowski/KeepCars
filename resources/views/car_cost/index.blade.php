@extends('layouts.app')

@section('content')

    <div class="container">

        <script>
            var pojazd = document.location.pathname.replace("/car_cost/index", "");
            if (pojazd == "") pojazd = 1;
            else pojazd = pojazd.replace("/", "");
        </script>

        <select id="sel">

            <option selected="true" disabled="disabled">Wybierz samochód...</option>
            @foreach($cars as $key => $value)
                <option value="{{$value->id}}" @if(old('car_id')==$value->id)selected @endif>
                    {{$value->brand->name}} {{$value->model}} - {{$value->registration_number}}</option>
                <script>
                    if ("{{$value->id}}" == pojazd) {
                        var przebieg = "{{$value->mileage}}";
                        var marka = "{{$value->brand->name}}";
                        var model = "{{$value->model}}";
                        var rejestracja = "{{$value->registration_number}}";
                        var rocznik = "{{$value->production_year}}";
                        var vin = "{{$value->vin}}";
                    }
                </script>
            @endforeach
        </select>
        <button class="btn btn-primary ml-2" type="submit" onclick="trasa()"> Wybierz</button>
        <div class="card">
            <div class="card-header bg-dark text-white font-weight-bold font-italic">
                Ostatnie koszty pojazdu
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="text-muted" id="wybranyPojazd"></h2>
                        <h4 class="text-muted" id="wybranyPojazd2"></h4>
                        <h4 class="text-muted" id="wybranyPojazd3"></h4>
                        <h4 class="text-muted" id="wybranyPojazd4"></h4>
                    </div>
                    <div class="col-lg-6">
                        <img src="#" id="photoCar" height="150px"/>
                    </div>
                </div>
            </div>
        </div>
        <script>
            document.getElementById('wybranyPojazd').innerText = marka + " " + model + " - " + rejestracja;
            document.getElementById('wybranyPojazd2').innerText = "Rok produkcji:" + rocznik;
            document.getElementById('wybranyPojazd3').innerText = "VIN: " + vin;
            document.getElementById('wybranyPojazd4').innerText = "Aktualny przebieg: " + przebieg + " km";
            document.getElementById("photoCar").src = "https://192.168.134.65/images/cars/" + vin;

        </script>

        <ul class="list-ic vertical">
            <?php $counter = 1; ?>
            @if(!empty($dane))
                @foreach($dane as $key => $value)
                    <li>
                        <span><?php echo $counter; ?></span>
                        <a href="#" class="text-primary font-weight-bold" style="cursor:default;">
                            {{ $value->KOSZT }}
                        </a>
                        <a href="#" class="font-weight-bold" style="cursor:default;">
                            - {{ $value->PLN }} PLN </a>
                        <a href="#" class="font-weight-bold text-primary" style="cursor:default;">
                            dnia {{$value->DAT}} </a>
                        <a href="#" class="font-weight-bold" style="cursor:default;">
                            przez {{$value->IMIE}} {{$value->NAZWISKO}} - {{$value->KIEROWCA}}</a>
                    </li>
                    <?php $counter++; ?>
                @endforeach
            @else
                <div class="bg-dark text-center" style="height: 50px">
                    <h1 class="text-white m-md-auto">Brak historii dla tego pojazdu</h1>
                </div>
            @endif
        </ul>
    </div>


    </div>
    <script src="{{asset('js/filteredTable.js')}}"></script>
    <script>
        function trasa() {
            var id = document.getElementById("sel").value;
            // TODO zmiana adresu na właściwy do przekierowania
            window.location.replace("https://192.168.134.65/car_cost/index/" + id);
        }
    </script>


@endsection
