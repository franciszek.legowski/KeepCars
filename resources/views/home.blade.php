@extends('layouts.app')

@section('content')
<?php
//        function frand($min, $max, $decimals = 0)
//        {
//            $scale = pow(10, $decimals);
//            return mt_rand($min * $scale, $max * $scale) / $scale;
//        }
//
//
//        $samochody_all = [1,2, 5,6, 7, 8, 9,10,11,12,13,14];
//        $kierowcy = [4, 5,6, 7, 8, 9,10,11,12,13];
//        $spalania = [
//            1=>7.7,
//            2=>8.6,
//            5=>10.5,
//            6=>9.9,
//            7=>10.2,
//            8=>7.3,
//            9=>7.3,
//            10=>6.9,
//            11=>7.1,
//            12=>7.3,
//            13=>7.9,
//            14=>5.8
//
//        ];
//        $przebiegi = [
//            1=>121200,
//            2=>172321,
//            5=>214378,
//            6=>204521,
//            7=>315780,
//            8=>287004,
//            9=>95412,
//            10=>279425,
//            11=>183456,
//            12=>156780,
//            13=>3108,
//            14=>375102
//        ];
//
//        $date = '2017-10-01';
//        while ($date < '2018-05-08') {
//
//            $date = date('Y-m-d', strtotime(" {$date}  next monday"));
//            $samochody = $samochody_all;
//            shuffle($samochody);
//    //        echo print_r($samochody);
//            foreach ($kierowcy as $key => $kierowca) {
//                $samochod = $samochody[$key];
//                if ($kierowca == 4 or $kierowca==7) {
//    //                Duże spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod], $spalania[$samochod] * 1.2, 2);
//                } elseif ($kierowca == 9) {
//    //                Małe spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.82, $spalania[$samochod] * 0.93, 2);
//                } else {
//    //                Przeciętne spalanie.
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.97, $spalania[$samochod] * 1.04, 2);
//                }
//                $cenaPaliwaTegoDnia = frand(4.3, 4.6, 2);
//                $przejechaneKilometryTegoDnia = frand(200, 600, 0);
//                $przebiegPoczatkowy = $przebiegi[$samochod];
//                $przebiegi[$samochod] = $przebiegi[$samochod] + $przejechaneKilometryTegoDnia;
//                $zatankowanieLitryTegoDnia = $spalanieKierowcyTegoDnia * ($przejechaneKilometryTegoDnia / 100);
//                $zaplacil = $zatankowanieLitryTegoDnia * $cenaPaliwaTegoDnia;
//                $godzinaTankowania  =frand(14, 22, 0);
//                $minutyTankowania = frand(1, 59, 0);
//                echo "INSERT INTO `keepcars`.`refuels` (`car_id`, `user_id`, `refuel_quota`, `price_of_litre`, `amount_of_litre`, `refuel_date`, `mileage`) VALUES",
//                "('", $samochod, "', '", $kierowca, "', '", round($zaplacil,2), "', '", $cenaPaliwaTegoDnia, "', '", round($zatankowanieLitryTegoDnia,2), "', '",
//                $date, " ",$godzinaTankowania , ":",$minutyTankowania , "', '", $przebiegi[$samochod], "');";
//                echo "<br/>";
//                $przebiegi[$samochod]=($przebiegi[$samochod]+3);
//                echo "INSERT INTO `keepcars`.`rides` (`user_id`, `car_id`, `ride_date`, `begin_mileage`, `end_mileage`) VALUES",
//                    "('",$kierowca,"', '",$samochod,"', '",$date, " ",$godzinaTankowania+1 , ":",$minutyTankowania ,"', '",$przebiegPoczatkowy,"', '",$przebiegi[$samochod],"');";
//                echo "<br/>";
//
//            }
//
//
//            $date = date('Y-m-d', strtotime(" {$date}  + 1 day"));
//    //        echo $date, " ";
//            shuffle($samochody);
//    //        echo print_r($samochody);
//            foreach ($kierowcy as $key => $kierowca) {
//                $samochod = $samochody[$key];
//                if ($kierowca == 4 or $kierowca==7) {
//    //                Duże spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod], $spalania[$samochod] * 1.2, 2);
//                } elseif ($kierowca == 9) {
//    //                Małe spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.82, $spalania[$samochod] * 0.93, 2);
//                } else {
//    //                Przeciętne spalanie.
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.97, $spalania[$samochod] * 1.04, 2);
//                }
//                $cenaPaliwaTegoDnia = frand(4.3, 4.6, 2);
//                $przejechaneKilometryTegoDnia = frand(200, 600, 0);
//                $przebiegPoczatkowy = $przebiegi[$samochod];
//                $przebiegi[$samochod] = $przebiegi[$samochod] + $przejechaneKilometryTegoDnia;
//                $zatankowanieLitryTegoDnia = $spalanieKierowcyTegoDnia * ($przejechaneKilometryTegoDnia / 100);
//                $zaplacil = $zatankowanieLitryTegoDnia * $cenaPaliwaTegoDnia;
//                echo "INSERT INTO `keepcars`.`refuels` (`car_id`, `user_id`, `refuel_quota`, `price_of_litre`, `amount_of_litre`, `refuel_date`, `mileage`) VALUES",
//                "('", $samochod, "', '", $kierowca, "', '", round($zaplacil,2), "', '", $cenaPaliwaTegoDnia, "', '", round($zatankowanieLitryTegoDnia,2), "', '",
//                $date, " ", frand(14, 22, 0), ":", frand(1, 59, 0), "', '", $przebiegi[$samochod], "');";
//                echo "<br/>";
//                $przebiegi[$samochod]=($przebiegi[$samochod]+3);
//                echo "INSERT INTO `keepcars`.`rides` (`user_id`, `car_id`, `ride_date`, `begin_mileage`, `end_mileage`) VALUES",
//                "('",$kierowca,"', '",$samochod,"', '",$date, " ",$godzinaTankowania+1 , ":",$minutyTankowania ,"', '",$przebiegPoczatkowy,"', '",$przebiegi[$samochod],"');";
//                echo "<br/>";
//            }
//
//            $date = date('Y-m-d', strtotime(" {$date}  + 1 day"));
//    //        echo $date, " ";
//            shuffle($samochody);
//    //        echo print_r($samochody);
//            foreach ($kierowcy as $key => $kierowca) {
//                $samochod = $samochody[$key];
//                if ($kierowca == 4 or $kierowca==7) {
//    //                Duże spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod], $spalania[$samochod] * 1.2, 2);
//                } elseif ($kierowca == 9) {
//    //                Małe spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.82, $spalania[$samochod] * 0.93, 2);
//                } else {
//    //                Przeciętne spalanie.
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.97, $spalania[$samochod] * 1.04, 2);
//                }
//                $cenaPaliwaTegoDnia = frand(4.3, 4.6, 2);
//                $przejechaneKilometryTegoDnia = frand(200, 600, 0);
//                $przebiegPoczatkowy = $przebiegi[$samochod];
//                $przebiegi[$samochod] = $przebiegi[$samochod] + $przejechaneKilometryTegoDnia;
//                $zatankowanieLitryTegoDnia = $spalanieKierowcyTegoDnia * ($przejechaneKilometryTegoDnia / 100);
//                $zaplacil = $zatankowanieLitryTegoDnia * $cenaPaliwaTegoDnia;
//                echo "INSERT INTO `keepcars`.`refuels` (`car_id`, `user_id`, `refuel_quota`, `price_of_litre`, `amount_of_litre`, `refuel_date`, `mileage`) VALUES",
//                "('", $samochod, "', '", $kierowca, "', '", round($zaplacil,2), "', '", $cenaPaliwaTegoDnia, "', '", round($zatankowanieLitryTegoDnia,2), "', '",
//                $date, " ", frand(14, 22, 0), ":", frand(1, 59, 0), "', '", $przebiegi[$samochod], "');";
//                echo "<br/>";
//                $przebiegi[$samochod]=($przebiegi[$samochod]+3);
//                echo "INSERT INTO `keepcars`.`rides` (`user_id`, `car_id`, `ride_date`, `begin_mileage`, `end_mileage`) VALUES",
//                "('",$kierowca,"', '",$samochod,"', '",$date, " ",$godzinaTankowania+1 , ":",$minutyTankowania ,"', '",$przebiegPoczatkowy,"', '",$przebiegi[$samochod],"');";
//                echo "<br/>";
//            }
//
//            $date = date('Y-m-d', strtotime(" {$date}  + 1 day"));
//    //        echo $date, " ";
//            shuffle($samochody);
//    //        echo print_r($samochody);
//            foreach ($kierowcy as $key => $kierowca) {
//                $samochod = $samochody[$key];
//                if ($kierowca == 4 or $kierowca==7) {
//    //                Duże spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod], $spalania[$samochod] * 1.2, 2);
//                } elseif ($kierowca == 9) {
//    //                Małe spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.82, $spalania[$samochod] * 0.93, 2);
//                } else {
//    //                Przeciętne spalanie.
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.97, $spalania[$samochod] * 1.04, 2);
//                }
//                $cenaPaliwaTegoDnia = frand(4.3, 4.6, 2);
//                $przebiegPoczatkowy = $przebiegi[$samochod];
//                $przejechaneKilometryTegoDnia = frand(200, 600, 0);
//                $przebiegi[$samochod] = $przebiegi[$samochod] + $przejechaneKilometryTegoDnia;
//                $zatankowanieLitryTegoDnia = $spalanieKierowcyTegoDnia * ($przejechaneKilometryTegoDnia / 100);
//                $zaplacil = $zatankowanieLitryTegoDnia * $cenaPaliwaTegoDnia;
//                echo "INSERT INTO `keepcars`.`refuels` (`car_id`, `user_id`, `refuel_quota`, `price_of_litre`, `amount_of_litre`, `refuel_date`, `mileage`) VALUES",
//                "('", $samochod, "', '", $kierowca, "', '", round($zaplacil,2), "', '", $cenaPaliwaTegoDnia, "', '", round($zatankowanieLitryTegoDnia,2), "', '",
//                $date, " ", frand(14, 22, 0), ":", frand(1, 59, 0), "', '", $przebiegi[$samochod], "');";
//                echo "<br/>";
//                $przebiegi[$samochod]=($przebiegi[$samochod]+3);
//                echo "INSERT INTO `keepcars`.`rides` (`user_id`, `car_id`, `ride_date`, `begin_mileage`, `end_mileage`) VALUES",
//                "('",$kierowca,"', '",$samochod,"', '",$date, " ",$godzinaTankowania+1 , ":",$minutyTankowania ,"', '",$przebiegPoczatkowy,"', '",$przebiegi[$samochod],"');";
//                echo "<br/>";
//            }
//
//            $date = date('Y-m-d', strtotime(" {$date}  + 1 day"));
//    //        echo $date, "<br/>";
//            shuffle($samochody);
//    //        echo print_r($samochody);
//            foreach ($kierowcy as $key => $kierowca) {
//                $samochod = $samochody[$key];
//                if ($kierowca == 4 or $kierowca==7) {
//    //                Duże spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod], $spalania[$samochod] * 1.2, 2);
//                } elseif ($kierowca == 9) {
//    //                Małe spalanie
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.82, $spalania[$samochod] * 0.93, 2);
//                } else {
//    //                Przeciętne spalanie.
//                    $spalanieKierowcyTegoDnia = frand($spalania[$samochod] * 0.97, $spalania[$samochod] * 1.04, 2);
//                }
//                $cenaPaliwaTegoDnia = frand(4.3, 4.6, 2);
//                $przebiegPoczatkowy = $przebiegi[$samochod];
//                $przejechaneKilometryTegoDnia = frand(200, 600, 0);
//                $przebiegi[$samochod] = $przebiegi[$samochod] + $przejechaneKilometryTegoDnia;
//                $zatankowanieLitryTegoDnia = $spalanieKierowcyTegoDnia * ($przejechaneKilometryTegoDnia / 100);
//                $zaplacil = $zatankowanieLitryTegoDnia * $cenaPaliwaTegoDnia;
//                echo "INSERT INTO `keepcars`.`refuels` (`car_id`, `user_id`, `refuel_quota`, `price_of_litre`, `amount_of_litre`, `refuel_date`, `mileage`) VALUES",
//                "('", $samochod, "', '", $kierowca, "', '", round($zaplacil,2), "', '", $cenaPaliwaTegoDnia, "', '", round($zatankowanieLitryTegoDnia,2), "', '",
//                $date, " ", frand(14, 22, 0), ":", frand(1, 59, 0), "', '", $przebiegi[$samochod], "');";
//                echo "<br/>";
//                $przebiegi[$samochod]=($przebiegi[$samochod]+3);
//                echo "INSERT INTO `keepcars`.`rides` (`user_id`, `car_id`, `ride_date`, `begin_mileage`, `end_mileage`) VALUES",
//                "('",$kierowca,"', '",$samochod,"', '",$date, " ",$godzinaTankowania+1 , ":",$minutyTankowania ,"', '",$przebiegPoczatkowy,"', '",$przebiegi[$samochod],"');";
//                echo "<br/>";
//            }
//
//
//        }
//        ?>

    <div class="container">
        <div class="card">
            <div class="card-header bg-dark text-white">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                Witaj {{ Auth::user()->name }}
            </div>
            <div class="card-body">
                <h1 class="text-center"> RAPORT RÓŻNIC</h1>
                <table class="index-table auto-width">
                    <thead>
                    <tr class="description-tr h5 font-weight-bold">
                        <td>Samochód</td>
                        <td>Kierowca</td>
                        <td class="small-table-tx-max-width-150">Przebieg końcowy</td>
                        <td class="text-center">Data</td>
                        <td>Kierowca</td>
                        <td class="small-table-tx-max-width-150">Przebieg początkowy</td>
                        <td class="text-center">Data</td>
                        <td>Różnica</td>
                        <td class="text-center" style="min-width: 135px;">Edytuj przejazd&nbsp:</td>
                    </tr>
                    </thead>
                    @if(count($roznice))
                        <?php $counter = 1; ?>
                        @foreach($roznice as $value)
                            <tr id="trNum<?php echo $counter; ?>">
                                <td class="small-table-tx-max-width-250">{{$value['ride1']->car_id->marka}} {{$value['ride1']->car_id->model}} {{$value['ride1']->car_id->registration_number}} {{$value['ride1']->car_id->mileage}}
                                    km
                                </td>
                                <td>{{$value['ride1']->user_id->name}} {{$value['ride1']->user_id->surname}}</td>
                                <td>{{$value['ride1']->end_mileage}} km</td>
                                <td style="min-width: 90px;"
                                    class="text-center">{{date("d-m-Y H:i", strtotime($value['ride1']->ride_date))}}</td>
                                <td>{{$value['ride2']->user_id->name}} {{$value['ride2']->user_id->surname}}</td>
                                <td>{{$value['ride2']->begin_mileage}} km</td>
                                <td style="min-width: 90px;"
                                    class="text-center">{{date("d-m-Y H:i", strtotime($value['ride2']->ride_date))}}</td>
                                <td>{{$value['ride2']->begin_mileage - $value['ride1']->end_mileage}} km</td>
                                <td><a href="#" style="font-size: 17px; text-decoration: none"
                                       onclick="event.preventDefault();
                                               document.getElementById('edit-form-{{$value['ride1']->id}}').submit();">
                                        Pierwszy <i class="material-icons">mode_edit</i>

                                    </a>
                                    <form id="edit-form-{{$value['ride1']->id}}"
                                          action="{{route('ride.edit',['id'=>$value['ride1']->id])}}"
                                          method="POST" style="display: none;">
                                        <input type="hidden" name="_method" value="GET"></input>
                                        @csrf
                                    </form>
                                    <br>
                                    <a href="" style="font-size: 17px; text-decoration: none"
                                       onclick="event.preventDefault();
                                               document.getElementById('edit-form-{{$value['ride2']->id}}').submit();">
                                        Drugi
                                        <i class="material-icons">mode_edit</i>
                                    </a>
                                    <form id="edit-form-{{$value['ride2']->id}}"
                                          action="{{route('ride.edit',['id'=>$value['ride2']->id])}}"
                                          method="POST" style="display: none;">
                                        <input type="hidden" name="_method" value="GET"></input>
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                            <script>
                                if ("{{$value['ride2']->begin_mileage - $value['ride1']->end_mileage}}" == 0)
                                    document.getElementById('trNum<?php echo $counter; ?>').style.display = "none";
                            </script>
                            <?php $counter++; ?>
                        @endforeach
                    @endif
                </table>
                @if(!count($roznice))
                    <h1 class="text-center">Nie ma różnic w przejazdach.</h1>
                @endif
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="text-center"> Ostatnie koszty paliwowe</h2>
                        <ul class="list-ic-home-blade vertical">
                            <?php $counter = 1; ?>
                            @foreach($dane as $key => $value)
                                <li>
                                    <span><?php echo $counter; ?></span>
                                    <a href="{{route("refuel.edit", $value->ID)}}"
                                       class="text-primary font-weight-bold h4" style="color: #676d73!important;">
                                        {{ $value->LITRY }} litrów paliwa - {{ $value->PLN }} złotych </a>
                                    <div class="ml-5 font-weight-bold">
                                        Data: {{date("d-m-Y H:i", strtotime($value->DAT))}} </div>
                                    <div class="ml-5 font-weight-bold">
                                        Pojazd: {{$value->MARKA}} {{$value->MODEL}} - {{$value->NRREJ}}</div>
                                    <div class="ml-5 font-weight-bold" href="">
                                        Kierowca: {{$value->IMIE}} {{$value->NAZWISKO}}- {{$value->KIEROWCA}}</div>
                                </li>
                                <?php $counter++; ?>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <h2 class="text-center"> Ostatnie koszty serwisowe</h2>
                        <ul class="list-ic-home-blade vertical">
                            <?php $counter = 1; ?>
                            @foreach($daneSerwisowe as $key => $value)
                                <li>
                                    <span><?php echo $counter; ?></span>
                                    <a href="{{route("cost.edit", $value->ID)}}"
                                       class=" font-weight-bold h4" style="color: #676d73;!important;">
                                        {{ $value->KOSZT }} - {{ $value->PLN }} złotych </a>
                                    <div class="ml-5 font-weight-bold">
                                        Data: {{date("d-m-Y H:i", strtotime($value->DAT))}} </div>
                                    <div class="ml-5 font-weight-bold">
                                        Pojazd: {{$value->MARKA}} {{$value->MODEL}} - {{$value->NRREJ}}</div>
                                    <div class="ml-5 font-weight-bold" href="">
                                        Kierowca: {{$value->IMIE}} {{$value->NAZWISKO}}- {{$value->KIEROWCA}}</div>
                                </li>
                                <?php $counter++; ?>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--scripts--}}
    <script src="{{asset('js/charts.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

@endsection
