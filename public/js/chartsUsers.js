function wykresLiniowyPrzebieguKilometrowUSER(canvasID, daty, kilometry) {

    var myLineChart = new Chart(document.getElementById(canvasID), {
        type: 'line',
        data: {
            labels: daty,
            datasets: [{
                label: "Przejechane kilometry",
                lineTension: 0.3,
                backgroundColor: "rgba(2,117,216,0.2)",
                borderColor: "rgba(2,117,216,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(2,117,216,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                pointHitRadius: 20,
                pointBorderWidth: 2,
                data: kilometry,
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres Liniowy Przebiegu Kilometrów Kierowcy'
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        // min: 0,
                        // max: 40000,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, .125)",
                    }
                }],
            },
            legend: {
                display: false
            }
        }
    });
}


function wykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER(canvasID, daty, koszty1, koszty2) {

    var ctx = document.getElementById(canvasID);
    var myLineChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: daty,
            datasets: [{
                label: "Wydatki Paliwowe",
                backgroundColor: "rgba(0, 123, 255,1)",
                borderColor: "rgba(2,117,216,1)",
                data: koszty1,
            },
                {
                    label: "Koszty Serwisowe",
                    backgroundColor: "rgba(220, 53, 69,1)",
                    data: koszty2,
                    hidden: true,
                }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres kosztów kierowcy'
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 6
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        min: 0,
                        // max: 40000,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        display: true
                    }
                }],
            },
            legend: {
//                position:'right',
                display: true
            }
        }
    });
}

function barChartTwoUSER(canvasID) {

    var ctx = document.getElementById(canvasID);
    var myLineChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["2013", "2014", "2015", "2016", "2017", "2018"],
            datasets: [{
                label: "Wydatki",
                backgroundColor: "rgba(2,117,216,1)",
                borderColor: "rgba(2,117,216,1)",
                data: [420015, 135312, 654251, 117841, 359821, 74984],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres kosztów w lataach: 2013-2018'
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 6
                    }
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 700000,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        display: true
                    }
                }],
            },
            legend: {
                display: false
            }
        }
    });
}


function wykresDoughnutProporcjiUzywanychPojazdowUSER(canvasID, pojazdy, przebiegi) {

    var ctx = document.getElementById(canvasID);
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: pojazdy,
            datasets: [{
                data: przebiegi,
                backgroundColor: ["#5bc0de", "#d9534f", "#edee75", "#70b2f4", "#9F58E5", "#ff8888", "#ffdda8", "#f9ff93",
                    "#d5ffb5", "#b3fdff", "#e20000", "#e25a00", "#dbe200", "#00e22d", "#00dbe2", "#404068", "#796cb2", "#ef4f91",
                    "#ffbb55", "#48467c", "#50dfe4", "#ffa6d6", "#00ff00", "#ff0000", "#00dbe2", "#404068", "#796cb2", "#ef4f91",
                    "#ffbb55", "#48467c", "#50dfe4", "#ffa6d6", "#00ff00", "#ff0000", "#f9af2b", "#64dd17", "#3fa2b0", "#4d5857",
                    "#e5c8c3", "#ece8e1", "#7b3c7e", "#f7b3ff", "#f6fd89", "#fbbb7a", "#9aeec6", "#f465e0", '#007bff', '#dc3545',
                    '#00ffff', '#800080', '#660066', '#0099cc', '#fef65b', '#ff4040', '#6dc066', '#fef65b', '#191970',
                    '#6897bb', '#ff4444', '#a0db8e', '#0099cc', '#999999', '#ff4040', '#afeeee', '#00ff7f', '#8b0000',
                    '#088da5', '#ff00ff', '#008000', '#ffff00', '#800000', '#404040', '#ff0000', '#008080', '#666666',
                    '#007bff', '#dc3545', '#ffc107', '#0c1b1f', '#aaf442', '#008080', '#007bff', '#dc3545', '#ffc107',
                    '#0c1b1f', '#aaf442', '#008080', '#ff0000', '#00ffff', '#800080', '#660066', '#0099cc', '#fef65b',
                    '#ff4040', '#6dc066', '#fef65b', '#191970', '#6897bb', '#ff4444', '#a0db8e', '#0099cc', '#999999',
                    '#ff4040', '#afeeee', '#00ff7f', '#8b0000', '#088da5', '#ff00ff', '#008000', '#ffff00', '#800000',
                    '#404040', '#ff0000', '#008080', '#666666', '#007bff', '#dc3545', '#ffc107', '#0c1b1f', '#aaf442',
                    '#008080', '#007bff', '#dc3545', '#ffc107', '#0c1b1f', '#aaf442', '#008080', '#ff0000', '#008080',
                    '#00ffff', '#800080', '#660066', '#0099cc', '#fef65b', '#ff4040', '#6dc066', '#fef65b', '#191970',
                    '#6897bb', '#ff4444', '#a0db8e', '#0099cc', '#999999', '#ff4040', '#afeeee', '#00ff7f', '#8b0000',
                    '#088da5', '#ff00ff', '#008000', '#ffff00', '#800000', '#404040', '#ff0000', '#008080',
                    '#666666', '#007bff', '#dc3545', '#ffc107', '#0c1b1f', '#aaf442'],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres przejechanych kilometrów w danych pojeździe'
            },
            legend: {
                display: true,
                position: 'left'

            }
        }

    });
}

function wykresKolowyProporcjiKilometrowTymAutemWStosunkuDoInnychKierowcowUSER(canvasID, tenKierowca, pozostaliKierowcy) {

    var ctx = document.getElementById(canvasID);
    var myDoughnutChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Tego Kierowcy", "Pozostałych Kierowców"],
            datasets: [{
                data: [tenKierowca, pozostaliKierowcy],
                backgroundColor: ['#808080', '#8a2be2'],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres przejechanych kilometrów'
            },
            legend: {
                display: false
            }
        }
    });
}

function lineChartSrednieSpalanieUSER(canvasID, daty, spalaniaKierowcy, spalaniaFloty) {

    var myLineChart = new Chart(document.getElementById(canvasID), {
        type: 'line',
        data: {
            labels: daty,
            datasets: [{
                label: "Średnie spalanie kierowcy",
                lineTension: 0.3,
                backgroundColor: "rgba(0, 51, 102,0)",
                borderColor: "rgba(0, 51, 102,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(0, 51, 102,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(0, 51, 102,1)",
                pointHitRadius: 20,
                pointBorderWidth: 2,
                data: spalaniaKierowcy,
            }, {
                label: "Średnie spalanie floty",
                lineTension: 0.3,
                backgroundColor: "rgba(2,117,216,0)",
                borderColor: "rgba(218, 165, 32,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(218, 165, 32,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(218, 165, 32,1)",
                pointHitRadius: 20,
                pointBorderWidth: 2,
                data: spalaniaFloty,
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres Średniego Spalania kierowcy'
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        // min: 0,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, .225)",
                    }
                }],
            },
            legend: {
                display: true
            }
        }
    });
}
