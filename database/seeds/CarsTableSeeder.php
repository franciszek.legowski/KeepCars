<?php

use App\Car;
use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    public function run()
    {
        $cars = [

            [
                'brand_id' => '6',
                'model' => 'A3',
                'production_year' => '2015',
                'fuel_type_id' => '1',
                'vin' => '1G4HA5EM2AU134720',
                'registration_number' => 'CT52JA4',
                'mileage' => '120000',
            ],
            [
                'brand_id' => '6',
                'model' => 'A4',
                'production_year' => '2014',
                'fuel_type_id' => '1',
                'vin' => 'WVWAK73C96P183072',
                'registration_number' => 'CT5A51L',
                'mileage' => '172321',
            ],
            [
                'brand_id' => '7',
                'model' => 'Bentayga',
                'production_year' => '2017',
                'fuel_type_id' => '1',
                'vin' => '1FT8W4DT1BEA70903',
                'registration_number' => 'GD42FA1',
                'mileage' => '75105',
            ],
            [
                'brand_id' => '8',
                'model' => 'X3',
                'production_year' => '2017',
                'fuel_type_id' => '2',
                'vin' => '1XKADB9X09J235787',
                'registration_number' => 'CTFA3F1',
                'mileage' => '153075',
            ],
            [
                'brand_id' => '21',
                'model' => 'Transit',
                'production_year' => '2017',
                'fuel_type_id' => '2',
                'vin' => '1GTHG39C781203020',
                'registration_number' => 'CTR3276',
                'mileage' => '214378',
            ],
            [
                'brand_id' => '21',
                'model' => 'Transit',
                'production_year' => '2016',
                'fuel_type_id' => '2',
                'vin' => '1G6KF57942U188125',
                'registration_number' => 'CTJA42A',
                'mileage' => '204521',
            ],
            [
                'brand_id' => '21',
                'model' => 'Transit',
                'production_year' => '2017',
                'fuel_type_id' => '2',
                'vin' => '5J6TF2H56BL849472',
                'registration_number' => 'CT55MAT',
                'mileage' => '315780',
            ],
            [
                'brand_id' => '40',
                'model' => 'Vito',
                'production_year' => '2015',
                'fuel_type_id' => '2',
                'vin' => '4B3AU42Y8SE142413',
                'registration_number' => 'CTS81A4',
                'mileage' => '287004',
            ],

            [
                'brand_id' => '40',
                'model' => 'Vito',
                'production_year' => '2015',
                'fuel_type_id' => '2',
                'vin' => '1FMZU73W94UB75491',
                'registration_number' => 'CT5J1AL',
                'mileage' => '95412',
            ],
            [
                'brand_id' => '48',
                'model' => 'Astra',
                'production_year' => '2014',
                'fuel_type_id' => '2',
                'vin' => '1GCHK23U91F175322',
                'registration_number' => 'CT12AB3',
                'mileage' => '279425',
            ],
            [
                'brand_id' => '48',
                'model' => 'Astra',
                'production_year' => '2015',
                'fuel_type_id' => '2',
                'vin' => '1GTGC23R1YF519658',
                'registration_number' => 'CT81AVR',
                'mileage' => '183456',
            ],
            [
                'brand_id' => '48',
                'model' => 'Astra',
                'production_year' => '2016',
                'fuel_type_id' => '2',
                'vin' => '5B4KP42R713353669',
                'registration_number' => 'CTJ421A',
                'mileage' => '156780',
            ],
            [
                'brand_id' => '48',
                'model' => 'Astra',
                'production_year' => '2017',
                'fuel_type_id' => '2',
                'vin' => '1XKWDR9X5TR714245',
                'registration_number' => 'CTYU21A',
                'mileage' => '3108',
            ],
            [
                'brand_id' => '59',
                'model' => 'Fabia',
                'production_year' => '2016',
                'fuel_type_id' => '1',
                'vin' => '1XKTDU9X9YJ896298',
                'registration_number' => 'CT53AL1',
                'mileage' => '375102',
            ],

        ];
        foreach ($cars as $key => $value) {
            Car::create($value);
        }


    }
}



