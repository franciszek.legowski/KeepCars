<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Szef',
                'description' => 'Użytkownik ma wszystkie funkcjonalności'
            ],
            [
                'name' => 'accountant',
                'display_name' => 'Księgowy',
                'description' => 'Wszystkie funkcjonalności z tankowaniami i kosztami, dodatkowo generowanie raportów'
            ],
            [
                'name' => 'driver',
                'display_name' => 'Kierowca',
                'description' => 'Dodawanie tankowań i edycja'
            ],
            [
                'name' => 'mechanic',
                'display_name' => 'Mechanik',
                'description' => 'Dodawanie kosztów i edycja'
            ],
        ];

        foreach ($roles as $key => $value) {
            Role::create($value);
        }
    }
}