<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'role_id' => 1,
                'name' => 'Super User',
                'surname' => 'Keepcars',
                'email' => 'keepcars.root@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 1,
                'name' => 'Dariusz',
                'surname' => 'Kępa',
                'email' => 'dariusz@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 2,
                'name' => 'Barbara',
                'surname' => 'Kowalska',
                'email' => 'barbara@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Wiesław',
                'surname' => 'Paleta',
                'email' => 'wieslaw@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Marek',
                'surname' => 'Przybylski',
                'email' => 'marek@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Adam',
                'surname' => 'Tomczyk',
                'email' => 'adam@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Bartosz',
                'surname' => 'Tomczyk',
                'email' => 'bartosz@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Cezary',
                'surname' => 'Spirz',
                'email' => 'cezary@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Zbigniew',
                'surname' => 'Pankowski',
                'email' => 'zbigniew@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Radosław',
                'surname' => 'Grzelak',
                'email' => 'radoslaw@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Paweł',
                'surname' => 'Skrzyp',
                'email' => 'pawel@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Maciej',
                'surname' => 'Adamczyk',
                'email' => 'maciej@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 3,
                'name' => 'Grzegorz',
                'surname' => 'Grzyb',
                'email' => 'grzegorz@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
            [
                'role_id' => 4,
                'name' => 'Ryszard',
                'surname' => 'Grabek',
                'email' => 'ryszard@gmail.com',
                'password' => bcrypt('keepcars1234'),
                'remember_token' => str_random(60),
            ],
        ];

        foreach ($users as $key => $value) {
            User::create($value);
        }
    }
}
