<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('BrandsTableSeeder');
        $this->command->info('Brands table seeded!');

        $this->call('CostTypesTableSeeder');
        $this->command->info('Cost types table seeded!');

        $this->call('FuelTypesTableSeeder');
        $this->command->info('Fuel types table seeded!');

        $this->call('RolesTableSeeder');
        $this->command->info('Roles table seeded!');

        $this->call('PermissionsTableSeeder');
        $this->command->info('Permissions table seeded!');

        $this->call('PermissionRolesTableSeeder');
        $this->command->info('Permission-Roles table seeded!');

        $this->call('UsersTableSeeder');
        $this->command->info('Users table seeded!');

        $this->call('CarsTableSeeder');
        $this->command->info('Cars table seeded!');

    }
}
