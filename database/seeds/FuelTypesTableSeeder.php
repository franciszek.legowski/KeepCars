<?php

use App\Fuel_type;
use Illuminate\Database\Seeder;

class FuelTypesTableSeeder extends Seeder
{
    public function run()
    {
        $fuel_types = [

            [
                'name' => 'benzyna'
            ],
            [
                'name' => 'diesel'
            ],
            [
                'name' => 'LPG'
            ]
        ];

        foreach ($fuel_types as $key => $value) {
            Fuel_type::create($value);
        }
    }
}
