<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RidesAfterUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
CREATE DEFINER=`root`@`localhost` TRIGGER `keepcars`.`rides_AFTER_UPDATE` 
AFTER UPDATE ON `rides` 
FOR EACH ROW
BEGIN
declare nastepny integer;
declare poprzedni integer;
declare id0 integer;
declare km integer;
declare car integer;

#insert into roznice (ride1_id, ride2_id)
#values(old.begin_mileage, new.begin_mileage),
#(old.end_mileage, new.end_mileage);

if (old.end_mileage <> new.end_mileage)
then
	#szukaj roznicy w tabeli roznice
	set nastepny = (
		select ride2_id from roznice 
			where ride1_id = new.id limit 1);
    #jesli znalazno w roznicach
    if (nastepny is not null)
    then
		set km = (select begin_mileage 
			from rides where rides.id = nastepny limit 1);
		#porownaj nowa wartosc z roznica
		if(new.end_mileage = km)
		then
			#usun z roznosci
            #znajdz id roznicy
            set id0 = (
				select id from roznice where new.id = ride1_id 
					and nastepny = ride2_id);
			# usun roznice z bazy
            delete from roznice where roznice.id = id0;
        end if;
    else # nie znaleziono w tabrli roznice
		# szukaj nastepnego przejazdu w rides
        set nastepny = (select id from rides 
			where car_id = new.car_id and new.ride_date < ride_date
            order by ride_date limit 1);
		#jesli jest nastepny przjazd
        if (nastepny is not null)
        then
			#porownaj stan licznika
            set km = (select begin_mileage from rides 
            where rides.id = nastepny);
            if (new. end_mileage <> km)
            then
				#dodaj do tabeli roznic
                insert into roznice(ride1_id, ride2_id) 
					values(new.id, nastepny);
            end if;
        end if;
    end if;
end if;

#Zmieniono początkowy przebieg
if (old.begin_mileage <> new.begin_mileage)
then
	#szukaj roznicy w tabeli roznice
	set nastepny = (
		select ride2_id from roznice where ride1_id = new.id);
    #jesli znalazno w roznicach
    if (nastepny is not null)
    then
		set km = (select begin_mileage 
			from rides where rides.id = nastepny);
		#porownaj nowa wartosc z roznica
		if(new.end_mileage = km)
		then
			# usun roznice z bazy
            delete from roznice where roznice.ride2_id = new.id;
        end if;
    else # nie znaleziono w tabrli roznice
		# szukaj nastepnego przejazdu w rides
        set poprzedni = (select id from rides 
			where car_id = new.car_id and new.ride_date > ride_date
            order by ride_date desc limit 1);
		#jesli jest poprzedni przjazd
        if (poprzedni is not null)
        then
			#porownaj stan licznika
            set km = (select begin_mileage from rides 
            where rides.id = poprzedni);
            if (new.begin_mileage <> km)
            then
				#dodaj do tabeli roznic
                insert into roznice(ride1_id, ride2_id) 
                values(poprzedni, new.id);
            end if;
        end if;
    end if;
end if;

#Zmieniono car_id lub date
if (old.car_id <> new.car_id or 
	old.ride_date <> new.ride_date)
then
	#czy przejazd byl w roznicach na 2 pozycji
    set poprzedni = (select ride1_id from roznice 
		where ride2_id = new.id);
    if(poprzedni is not null)
    then
        # zczytaj kilometry przejazdu 1
        set km = (select end_mileage from rides
			where id = poprzedni);
            
		#poszukaj id nastepnego przejazdu
        set id0 = (select id from rides
			where car_id = (select car_id from rides 
				where id = poprzedni)
			and ride_date > (select ride_date from rides 
				where id = poprzedni)
            order by ride_date limit 1);
		#czy znaleziono następny przejazd
        if(id0 is not null)
        then
			if(km <> (select begin_mileage from rides 
				where id = id0))
            then
				insert into roznice (ride1_id, ride2_id) 
					values(poprzedni, id0);
            end if;
        end if;
            
		#usun wpisy w tabeli roznice
		delete from roznice where ride2_id = new.id;
    end if;
    
    #czy przejazd był w różnicach na 1 pozycji
    set nastepny = (select ride2_id from roznice 
		where ride1_id = new.id);
    if(nastepny is not null)
    then
		# zczytaj kilometry przejazdu 2
        set km = (select end_mileage from rides
			where id = nastepny);
            
		#poszukaj id poprzedniego przejazdu
        set id0 = (select id from rides
			where car_id = (select car_id from rides 
				where id = nastepny)
			and ride_date < (select ride_date from rides 
				where id = nastepny)
            order by ride_date desc limit 1);
		#czy znaleziono poprzedni przejazd
        if(id0 is not null)
        then
			if(km <> (select end_mileage from rides where id = id0))
            then
				insert into roznice (ride1_id, ride2_id) 
					values(id0, nastepny);
            end if;
        end if;
    
		#usuń wpisy w tabeli roznice
        delete from roznice where ride1_id = new.id;
    end if;
    
    #szukaj poprzedniego przejazdu
    set poprzedni = (select id from rides 
		where rides.car_id = new.car_id
		and rides.ride_date < new.ride_date 
		order by rides.ride_date desc limit 1);
    if (poprzedni is not null)
    then
		insert into roznice(ride1_id, ride2_id)
			values(poprzedni, new.id);
    end if;
	#szukaj następnego przejazdu
	set nastepny = (select id from rides where car_id = new.car_id
		and ride_date > new.ride_date order by ride_date limit 1);
    if (nastepny is not null)
    then
		insert into roznice(ride1_id, ride2_id)
			values(new.id, nastepny);
    end if;
        
end if;
END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `keepcars`.`rides_AFTER_UPDATE`');
    }
}
