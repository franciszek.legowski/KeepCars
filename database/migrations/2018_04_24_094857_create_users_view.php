<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUsersView extends Migration
{
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `keepcars`.`users_view`');
        DB::statement('
        CREATE 
            ALGORITHM = UNDEFINED 
            DEFINER = `root`@`localhost` 
            SQL SECURITY DEFINER
        VIEW `users_view` AS
            (SELECT 
                `users`.`id` AS `id`,
                `users`.`name` AS `name`,
                `users`.`surname` AS `surname`,
                `users`.`email` AS `email`,
                `roles`.`display_name` AS `rola`
            FROM
                (`users`
                LEFT JOIN `roles` ON ((`roles`.`id` = `users`.`role_id`))))
        ');
    }

    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS `keepcars`.`users_view`');
    }
}
