<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCarsView extends Migration
{
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `keepcars`.`cars_view`');
        DB::statement('
        CREATE 
            ALGORITHM = UNDEFINED 
            DEFINER = `root`@`localhost` 
            SQL SECURITY DEFINER
        VIEW `cars_view` AS
            (SELECT 
                `cars`.`id` AS `id`,
                `brands`.`name` AS `marka`,
                `cars`.`model` AS `model`,
                `cars`.`production_year` AS `production_year`,
                `cars`.`mileage` AS `mileage`,
                `cars`.`registration_number` AS `registration_number`,
                `cars`.`vin` AS `vin`
            FROM
                (`cars`
                LEFT JOIN `brands` ON ((`cars`.`brand_id` = `brands`.`id`))))
        ');
    }

    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS `keepcars`.`cars_view`');
    }
}
