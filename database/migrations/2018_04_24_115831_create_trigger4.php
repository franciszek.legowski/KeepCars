<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTrigger4 extends Migration
{
    public function up()
    {
        DB::unprepared('CREATE DEFINER = CURRENT_USER TRIGGER `keepcars`.`refuels_AFTER_INSERT` 
            AFTER INSERT ON `refuels` FOR EACH ROW
                BEGIN
				UPDATE cars 
                SET mileage = new.mileage
                WHERE id = new.car_id;
                END
        ');
    }

    public function down()
    {
        DB::unprepared('DROP TRIGGER `keepcars`.`refuels_AFTER_INSERT`');
    }
}
