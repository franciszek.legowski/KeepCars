<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    public function up()
    {
        DB::unprepared('
        CREATE DEFINER=`root`@`localhost` TRIGGER `keepcars`.`users_AFTER_INSERT` AFTER INSERT ON `users` FOR EACH ROW
            BEGIN
                INSERT INTO `keepcars`.`oauth_clients`
                (`user_id`,`name`,`secret`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`)
                VALUES (new.id, \'new secret token\', sha1(new.id+now()), \'http://localhost\', 0, 1, 0, now(), now());
                INSERT INTO `role_user` (`user_id`, `role_id`) VALUES (new.id, new.role_id);
            END
        ');
    }

    public function down()
    {
        DB::unprepared('DROP TRIGGER `keepcars`.`users_AFTER_INSERT`');
    }
}
