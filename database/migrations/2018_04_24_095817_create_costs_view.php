<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCostsView extends Migration
{
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `keepcars`.`costs_view`');
        DB::statement('
        CREATE 
            ALGORITHM = UNDEFINED 
            DEFINER = `root`@`localhost` 
            SQL SECURITY DEFINER
        VIEW `costs_view` AS
            (SELECT 
                `costs`.`id` AS `id`,
                `costs`.`user_id` AS `user_id`,
                `costs`.`car_id` AS `car_id`,
                `costs`.`cost_quota` AS `koszt`,
                `cost_types`.`name` AS `typ_kosztu`,
                `costs`.`description` AS `opis`,
                `costs`.`cost_date` AS `data`
            FROM
                (`costs`
                LEFT JOIN `cost_types` ON ((`cost_types`.`id` = `costs`.`cost_type_id`)))
            ORDER BY `costs`.`cost_date`)
        ');
    }

    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS `keepcars`.`costs_view`');
    }
}
