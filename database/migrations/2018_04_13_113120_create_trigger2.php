<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger2 extends Migration
{
    public function up()
    {
        DB::unprepared('
        CREATE DEFINER=`root`@`localhost` TRIGGER `keepcars`.`users_AFTER_UPDATE` AFTER UPDATE ON `users` FOR EACH ROW
            BEGIN
	            UPDATE `keepcars`.`role_user` SET role_id = new.role_id WHERE user_id = old.id;
            END
        ');
    }

    public function down()
    {
        DB::unprepared('DROP TRIGGER `keepcars`.`users_AFTER_UPDATE`');
    }
}
