<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTrigger5 extends Migration
{
    public function up()
    {
        //TODO: podwónie wyświetla edytowany przejazd, nie usuwa naprawionych.
        DB::unprepared('CREATE DEFINER = CURRENT_USER TRIGGER `keepcars`.`rides_AFTER_INSERT` 
            AFTER INSERT ON `rides` FOR EACH ROW
                BEGIN
                declare id0 int;
                declare km int;
                set id0 = (select id from rides 
					where car_id = new.car_id 
					and ride_date < new.ride_date 
                    order by ride_date desc limit 1);
				if(id0 is not null)
                then
					set km = (select end_mileage from rides 
						where id = id0 limit 1);
                    if(km <> new.begin_mileage)
                    then
						insert into roznice (ride1_id, ride2_id)
							values(id0, new.id);
						end if;
				end if;
                END
        ');
    }

    public function down()
    {
        DB::unprepared('DROP TRIGGER `keepcars`.`rides_AFTER_INSERT`');
    }
}
