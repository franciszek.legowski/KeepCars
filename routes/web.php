<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('home');
    }
    return view('welcome');
});


Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('home', 'HomeController@index')->name('home');

    // Wykresy
    Route::get('chart', 'ChartsController@chart_kolowy_inne')->name('chart');
    Route::get('chart/view', 'ChartsController@chart')->name('chart_view');
    Route::post('chart/response', 'ChartsController@response')->name('chart.response');
    Route::get('chart/index', 'ChartsController@chart_kolowy_tanlowania_inne')->name('chart_litry_samochod');
//    Route::get('chart/index', 'ChartsController@chart_litry_PLN_samochod')->name('chart_litry_PLN_samochod');

    // role
    Route::get('role/create', 'RoleController@create')->name('role.create');
    Route::post('role/store', 'RoleController@store')->name('role.store');
    Route::get('role/index', 'RoleController@index')->name('role.index');
    Route::delete('role/destroy/{id}', 'RoleController@destroy')->name('role.destroy');
    Route::get('role/edit/{id}', 'RoleController@edit')->name('role.edit');
    Route::post('role/update/{id}', 'RoleController@update')->name('role.update');

    // typy paliwa
    Route::get('fuel_type/create', 'FuelTypeController@create')->name('fuel_type.create');
    Route::post('fuel_type/store', 'FuelTypeController@store')->name('fuel_type.store');
    Route::get('fuel_type/index', 'FuelTypeController@index')->name('fuel_type.index');
    Route::delete('fuel_type/destroy/{id}', 'FuelTypeController@destroy')->name('fuel_type.destroy');
    Route::get('fuel_type/edit/{id}', 'FuelTypeController@edit')->name('fuel_type.edit');
    Route::post('fuel_type/update/{id}', 'FuelTypeController@update')->name('fuel_type.update');

    // marki samochodów
    Route::get('brand/create', 'BrandController@create')->name('brand.create');
    Route::post('brand/store', 'BrandController@store')->name('brand.store');
    Route::get('brand/index', 'BrandController@index')->name('brand.index');
    Route::delete('brand/destroy/{id}', 'BrandController@destroy')->name('brand.destroy');
    Route::get('brand/edit/{id}', 'BrandController@edit')->name('brand.edit');
    Route::post('brand/update/{id}', 'BrandController@update')->name('brand.update');

    // typy kosztu
    Route::get('cost_type/create', 'CostTypeController@create')->name('cost_type.create');
    Route::post('cost_type/store', 'CostTypeController@store')->name('cost_type.store');
    Route::get('cost_type/index', 'CostTypeController@index')->name('cost_type.index');
    Route::delete('cost_type/destroy/{id}', 'CostTypeController@destroy')->name('cost_type.destroy');
    Route::get('cost_type/edit/{id}', 'CostTypeController@edit')->name('cost_type.edit');
    Route::post('cost_type/update/{id}', 'CostTypeController@update')->name('cost_type.update');


    // samochody
    Route::get('car/create', 'CarController@create')->name('car.create');
    Route::post('car/store', 'CarController@store')->name('car.store');
    Route::get('car/index', 'CarController@index')->name('car.index');
    Route::delete('car/destroy/{id}', 'CarController@destroy')->name('car.destroy');
    Route::get('car/edit/{id}', 'CarController@edit')->name('car.edit');
    Route::post('car/update/{id}', 'CarController@update')->name('car.update');
    Route::post('car/qr/{id}', 'CarController@qr')->name('car.qr');
    Route::get('car_cost/index/{id?}', 'Car_CostController@index')->name('car_cost.index');

    // tankowania

    Route::get('refuel/create', 'RefuelController@create')->name('refuel.create');
    Route::post('refuel/store', 'RefuelController@store')->name('refuel.store');
    Route::get('refuel/index', 'RefuelController@index')->name('refuel.index');
    Route::delete('refuel/destroy/{id}', 'RefuelController@destroy')->name('refuel.destroy');
    Route::get('refuel/edit/{id}', 'RefuelController@edit')->name('refuel.edit');
    Route::post('refuel/update/{id}', 'RefuelController@update')->name('refuel.update');

    // koszta
    Route::get('cost/create', 'CostController@create')->name('cost.create');
    Route::post('cost/store', 'CostController@store')->name('cost.store');
    Route::get('cost/index', 'CostController@index')->name('cost.index');
    Route::delete('cost/destroy/{id}', 'CostController@destroy')->name('cost.destroy');
    Route::get('cost/edit/{id}', 'CostController@edit')->name('cost.edit');
    Route::post('cost/update/{id}', 'CostController@update')->name('cost.update');

    // użytkownicy
    Route::get('user/create', 'UserController@create')->name('user.create');
    Route::post('user/store', 'UserController@store')->name('user.store');
    Route::get('user/index', 'UserController@index')->name('user.index');
    Route::delete('user/destroy/{id}', 'UserController@destroy')->name('user.destroy');
    Route::get('user/edit/{id}', 'UserController@edit')->name('user.edit');
    Route::post('user/update/{id}', 'UserController@update')->name('user.update');
    Route::get('/changePassword', 'HomeController@showChangePasswordForm');
    Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');

    // przejazdy
    Route::get('ride/create', 'RideController@create')->name('ride.create');
    Route::post('ride/store', 'RideController@store')->name('ride.store');
    Route::get('ride/index', 'RideController@index')->name('ride.index');
    Route::delete('ride/destroy/{id}', 'RideController@destroy')->name('ride.destroy');
    Route::get('ride/edit/{id}', 'RideController@edit')->name('ride.edit');
    Route::post('ride/update/{id}', 'RideController@update')->name('ride.update');

    // token
    Route::get('user/token', 'TokenController@index')->name('user.token');

    // raporty
    Route::get('raport/index', 'RaportController@index')->name('raport.index');

    Route::post('raport/car/wykres', 'RaportController@car_wykres')->name('raport.car.wykres');
    Route::post('raport/car/{export?}', 'RaportController@car')->name('raport.car');
    Route::post('raport/kierowca/wykres', 'RaportController@kierowca_wykres')->name('raport.kierowca.wykres');
    Route::post('raport/kierowca/{export?}', 'RaportController@kierowca')->name('raport.kierowca');
    Route::get('raport/single/car', 'RaportController@show')->name('raport.show');
    Route::get('raport/export', 'RaportController@export')->name('raport.export');
    Route::get('raport/excel', 'RaportController@excel')->name('raport.excel');
//    Route::get('/getExport', 'ExcelController@getExport')->name('getExport');
});





