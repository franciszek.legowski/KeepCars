<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// marki samochodów
Route::get('brands', 'BrandAPIController@index')->name('brandAPI.index');
Route::get('brand/{id}', 'BrandAPIController@show')->name('brandAPI.show');
Route::post('brand', 'BrandAPIController@store')->name('brandAPI.store');
Route::put('brand/{id}', 'BrandAPIController@update')->name('brandAPI.update');
Route::delete('brand/{id}', 'BrandAPIController@destroy')->name('brandAPI.destroy');


// samochody
Route::get('cars', 'CarAPIController@index')->name('carAPI.index');
Route::get('car/{id}', 'CarAPIController@show')->name('carAPI.show');
Route::post('car', 'CarAPIController@store')->name('carAPI.store');
Route::put('car/{id}', 'CarAPIController@update')->name('carAPI.update');
Route::delete('car/{id}', 'CarAPIController@destroy')->name('carAPI.destroy');
// pozyskanie car_id dla danego VIN


// koszta
Route::get('costs', 'CostAPIController@index')->name('costAPI.index');
Route::get('cost/latest/{id}', 'CostAPIController@latest')->name('costAPI.latest');
Route::get('cost/{id}', 'CostAPIController@show')->name('costAPI.show');
Route::post('cost', 'CostAPIController@store')->name('costAPI.store');
Route::put('cost/{id}', 'CostAPIController@update')->name('costAPI.update');
Route::delete('cost/{id}', 'CostAPIController@destroy')->name('costAPI.destroy');


// role
Route::get('roles', 'RoleAPIController@index')->name('roleAPI.index');
Route::get('role/{id}', 'RoleAPIController@show')->name('roleAPI.show');
Route::post('role', 'RoleAPIController@store')->name('roleAPI.store');
Route::put('role/{id}', 'RoleAPIController@update')->name('roleAPI.update');
Route::delete('role/{id}', 'RoleAPIController@destroy')->name('roleAPI.destroy');


// użytkownicy
Route::get('users', 'UserAPIController@index')->name('userAPI.index');

Route::post('user', 'UserAPIController@store')->name('userAPI.store');
Route::put('user/{id}', 'UserAPIController@update')->name('userAPI.update');
Route::delete('user/{id}', 'UserAPIController@destroy')->name('userAPI.destroy');
// pozyskanie tokena dla danego adresu email


// typy kosztów
Route::get('cost_types', 'CostTypeAPIController@index')->name('cost_typeAPI.index');
Route::get('cost_type/{id}', 'CostTypeAPIController@show')->name('cost_typeAPI.show');
Route::post('cost_type', 'CostTypeAPIController@store')->name('cost_typeAPI.store');
Route::put('cost_type/{id}', 'CostTypeAPIController@update')->name('cost_typeAPI.update');
Route::delete('cost_type/{id}', 'CostTypeAPIController@destroy')->name('cost_typeAPI.destroy');


// typy paliwa
Route::get('fuel_types', 'FuelTypeAPIController@index')->name('fuel_typeAPI.index');
Route::get('fuel_type/{id}', 'FuelTypeAPIController@show')->name('fuel_typeAPI.show');
Route::post('fuel_type', 'FuelTypeAPIController@store')->name('fuel_typeAPI.store');
Route::put('fuel_type/{id}', 'FuelTypeAPIController@update')->name('fuel_typeAPI.update');
Route::delete('fuel_type/{id}', 'FuelTypeAPIController@destroy')->name('fuel_typeAPI.destroy');


// tankowania
Route::get('refuels', 'RefuelAPIController@index')->name('refuelAPI.index');
Route::get('refuel/latest/{id}', 'RefuelAPIController@latest')->name('refuelAPI.latest');
Route::get('refuel/{id}', 'RefuelAPIController@show')->name('refuelAPI.show');
Route::post('refuel', 'RefuelAPIController@store')->name('refuelAPI.store');
Route::put('refuel/{id}', 'RefuelAPIController@update')->name('refuelAPI.update');
Route::delete('refuel/{id}', 'RefuelAPIController@destroy')->name('refuelAPI.destroy');


// przejazdy
Route::get('rides', 'RideAPIController@index')->name('rideAPI.index');
Route::get('ride/{id}', 'RideAPIController@show')->name('rideAPI.show');
Route::post('ride', 'RideAPIController@store')->name('rideAPI.store');
Route::put('ride/{id}', 'RideAPIController@update')->name('rideAPI.update');
Route::delete('ride/{id}', 'RideAPIController@destroy')->name('rideAPI.destroy');

Route::post('secret', 'TokenController@secret')->name('user.secret');
Route::get('user/{id}', 'UserAPIController@show')->name('userAPI.show');
Route::post('car/qr', 'CarAPIController@qr')->name('carAPI.qr');